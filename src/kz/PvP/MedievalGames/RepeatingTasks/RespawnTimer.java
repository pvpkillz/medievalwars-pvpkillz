package kz.PvP.MedievalGames.RepeatingTasks;

import java.sql.SQLException;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Events.DeathListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

 
public class RespawnTimer extends BukkitRunnable {
	Player p;
    public RespawnTimer(Player pl) {
        p = pl;
    }

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
        	
    	if (DeathListener.x.containsKey(p.getName()) && DeathListener.x.get(p.getName()) >= 1){
    	p.setLevel(DeathListener.x.get(p.getName()));
    	DeathListener.x.put(p.getName(), DeathListener.x.get(p.getName()) - 1);
    	p.getInventory().clear();
    	}
    	else {
    	if (!DeathListener.x.containsKey(p.getName())){
    		try {
				Games.Start(p, true);
				
		    	if (JoinListener.red.contains(p)){
		    		final double redx = (double) Config.getConfig(ConfigFile.Teleports).get("redspawn" + ".Loc." +  "X");
		    		final double redy = (double) Config.getConfig(ConfigFile.Teleports).get("redspawn" + ".Loc." +  "Y");
		    		final double redz = (double) Config.getConfig(ConfigFile.Teleports).get("redspawn" + ".Loc." +  "Z");
			    	p.sendBlockChange(new Location(Bukkit.getWorld("war"),redx, redy + 5, redz), 0, (byte) 0);
		    		}
		    		else if (JoinListener.blue.contains(p)){
		    		final double bluex = (double) Config.getConfig(ConfigFile.Teleports).get("bluespawn" + ".Loc." +  "X");
		    		final double bluey = (double) Config.getConfig(ConfigFile.Teleports).get("bluespawn" + ".Loc." +  "Y");
		    		final double bluez = (double) Config.getConfig(ConfigFile.Teleports).get("bluespawn" + ".Loc." +  "Z");
			    	p.sendBlockChange(new Location(Bukkit.getWorld("war"),bluex, bluey + 5, bluez), 0, (byte) 0);
		    		}
    			DeathListener.BT.get(p.getName()).cancel();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else if (DeathListener.x.get(p.getName()) <= 0){
			DeathListener.x.remove(p.getName());
    		try {
				Games.Start(p, true);
    			DeathListener.BT.get(p.getName()).cancel();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	}
	}
 
}