package kz.PvP.MedievalGames;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kz.PvP.MedievalGames.Enums.RewardType;
import kz.PvP.MedievalGames.Threads.Query;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class Mysql {
	
	static Main plugin;
	
	
	public Mysql(Main mainclass) {
		plugin = mainclass;
	}
	
	
	public static int GetUserID (String p) throws SQLException{
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM Users WHERE LOWER(Username) = LOWER('" + p + "') LIMIT 1");
		int userid;
		if (res.next())
		userid = Integer.parseInt(res.getString("ID"));
		else
		userid = 0;
		
		return userid;
	}
	/*
	 * DEPRECIATED DUE TO MYSQL VULNERBILITIES WITH SQL INJECTION
	public static void dbSubmit (String query) throws SQLException{
			Statement stmt = Main.con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			Main.con.close();
	}
	*/
	public static PreparedStatement PS (String query) throws SQLException{
		// We will make sure our database is safe from injections.
		PreparedStatement ps = Main.con.prepareStatement(query);
		return ps;
	}
	
	public static ResultSet dbGet(String query) throws SQLException{
		ResultSet rs = null;
		try {
			rs = Main.con.createStatement().executeQuery(query);
		} catch (SQLException e) {
			System.out.println("Failed to get info from Database. Make sure connection is open!");
		}
		return rs;
	}
	
	public static int getPlayerPts(String name) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM Points WHERE ID = '" + GetUserID(name) + "' LIMIT 1");
		int userpts;
		if (res.next())
		userpts = Integer.parseInt(res.getString("Points"));
		else{
		Main.con.createStatement().executeUpdate("INSERT INTO Points (ID, Points) VALUES ('" + GetUserID(name) + "','0')");
		userpts = 0;
		}
		return userpts;
	}
	public static boolean modifyUserPts (String p, int Change, RewardType reason) throws SQLException{
		int Player = GetUserID (p);
		
		int curPts = getPlayerPts(p);
		Player user = Bukkit.getPlayerExact(p);

		if (Change < 0){// We want to take away coins
			if(curPts < Math.abs(Change)){// Not enough coins
				Message.P(user, Message.Replacer(Message.Replacer(Message.CannotAfford, "" + curPts, "<points>"), "" + Math.abs(Change), "<cost>"));
				return false;
			}
			else if (curPts >= Math.abs(Change)){// Enough coins met
				int newPts = curPts - Math.abs(Change);
				Message.P(user, Message.Replacer(Message.Replacer(Message.PurchaseSuccess, "" + newPts, "<got>"), "" + Math.abs(Change), "<paid>"));
				
				PreparedStatement ps = Mysql.PS("UPDATE Points SET Points = ? WHERE ID = ?");
				ps.setInt(1, newPts);
				ps.setInt(2, Player);
				ps.executeUpdate();
				//dbSubmit("UPDATE Points SET Points = '" + newPts + "' WHERE ID = '" + Player + "'");
				
				return true;
			}
			
		}
		else if (Change >= 0){// We want to give coins
			int newPts = curPts + Math.abs(Change);
			String EXTRA = ".";
			if (reason == RewardType.Kill)
			EXTRA = " for the kill.";
			else if (reason == RewardType.FlagCap)
			EXTRA = " for capturing the flag.";			
			//dbSubmit("UPDATE Points SET Points = '" + newPts + "' WHERE ID = '" + Player + "'");
			
			PreparedStatement ps = Mysql.PS("UPDATE Points SET Points = ? WHERE ID = ?");
			ps.setInt(1, newPts);
			ps.setInt(2, Player);
			ps.executeUpdate();
			
			
			
			Message.P(user, Message.Replacer(Message.Replacer(Message.Replacer(Message.EarnedPoints, "" + Math.abs(Change), "$"), "" + getPlayerPts(p), "<curpoints>"), EXTRA, "<reason>"));
			return true;
		}
		return false;
	}
	
	public static String GetUserUps(String name) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM Upgrades WHERE User = '" + GetUserID(name) + "' LIMIT 1");
		String userups;
		if (res.next())
		userups = res.getString("Upgrades");
		else{
		Main.con.createStatement().executeUpdate("INSERT INTO Upgrades (User, Upgrades) VALUES ('" + GetUserID(name) + "','')");
		userups = " ";
		}
		return userups;
	}
	public static String GetUserActiveUps(String name) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM ActiveUpgrade WHERE User = '" + GetUserID(name) + "' LIMIT 1");
		String useractiveups;
		if (res.next())
		useractiveups = res.getString("ActiveUpgrade");
		else{
		Main.con.createStatement().executeUpdate("INSERT INTO ActiveUpgrade (User, ActiveUpgrade) VALUES ('" + GetUserID(name) + "','')");
		useractiveups = " ";
		}
		return useractiveups;
	}
	public static String BuyUpgrade(Player p, String upgrade, int cost, String Class) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM Upgrades WHERE User = '" + GetUserID(p.getName()) + "' LIMIT 1");
		String userups = null;
		//ConfigurationSection list = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(classc);
		if (res.next()){
		userups = res.getString("Upgrades");
		if (userups.contains(" " + Class + ":" + upgrade.toLowerCase())){
		Message.P(p, Message.YouAlreadyOwnUpgrade);
		}
		else{
		if (getPlayerPts(p.getName()) >= cost){
		Message.P(p, Message.SuccessPurchase);
		Main.con.createStatement().executeUpdate("UPDATE Upgrades SET Upgrades = '" + userups + " " + Class + ":" + upgrade.toLowerCase() + "|" + "' WHERE User = '" + GetUserID(p.getName()) + "'");
		Main.con.createStatement().executeUpdate("UPDATE Points SET Points = '" + (getPlayerPts(p.getName()) - cost) + "' WHERE ID = '" + GetUserID(p.getName()) + "'");
		ActivateUpgrade(p, upgrade, Class);
		}
		else{
		Message.P(p, Message.Replacer(Message.Replacer(Message.CannotAfford, "" + cost, "<cost>"), "" + getPlayerPts(p.getName()), "<points>"));
		}
		}
		}
		else{
		}
		return userups;
	}
	public static void ActivateUpgrade(Player p, String Upgrade, String Class) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM ActiveUpgrade WHERE User = '" + GetUserID(p.getName()) + "' LIMIT 1");
		String userups = null;
		//ConfigurationSection list = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(classc);
		if (res.next()){
		
		userups = res.getString("ActiveUpgrade");
		
		String[] split = Upgrade.split(" ");
		if (userups.contains(" " + Class + ":" + Upgrade.toLowerCase())){
		Message.P(p, Message.DeactivatedUpgrade);
		userups = userups.replace(" " + Class + ":" + Upgrade.toLowerCase() + "=", "");
		Main.con.createStatement().executeUpdate("UPDATE ActiveUpgrade SET ActiveUpgrade = '" + userups + "' WHERE User = '" + GetUserID(p.getName()) + "'");
		}
		else{
		if (GetUserUps(p.getName()).contains(" " + Class + ":" + Upgrade.toLowerCase())){
		/*
		 *  If we used the method below, we should wipe the activations a player makes when he changes class.
		 *  This would have to  be done, to avoid cross-class conflict messages.
		*/
		if (userups.toLowerCase().contains(" " + Class.toLowerCase() + ":" + split[0].toLowerCase())){
		// I have recoded a small part of this code + the config so that there are no issues with the String.contains("BLA") and String.contains("BLA I")
		// I will need to code a small system that will de-activate conflicting upgrades automatically
		// The goal of this plugin is to make it dynamic and very simple to use...
		// 
		
		String[] allups = userups.split("=");
		for (String up : allups){
			if (up.toLowerCase().contains(" " + Class.toLowerCase() + ":" + split[0].toLowerCase())){
				userups = userups.replace(up.toLowerCase() + "=", "");
			}
		}
		}
		Message.P(p, Message.Replacer(Message.ActivatedUpgrade, Class, "<class>"));
		
		if (Games.NotAttacked.contains(p)){
			Kits.GiveKit(p);
		}
		Main.con.createStatement().executeUpdate("UPDATE ActiveUpgrade SET ActiveUpgrade = '" + userups + " " + Class + ":" + Upgrade.toLowerCase() + "=" + "' WHERE User = '" + GetUserID(p.getName()) + "'");
		}
		else{
		Message.P(p, Message.UnactivatedUpgrade);
		}
		}
		
		
		}
		
		
		else{
		}
	}


	public static String getUserName(int id) throws SQLException {
		ResultSet res = Main.con.createStatement().executeQuery("SELECT * FROM Users WHERE ID = '" + id + "' LIMIT 1");
		String username;
		if (res.next())
		username = res.getString("Username");
		else
		username = "None";
		
		return username;
	}


	public static int getMwTime(String name) throws SQLException {
		int totalGameTime = 0;
		
		ResultSet rs = Mysql.dbGet("SELECT * FROM UserInfo WHERE User = '" + GetUserID(name) + "' LIMIT 1");
		totalGameTime = rs.getInt("MWTime");
		
		
		return totalGameTime;
	}
}
