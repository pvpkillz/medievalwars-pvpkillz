package kz.PvP.MedievalGames.Threads;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import kz.PvP.MedievalGames.Main;

import org.bukkit.plugin.Plugin;




public class Query extends Thread{
	
	private String sql;
	private Logger log;
	private Connection con;
	
	public Query(String sql, Logger log, Connection con, Plugin plugin) {
		
		setDaemon(false);
		
		this.sql = sql;
		this.log = log;
		this.con = con;
	}
	
	@Override
	public void run() {
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException ex) {
			log.warning("Error with following query: "
					+ sql);
			log.warning(ex.getMessage());
			try {
				Main.con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NullPointerException ex) {
			log.warning("Error while performing a query. (NullPointerException)");
		}
	}
}
