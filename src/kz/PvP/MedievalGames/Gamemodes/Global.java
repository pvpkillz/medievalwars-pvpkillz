package kz.PvP.MedievalGames.Gamemodes;

import java.util.ArrayList;
import java.util.List;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.DamageListener;
import kz.PvP.MedievalGames.Events.InteractListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Message;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Color;
import org.bukkit.entity.Player;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcsg.double0negative.tabapi.TabAPI;

import com.comphenix.protocol.utility.MinecraftReflection;


public class Global {
	public static Main plugin;
	public Global(Main mainclass) {
		plugin = mainclass;
		}
	
	
	
	  public static void clearAll(Player p) {
	      for (int i = 0; i < TabAPI.getVertSize(); i++)
	        for (int j = 0; j < TabAPI.getHorizSize(); j++)
	          TabAPI.setTabString(plugin, p, i, j, "");
	  }
public static void updateTab(Player p){ //update the tab for a player
	  TabAPI.setTabString(plugin, p, 0, 0, ChatColor.GREEN + "<<===============");
	  TabAPI.setTabString(plugin, p, 0, 1, ChatColor.GREEN + " =MedievalWar=");
	  TabAPI.setTabString(plugin, p, 0, 2, ChatColor.GREEN + "============>>");
	  TabAPI.setTabString(plugin, p, 1, 0, ChatColor.AQUA + "    Blue Team");
	  TabAPI.setTabString(plugin, p, 1, 1, ChatColor.GRAY + "    Spectators");
	  TabAPI.setTabString(plugin, p, 1, 2, ChatColor.RED + "  Red Team");
	  TabAPI.setTabString(plugin, p, 2, 0, ChatColor.DARK_GREEN + "<<===============");
	  TabAPI.setTabString(plugin, p, 2, 1, ChatColor.DARK_GREEN + " ===PvPKillz===");
	  TabAPI.setTabString(plugin, p, 2, 2, ChatColor.DARK_GREEN + "============>>");
	  for (int x = 0; x <= 2; x++){
		  String emptyText = "";
		  for (int y = 3; y <= TabAPI.getVertSize() - 1; y++){
			int num = y - 3;
			if (x == 0){
				if (JoinListener.blue.size() > num)
				  TabAPI.setTabString(plugin, p, y, x, ChatColor.AQUA + JoinListener.blue.get(num).getName());
				else{
					TabAPI.setTabString(plugin, p, y, x, ChatColor.AQUA + emptyText + " ");
					emptyText = emptyText + " ";
				}
				}
			if (x == 1){
				if (JoinListener.spectators.size() > num)
					  TabAPI.setTabString(plugin, p, y, x, ChatColor.GRAY + JoinListener.spectators.get(num).getName());
				else{
					TabAPI.setTabString(plugin, p, y, x, ChatColor.GRAY + emptyText + " ");
					emptyText = emptyText + " ";
				}
			}
			if (x == 2){
				if (JoinListener.red.size() > num)
					  TabAPI.setTabString(plugin, p, y, x, ChatColor.RED + JoinListener.red.get(num).getName());
				else{
					TabAPI.setTabString(plugin, p, y, x, ChatColor.RED + emptyText + " ");
					emptyText = emptyText + " ";
				}
			}
		  }
	  }
	  
	  
	  
	  
	  TabAPI.updatePlayer(p);
	}









public static Location GetLocOfString(String string, String world) {
	double x = (double) Config.getConfig(ConfigFile.Teleports).get(string + ".X");
	double y = (double) Config.getConfig(ConfigFile.Teleports).get(string + ".Y");
	double z = (double) Config.getConfig(ConfigFile.Teleports).get(string + ".Z");
	Location loc = new Location(plugin.getServer().getWorld(world),x,y,z);
	
	if (Config.getConfig(ConfigFile.Teleports).get(string + ".Yaw") != null){
		double yaw = (double) Config.getConfig(ConfigFile.Teleports).get(string + ".Yaw");
		loc.setYaw((float)yaw);
	}
	if (Config.getConfig(ConfigFile.Teleports).get(string + ".Pitch") != null){
		double pitch = (double) Config.getConfig(ConfigFile.Teleports).get(string + ".Pitch");
		loc.setPitch((float)pitch);
	}
	return loc;
}

public static void SpawnRedHorse(String a) {
	double x = (double) Config.getConfig(ConfigFile.Teleports).get("horse.red." + a + "." +  "X");
	double y = (double) Config.getConfig(ConfigFile.Teleports).get("horse.red." + a + "." +  "Y");
	double z = (double) Config.getConfig(ConfigFile.Teleports).get("horse.red." + a + "." +  "Z");
	Location loc = new Location(Bukkit.getWorld("war"), x, y, z);
	Horse h = (Horse) Bukkit.getWorld("war").spawnEntity(loc, EntityType.HORSE);
	HorseInventory hi = h.getInventory();
	hi.setSaddle(new ItemStack(Material.SADDLE));
	h.setColor(Color.WHITE);
	h.setTamed(true);
	h.setMaxHealth(24.0);
	double ran = Math.random();
	if (ran < .10)
	h.setCustomName(ChatColor.RED + "Eclipse");
	else if (ran < .20)
	h.setCustomName(ChatColor.RED + "Mark");
	else if (ran < .30)
	h.setCustomName(ChatColor.RED + "Jumper");
	else if (ran < .40)
	h.setCustomName(ChatColor.RED + "Jumbo");
	else if (ran < .50)
	h.setCustomName(ChatColor.RED + "Cappie");
	else if (ran < .60)
	h.setCustomName(ChatColor.RED + "Cally");
	else if (ran < .70)
	h.setCustomName(ChatColor.RED + "Oliver");
	else if (ran < .80)
	h.setCustomName(ChatColor.RED + "Caramel");
	else if (ran < .90)
	h.setCustomName(ChatColor.RED + "Odyssy");
	else if (ran < 1.00)
	h.setCustomName(ChatColor.RED + "Wild Buck");
	h.setCustomNameVisible(true);
	Games.RedHorse.put(h.getUniqueId(), a);
}
public static void SpawnBlueHorse(String a) {
	double x = (double) Config.getConfig(ConfigFile.Teleports).get("horse.blue." + a + "." +  "X");
	double y = (double) Config.getConfig(ConfigFile.Teleports).get("horse.blue." + a + "." +  "Y");
	double z = (double) Config.getConfig(ConfigFile.Teleports).get("horse.blue." + a + "." +  "Z");
	Location loc = new Location(Bukkit.getWorld("war"), x, y, z);
	Horse h = (Horse) Bukkit.getWorld("war").spawnEntity(loc, EntityType.HORSE);
	HorseInventory hi = h.getInventory();
	hi.setSaddle(new ItemStack(Material.SADDLE));
	h.setColor(Color.BLACK);
	h.setTamed(true);
	h.setMaxHealth(24.0);
	double ran = Math.random();
	if (ran < .10)
	h.setCustomName(ChatColor.BLUE + "Billy");
	else if (ran < .20)
	h.setCustomName(ChatColor.BLUE + "Cappicino");
	else if (ran < .30)
	h.setCustomName(ChatColor.BLUE + "Abby");
	else if (ran < .40)
	h.setCustomName(ChatColor.BLUE + "Ai");
	else if (ran < .50)
	h.setCustomName(ChatColor.BLUE + "Magee");
	else if (ran < .60)
	h.setCustomName(ChatColor.BLUE + "Margo");
	else if (ran < .70)
	h.setCustomName(ChatColor.BLUE + "Java");
	else if (ran < .80)
	h.setCustomName(ChatColor.BLUE + "Galaxy");
	else if (ran < .90)
	h.setCustomName(ChatColor.BLUE + "Gambit");
	else if (ran < 1.00)
	h.setCustomName(ChatColor.BLUE + "Echo");
	h.setCustomNameVisible(true);
	Games.BlueHorse.put(h.getUniqueId(), a);
}













public static void DeleteFromArrays(Player p, String string) {
	InteractListener.IncreaseHorseHP1.remove(p.getName());
	InteractListener.IncreaseHorseHP2.remove(p.getName());
	InteractListener.IncreaseHorseHP3.remove(p.getName());
	InteractListener.Compass.remove(p.getName());
	InteractListener.Crossbow.remove(p.getName());
	InteractListener.Longbow.remove(p.getName());
	InteractListener.PoisonousShooters1.remove(p.getName());
	InteractListener.PoisonousShooters2.remove(p.getName());
	InteractListener.PoisonousShooters3.remove(p.getName());
	DamageListener.ArcherBlock1.remove(p.getName());
	DamageListener.ArcherBlock2.remove(p.getName());
	DamageListener.ArcherBlock3.remove(p.getName());
	DamageListener.IncreaseBlock1.remove(p.getName());
	DamageListener.IncreaseBlock2.remove(p.getName());
	DamageListener.IncreaseBlock3.remove(p.getName());
	DamageListener.ShieldBlocks.remove(p.getName());
	
	if (string == "full"){
		JoinListener.red.remove(p);
		JoinListener.blue.remove(p);
		JoinListener.spectators.remove(p);
		JoinListener.Objectives.remove(p);
		JoinListener.Boards.remove(p);
	}
}

public static void GiveItem(Player p, String string, Material nameTag, int slot, String loretext) {
	ItemStack selector = new ItemStack(nameTag);
	selector = MinecraftReflection.getBukkitItemStack(selector);
	ItemMeta im = selector.getItemMeta();
	im.setDisplayName(ChatColor.AQUA + string);
	List<String> lore = new ArrayList<String>();
	if (loretext.contains("\n")){
	String[] loreitems = loretext.split("\n");
	for (String loretxt: loreitems){
		lore.add(loretxt);
	}
	}
	else
	lore.add(loretext);
	
	selector.setItemMeta(im);
	im.setLore(lore);
	selector.setItemMeta(im);
	selector.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
	p.getInventory().setItem(slot, selector);
}



public static void GiveTools(Player p, boolean b) {
	if (b == true){
	GiveItem(p, "Class Selector", Material.EMERALD, 6, Message.ClassDesc);
	GiveItem(p, "Upgrade Selector", Material.CARROT_STICK, 7, Message.UpgradeDesc);
	GiveItem(p, "Activator", Material.FEATHER, 8, Message.ActivatorDesc);
	GiveItem(p, "Spectate Compass", Material.COMPASS, 5, Message.SpectateCompassDesc);

	}
	else if (b == false){
	if (Main.Gamemode == Gamemode.Mixed || Main.Gamemode == Gamemode.CTF)
	GiveItem(p, "Flag Detector", Material.COMPASS, 5, Message.CompassDesc);
	}
	
	}



public static void AutoBalanceTeams() {
	// We make a check to see if teams are balanced. If not then we will auto-balance them.
	if (JoinListener.red.size() - (Games.TeamBal + 1) >= JoinListener.blue.size()){//Red team is stacked
		Player player = JoinListener.red.get(JoinListener.red.size() - 1);
		DeleteFromArrays(player, "full");
		Message.P(player, Message.SwappedDueToBalance);
		JoinListener.AssignPlayer(player, Team.Blue);
	}
	else if (JoinListener.blue.size() - (Games.TeamBal + 1) >= JoinListener.red.size()){//Blue team is stacked
		Player player = JoinListener.blue.get(JoinListener.blue.size() - 1);
		DeleteFromArrays(player, "full");
		Message.P(player, Message.SwappedDueToBalance);
		JoinListener.AssignPlayer(player, Team.Red);
	}
}



public static String ChatRank(String pname, boolean Brackets) {
	int Kills = 0;
	if (Games.Kills.containsKey(pname))
	Kills = Games.Kills.get(pname);
	else
	Kills = 0;
	
	String P = "";
	if (Kills <= 100)
		P = "Peasant";
	else if (Kills <= 250)
		P = "Lionheart";
	else if (Kills <= 650)
		P = "Squire";
	else if (Kills <= 875)
		P = "Crusader";
	else if (Kills <= 1200)
		P = "Baron";
	else if (Kills <= 1600)
		P = "Count";
	else if (Kills <= 2200)
		P = "Desperado";
	else if (Kills <= 3200)
		P = "War-Chief";
	else if (Kills <= 4000)
		P = "Overlord";
	else if (Kills <= 6000)
		P = "Bandito";
	else if (Kills <= 8000)
		P = "Justiciar";
	else if (Kills <= 10000)
		P = "Archon";
	else if (Kills <= 13000)
		P = "Emperor";
	else if (Kills <= 17000)
		P = "Wunderkind";
	else if (Kills <= 20000)
		P = "The Amazing";
	
	
	String Name = "";
	
	if (Brackets)
	Name = ChatColor.GRAY + "(" + ChatColor.GOLD  + P + ChatColor.GRAY + ") ";
	else
	Name =  ChatColor.GOLD  + P + ChatColor.GRAY;
	
	return Name;
}

}
