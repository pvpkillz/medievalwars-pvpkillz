package kz.PvP.MedievalGames.Gamemodes;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;



public class CTF {
	public static Main plugin;
	public CTF(Main mainclass) {
		plugin = mainclass;
		}
	
	
	
	
	
	@SuppressWarnings("deprecation")
	public static void FlagDespawnTimer(final Location loc, final String team) {
		
		final byte b = plugin.getServer().getWorld("war").getBlockAt(loc).getData();
		if (team == "red"){
    		Games.RedFlagHolder = null;
    		Games.RedFlagLoc = loc;
    		plugin.getServer().broadcastMessage(ChatColor.GOLD + "Red flag has been dropped. Flag will return to base in " + Games.TimerDespawn + " seconds.");
		}
		else if (team == "blue"){
    		Games.BlueFlagHolder = null;
    		Games.BlueFlagLoc = loc;
    		plugin.getServer().broadcastMessage(ChatColor.GOLD + "Blue flag has been dropped. Flag will return to base in " + Games.TimerDespawn + " seconds.");

		}
		
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
			if (plugin.getServer().getWorld("war").getBlockTypeIdAt(loc) == 159 && 
					plugin.getServer().getWorld("war").getBlockAt(loc).getData() == b
					){
				plugin.getServer().getWorld("war").getBlockAt(loc).setTypeId(0);
				
				if (team == "red"){
				Games.RedFlagLoc = Global.GetLocOfString("flag.red", "war");
				SpawnFlag(Global.GetLocOfString("flag.red", "war"), "red");
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The red flag has returned, back to the base.");
				}
				else if (team == "blue"){
				Games.BlueFlagLoc = Global.GetLocOfString("flag.blue", "war");
				SpawnFlag(Global.GetLocOfString("flag.blue", "war"), "blue");
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The blue flag has returned, back to the base.");
				}
			}
		}
		}, 20L * Games.TimerDespawn);
		
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
			if (plugin.getServer().getWorld("war").getBlockTypeIdAt(loc) == 159 && 
					plugin.getServer().getWorld("war").getBlockAt(loc).getData() == b
					){
				plugin.getServer().getWorld("war").getBlockAt(loc).setTypeId(0);
				if (team == "red"){
				Games.RedFlagLoc = Global.GetLocOfString("flag.red", "war");
				SpawnFlag(Global.GetLocOfString("flag.red", "war"), "red");
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The red flag has returned, back to the base.");
				}
				else if (team == "blue"){
				Games.BlueFlagLoc = Global.GetLocOfString("flag.blue", "war");
				SpawnFlag(Global.GetLocOfString("flag.blue", "war"), "blue");
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The blue flag has returned, back to the base.");
				}
			}
		}
		}, 20L * Games.TimerDespawn + 5);
	}


	
	
	
	
	
	
	
	
	// Spawn Flag for CTF and Mixed gamemode
	@SuppressWarnings("deprecation")
	public static void SpawnFlag(Location location, String color) {
		Block flagblock = plugin.getServer().getWorld("war").getBlockAt(location);
		if (flagblock.getTypeId() != 0){
			if (flagblock.getTypeId() == 159 && flagblock.getData() == 14 && color == "red"){
			
			
			}
			else if (flagblock.getTypeId() == 159 && flagblock.getData() == 11 && color == "blue"){
			
				
			}
			else{
			location = new Location(location.getWorld(), location.getX(), location.getY() + 1, location.getZ());
			flagblock = plugin.getServer().getWorld("war").getBlockAt(location);
			if (flagblock.getTypeId() == 159){
				location = new Location(location.getWorld(), location.getX(), location.getY() + 2, location.getZ());
				flagblock = plugin.getServer().getWorld("war").getBlockAt(location);
			}
			}
		}
		flagblock.setTypeId(159);
		if (color == "blue"){
		
		flagblock.setData((byte)11);// We spawn a blue colored stained clay
		Games.BlueFlagLoc = location;
		Games.BlueFlagHolder = null;
		}
		else if (color == "red"){
		flagblock.setData((byte)14);// We spawn a blue colored stained clay
		Games.RedFlagLoc = location;
		Games.RedFlagHolder = null;
		}
}
	
	
	
	
	
}
