package kz.PvP.MedievalGames.Commands;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Events.DeathListener;
import kz.PvP.MedievalGames.Timers.PreGameTimer;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Message;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class GameCommands extends JavaPlugin implements CommandExecutor{
	
	public Main plugin;
	   
	   public GameCommands(Main main) {
		 	this.plugin = main;

	}
	   
	   
@SuppressWarnings("deprecation")
@Override
public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
{
  Player p = (Player)sender;
  Location loc = p.getLocation();
  double X = loc.getX();
  double Y = loc.getY();
  double Z = loc.getZ();
  double Yaw = (double)loc.getYaw();
  double P = (double)loc.getPitch();
  double XLook = p.getTargetBlock(null, 100).getX();
  double YLook = p.getTargetBlock(null, 100).getY();
  double ZLook = p.getTargetBlock(null, 100).getZ();
  if (cmd.getName().equalsIgnoreCase("mwa") && ((p.hasPermission("mw.*") || (p.hasPermission("mw.admin"))))){
	  if (args.length == 0){
		  p.sendMessage(ChatColor.GOLD + "==== " + ChatColor.GRAY + "Medieval War Admin Menu" + ChatColor.GOLD + " ====");
		  p.sendMessage(ChatColor.BLUE + "/MWA warps <WarpID>");
		  p.sendMessage(ChatColor.BLUE + "/MWA Horse " + ChatColor.GRAY + "<" + ChatColor.RED + "red" + ChatColor.GRAY + "/" + ChatColor.AQUA + "blue" + ChatColor.GRAY + "/" + ChatColor.GOLD + "clear" + ChatColor.GRAY + ">");
		  p.sendMessage(ChatColor.BLUE + "/MWA EndRound");
		  p.sendMessage(ChatColor.BLUE + "/MWA StartRound");
	  }
	  else if (args[0].equalsIgnoreCase("warps") && (args.length == 1)){
		  p.sendMessage(ChatColor.GOLD + "Valid arguments:");
		  p.sendMessage(ChatColor.GOLD + "redspawn, bluespawn, mainlobby, spectator, dead");
	  }
	  else if (args[0].equalsIgnoreCase("warps") && (args.length >= 2)){
		  if (args[1].equalsIgnoreCase("redspawn")){
			  Config.getConfig(ConfigFile.Teleports).set("redspawn" + ".Loc." +  "X", X);
		 	  Config.getConfig(ConfigFile.Teleports).set("redspawn" + ".Loc." +  "Y", Y);
		 	  Config.getConfig(ConfigFile.Teleports).set("redspawn" + ".Loc." +  "Z", Z);
		 	  Config.getConfig(ConfigFile.Teleports).set("redspawn" + ".Loc." +  "Pitch", P);
		 	  Config.getConfig(ConfigFile.Teleports).set("redspawn" + ".Loc." +  "Yaw", Yaw);
		 	 SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("bluespawn")){
			  Config.getConfig(ConfigFile.Teleports).set("bluespawn" + ".Loc." +  "X", X);
		 	  Config.getConfig(ConfigFile.Teleports).set("bluespawn" + ".Loc." +  "Y", Y);
		 	  Config.getConfig(ConfigFile.Teleports).set("bluespawn" + ".Loc." +  "Z", Z);
		 	  Config.getConfig(ConfigFile.Teleports).set("bluespawn" + ".Loc." +  "Pitch", P);
		 	  Config.getConfig(ConfigFile.Teleports).set("bluespawn" + ".Loc." +  "Yaw", Yaw);
		 	 SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("mainlobby")){
			  Config.getConfig(ConfigFile.Teleports).set("mainlobby" + ".Loc." +  "X", X);
		 	  Config.getConfig(ConfigFile.Teleports).set("mainlobby" + ".Loc." +  "Y", Y);
		 	  Config.getConfig(ConfigFile.Teleports).set("mainlobby" + ".Loc." +  "Z", Z);
		 	  Config.getConfig(ConfigFile.Teleports).set("mainlobby" + ".Loc." +  "Pitch", P);
		 	  Config.getConfig(ConfigFile.Teleports).set("mainlobby" + ".Loc." +  "Yaw", Yaw);
		 	 SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("dead")){
			  Config.getConfig(ConfigFile.Teleports).set("death" + ".Loc." +  "X", X);
		 	  Config.getConfig(ConfigFile.Teleports).set("death" + ".Loc." +  "Y", Y);
		 	  Config.getConfig(ConfigFile.Teleports).set("death" + ".Loc." +  "Z", Z);
		 	  Config.getConfig(ConfigFile.Teleports).set("death" + ".Loc." +  "Pitch", P);
		 	  Config.getConfig(ConfigFile.Teleports).set("death" + ".Loc." +  "Yaw", Yaw);
		 	 SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("spectator")){
			  Config.getConfig(ConfigFile.Teleports).set("spectator" + ".Loc." +  "X", X);
		 	  Config.getConfig(ConfigFile.Teleports).set("spectator" + ".Loc." +  "Y", Y);
		 	  Config.getConfig(ConfigFile.Teleports).set("spectator" + ".Loc." +  "Z", Z);
		 	  Config.getConfig(ConfigFile.Teleports).set("spectator" + ".Loc." +  "Pitch", P);
		 	  Config.getConfig(ConfigFile.Teleports).set("spectator" + ".Loc." +  "Yaw", Yaw);
		 	  SaveTeleports(p);
		  }
		  else{
			  p.sendMessage(ChatColor.RED + "Invalid choice");
		  }
	  }
	  else if (args[0].equalsIgnoreCase("endround")){
		  if (Main.Game == Game.GAME)
		  DeathListener.WinningTeam(p.getName()  + " from the Admin", ChatColor.GOLD);
		  else
		  p.sendMessage(ChatColor.RED + "You can't stop a game that never started.");
	  }
	  else if (args[0].equalsIgnoreCase("startround")){
		  if (Main.Game == Game.PREGAME){
			  Bukkit.getScheduler().cancelTask(PreGameTimer.pgtimer);
			  try {
				Games.BeginGame();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  }
		  else
		  p.sendMessage(ChatColor.RED + "You can't start a game, that already started.");
	  }
	  else if (args[0].equalsIgnoreCase("flag")){
		  if (args[1].equalsIgnoreCase("blue")){
		  Config.getConfig(ConfigFile.Teleports).set("flag.blue.X", XLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flag.blue.Y", YLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flag.blue.Z", ZLook);
	 	  SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("red")){
		  Config.getConfig(ConfigFile.Teleports).set("flag.red.X", XLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flag.red.Y", YLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flag.red.Z", ZLook);
	 	  SaveTeleports(p);
		  }
	  }
	  else if (args[0].equalsIgnoreCase("flagstop")){
		  
		  if (args[1].equalsIgnoreCase("blue")){
		  if (args[2].equalsIgnoreCase("2")){
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.blue.X", XLook);
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.blue.Y", YLook);
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.blue.Z", ZLook);
		  SaveTeleports(p);
		  }
		  else if (args[2].equalsIgnoreCase("1")){
		  Config.getConfig(ConfigFile.Teleports).set("flagstop.blue.X", XLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flagstop.blue.Y", YLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flagstop.blue.Z", ZLook);
	 	  SaveTeleports(p);
			  }
		  }
		  else if (args[1].equalsIgnoreCase("red")){
		  if (args[2].equalsIgnoreCase("2")){
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.red.X", XLook);
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.red.Y", YLook);
		  Config.getConfig(ConfigFile.Teleports).set("flagstop2.red.Z", ZLook);
		  SaveTeleports(p);
		  }
		  else if (args[2].equalsIgnoreCase("1")){
		  Config.getConfig(ConfigFile.Teleports).set("flagstop.red.X", XLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flagstop.red.Y", YLook);
	 	  Config.getConfig(ConfigFile.Teleports).set("flagstop.red.Z", ZLook);
	 	  SaveTeleports(p);
			  }
		  }
	  }
	  else if (args[0].equalsIgnoreCase("horse")){
		  int Number;
		  if (args.length == 1){
			 	p.sendMessage(ChatColor.RED + "/mwa Horse " + ChatColor.GRAY + "<" + ChatColor.RED + "red" + ChatColor.GRAY + "/" + ChatColor.AQUA + "blue" + ChatColor.GRAY + "/" + ChatColor.GOLD + "random" + ChatColor.GRAY + ">");
		  }
		  else{
		  if (args[1].equalsIgnoreCase("red")){
			    if (!Config.getConfig(ConfigFile.Teleports).contains("horse.red"))
				Number = 1;
				else{
				Set<String> listred = Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.red").getKeys(false);	
				Number = listred.size() + 1;
				}
			    Config.getConfig(ConfigFile.Teleports).set("horse.red." + Number + "." +  "X", X);
			 	Config.getConfig(ConfigFile.Teleports).set("horse.red." + Number + "." +  "Y", Y);
			 	Config.getConfig(ConfigFile.Teleports).set("horse.red." + Number + "." +  "Z", Z);
			 	p.sendMessage(ChatColor.RED + "Saved horse spawnpoint #" + Number);
			 	SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("blue")){
			    if (!Config.getConfig(ConfigFile.Teleports).contains("horse.blue"))
				Number = 1;
				else{
				Set<String> listblue = Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.blue").getKeys(false);	
				Number = listblue.size() + 1;
				}
				Config.getConfig(ConfigFile.Teleports).set("horse.blue." + Number + "." +  "X", X);
			 	Config.getConfig(ConfigFile.Teleports).set("horse.blue." + Number + "." +  "Y", Y);
			 	Config.getConfig(ConfigFile.Teleports).set("horse.blue." + Number + "." +  "Z", Z);
			 	p.sendMessage(ChatColor.RED + "Saved horse spawnpoint #" + Number);
			 	SaveTeleports(p);
		  }
		  else if (args[1].equalsIgnoreCase("clear")){
			 	Config.getConfig(ConfigFile.Teleports).set("horse", null);
			 	p.sendMessage(ChatColor.RED + "Cleared all Horse locations");
			 	SaveTeleports(p);
		  }
		  }
	  }
	  return true; 	
	  }
  else if (cmd.getName().equalsIgnoreCase("mwcontrol") && (p.hasPermission("mw.control"))){
	  if (args.length == 0){
		  Message.P(p, "/mwcontrol <CTF/TDM/MIXED/TEAME>     " + ChatColor.GRAY + "| Restart & Set next gamemode");
	  }
	  else if (args.length == 1){
		  if (args[0].equalsIgnoreCase("ctf")){
			  DeathListener.WinningTeam("No ", ChatColor.RED);
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "The next gamemode has been forced to be:");
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "Capture the Flag");
			  Games.TotalVotes.put(Gamemode.CTF, 300);
		  }
		  else if (args[0].equalsIgnoreCase("tdm")){
			  DeathListener.WinningTeam("No ", ChatColor.RED);
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "The next gamemode has been forced to be:");
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "Team Deathmatch");
			  Games.TotalVotes.put(Gamemode.TDM, 300);
		  }
		  else if (args[0].equalsIgnoreCase("mixed")){
			  DeathListener.WinningTeam("No ", ChatColor.RED);
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "The next gamemode has been forced to be:");
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "Mixed (Capture the flag + Team Deathmatch)");
			  Games.TotalVotes.put(Gamemode.Mixed, 300);
		  }
		  else if (args[0].equalsIgnoreCase("teame")){
			  DeathListener.WinningTeam("No ", ChatColor.RED);
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "The next gamemode has been forced to be:");
			  plugin.getServer().broadcastMessage(ChatColor.GOLD + "Team Elimination");
			  Games.TotalVotes.put(Gamemode.TeamElimination, 300);
		  }
		  else{
			  Message.P(p, "Invalid choice");
		  }
	  }
	  return true;
  }
  return false;

}
public static void SaveTeleports(Player p){
	 	try {
	 			Config.getConfig(ConfigFile.Teleports).save(Config.getConfigFile(ConfigFile.Teleports));
	 			p.sendMessage(ChatColor.RED + "Point has been set!");
		} catch (IOException e) {
			  	p.sendMessage(ChatColor.RED + "Error while saving warp. Retry please.");
			  	e.printStackTrace();
		}
}
}