package kz.PvP.MedievalGames.Commands;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Vote;
import kz.PvP.MedievalGames.Enums.ChatType;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.IconMenu;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Message;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.comphenix.protocol.utility.MinecraftReflection;



public class PlayerCommands extends JavaPlugin implements CommandExecutor{
	
	public static Main plugin;
	   

	   public PlayerCommands(Main main) {
	 	PlayerCommands.plugin = main;
	 	}
		public static HashMap<String, IconMenu> MENUS = new HashMap<String, IconMenu>();
		public static HashMap<String, Integer> Count = new HashMap<String, Integer>();
		public static HashMap<Player, Player> LastMessaged = new HashMap<Player, Player>();
		//public static HashMap<Player, Player> LastMessaged2 = new HashMap<Player, String>();

		
		int count;
		int task;
@Override
public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args)
{
  final Player p = (Player)sender;
  if (cmd.getName().equalsIgnoreCase("mw")){
	  	  MainMenu(p);
		  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("feedback")){
	  if (args.length == 0){
		  Message.P(p, "Thank you for your intrest in helping us.");
		  Message.P(p, "In this feedback section, you may suggest ideas or report any bugs you experience, during your gameplay.");
		  Message.P(p, "/Feedback <Message>");
	  }
	  else{
	  		String msg = "";
	  		for (int a = 0; a <= args.length - 1; a++){
	  			msg = msg + " " + args[a];
	  		}
	  		Message.P(p, ChatColor.GOLD + "Thank you for your feedback!");
		    Message.P(p, "This message will be inspected by our team of staff, and any bugs/issues will be fixed ASAP.");
		    
		    
			PreparedStatement ps;
			try {
				ps = Mysql.PS("INSERT INTO MedievalWar_Feedback (User, Feedback, Time) VALUES (?, ?, ?)");
				ps.setInt(1, Mysql.GetUserID(p.getName()));
				ps.setString(2, msg);
				ps.setLong(3, (System.currentTimeMillis() / 1000L));
				ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
		    try {
				Mysql.dbSubmit("INSERT INTO MedievalWar_Feedback (User, Feedback, Time) VALUES ('" + Mysql.GetUserID(p.getName()) + "', '" + msg +  "', '" + (System.currentTimeMillis() / 1000L) + "')");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    */
		    
	  }
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("stats")){
	  if (args.length == 0){
		  Message.P(p, "You can display stats by typing the following:");
		  Message.P(p, "/Stats <Player>");
	  }
	  else{
		  String pname = args[0];
		  try {
			if (Mysql.GetUserID(pname) != 0){
			  ResultSet rss = null;
				rss = Mysql.dbGet("SELECT COUNT(*) as TOTALKILLS FROM DEATHS WHERE Killer = '" + Mysql.GetUserID(pname) + "'");
			  int Kills = 0;
			  if (Games.Kills.containsKey(pname))
			  Kills = Games.Kills.get(pname);
			  else{
				if (!Games.Kills.containsKey(pname) && rss.next()){
						Games.Kills.put(pname, rss.getInt("TOTALKILLS"));
						Kills = rss.getInt("TOTALKILLS");
				}
				}
			  
			  int Deaths = 1;
			  ResultSet rs = null;
				rs = Mysql.dbGet("SELECT COUNT(*) as TOTALDEATHS FROM DEATHS WHERE Victim = '" + Mysql.GetUserID(pname) + "'");
				rs.next();
				
				Deaths = rs.getInt("TOTALDEATHS");
			  
			  int ID;
			try {
				ID = Mysql.GetUserID(pname);
				Message.P(p, "==== Stats for " + Mysql.getUserName(ID) + " ====");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  Message.P(p, "Kills: " + Kills + "     Deaths: " + Deaths);
			  if (Deaths == 0)
			  Deaths = 1;
			  double KD = Kills/Deaths;
			  DecimalFormat df = new DecimalFormat();
			  df.setMaximumFractionDigits(2);
			  
			  Message.P(p, "Kill/Death Ratio: " + df.format(KD));
			  Message.P(p, "Points: " + Mysql.getPlayerPts(pname));
			  Message.P(p, "Current Rank: " + Global.ChatRank(pname, false));
  }
			  else{
				  Message.P(p, "Sorry, but this player has never played on this server.");
			  }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  return true;
  }
  	else if (cmd.getName().equalsIgnoreCase("help")){
	  p.sendMessage(" ");
	  p.sendMessage(" ");
	  p.sendMessage(ChatColor.GOLD + "==== " + ChatColor.GRAY + "Medieval War" + ChatColor.GOLD + " ====");
	  p.sendMessage(" ");
	  p.sendMessage(ChatColor.GOLD + "For MedievalWars help, type " + ChatColor.GREEN + "/mw");
	  p.sendMessage(ChatColor.GRAY + "Current Gamemode: " + ChatColor.YELLOW + Games.gamemode);
	  p.sendMessage(" ");
	  p.sendMessage(ChatColor.GRAY + "Objective: " + ChatColor.GREEN + Games.objectives);
	  p.sendMessage(" ");
  	  return true;
  	}
  	else if (cmd.getName().equalsIgnoreCase("tell")){
  		if (args.length == 0){
  	  		p.sendMessage(ChatColor.GOLD + "Sends a private message to a different user.");
  	  		p.sendMessage(ChatColor.GOLD + "/" + cmd.getLabel() + " <Player> <Message>");
  		}
  		else if (args.length == 1){
  	  		p.sendMessage(ChatColor.GOLD + "Sends a private message to a different user.");
  	  		p.sendMessage(ChatColor.GOLD + "/" + cmd.getLabel() + " <Player> <Message>");
  		}
  		else if (args.length >= 2){
  	  		String recp = args[0];
  	  		String msg = "";
  	  		boolean sent = false;
  	  		for (int a = 1; a <= args.length - 1; a++){
  	  			msg = msg + " " + args[a];
  	  		}
  	  		for (Player pl : Bukkit.getOnlinePlayers()){
  	  			if (pl.getName().toLowerCase() == recp.toLowerCase() && pl.getName() != p.getName()){
	  	  				p.sendMessage(ChatColor.GREEN + "[PM sent to " + ChatColor.GRAY + pl.getName() + "] " + msg);
	  	  				pl.sendMessage(ChatColor.GREEN + "[PM] from " + ChatColor.GRAY + p.getName() + ": " + msg);  	  				sent = true;
  	  	  				LastMessaged.put(p, pl);
  	  	  				LastMessaged.put(pl, p);
  	  	  				//LastMessaged2.put(pl, p.getName());
  	  	  				sent = true;
  	  			}
  	  		}
  	  		if (sent == false){
  	  	  		for (Player pl : Bukkit.getOnlinePlayers()){
  	  	  			if (pl.getName().toLowerCase().contains(recp.toLowerCase()) && pl.getName() != p.getName()){
  	  	  	  		if (sent == false){
  	  	  				p.sendMessage(ChatColor.GREEN + "[PM sent to " + ChatColor.GRAY + pl.getName() + "] " + msg);
  	  	  				pl.sendMessage(ChatColor.GREEN + "[PM from " + ChatColor.GRAY + p.getName() + "] " + msg);
  	  	  				sent = true;
  	  	  				LastMessaged.put(p, pl);
  	  	  				LastMessaged.put(pl, p);
  	  	  				//LastMessaged2.put(pl, p.getName());
  	  	  	  		}
  	  	  			}
  	  	  		}
  	  	  		if (sent == false){
  	  	  			p.sendMessage(ChatColor.RED + "No user, found with that name.");
  	  	  		}
  	  		}
  	  		
  	  		
  	  		
  	  		sent = false;
  	  		msg = "";
  		}
  		
	  return true;
  	}
  	else if (cmd.getName().equalsIgnoreCase("reply")){
  		if (args.length == 0){
  	  	  		p.sendMessage(ChatColor.GOLD + "Replies to the user you were last messaged from.");
  	  	  		p.sendMessage(ChatColor.GOLD + "/" + cmd.getLabel() + " <Message>");
  		}
  		else if (args.length >= 1){
  	  		String msg = "";
  	  		for (int a = 0; a <= args.length - 1; a++){
  	  	  			msg =  msg + " " + args[a];
  	  	  		}
  	  	  		if (LastMessaged.containsKey(p)){
  	  	  	  		Player last = LastMessaged.get(p);
  	  	  				p.sendMessage(ChatColor.GREEN + "[Replied to " + last.getName() + "] " + ChatColor.GOLD + msg);
  	  	  				last.sendMessage(ChatColor.GREEN + "[PM'd " + ChatColor.GOLD + p.getName() + "] " + msg);
  	  	  	  		
  	  	  	  		
  	  	  		}
  	  	  		else{
  	  	  			p.sendMessage(ChatColor.RED + "You do not have anyone to reply to.");
  	  	  		}
  	  	  		msg = "";
  		}
	  return true;
  	}
	  else if (cmd.getName().equalsIgnoreCase("leave")){
		  JoinListener.red.remove(p);
		  JoinListener.blue.remove(p);
		  if (p.getVehicle() != null)
		  p.eject();
		  Message.P(p, "You are now a spectator.");
		  if (Main.Game != Game.PREGAME){
		  Location spawn = Bukkit.getWorld("war").getSpawnLocation();
		  p.teleport(spawn);
		  Games.SpecJoin(p);
		  }
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("points")){
		  try {
			p.sendMessage(ChatColor.GREEN + "You have " + Mysql.getPlayerPts(p.getName()) + " points!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			p.sendMessage(ChatColor.RED + "Failed getting points. Contact Undeadkillz!");
		}
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("rtv")){
		  Vote.ShowVoteMenu(p);
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("vote")){
		  Vote.ShowVoteMenu(p);
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("chat")){
		  if (args.length == 0){
			  Message.P(p, "=== MedievalWars Chat Preferences ===");
			  Message.P(p, "/chat <p/t>");
			  Message.P(p, "p = Your message will appear for everyone.");
			  Message.P(p, "t = Your message will appear for your team only.");
		  }
		  else if (args.length == 1){
			  if (args[0].equalsIgnoreCase("p")){
				  Games.Chatting.put(p.getName(), ChatType.Public);
				  Message.P(p, "Your messages will now appear to everyone.");
			  }
			  else if (args[0].equalsIgnoreCase("t")){
				  Games.Chatting.put(p.getName(), ChatType.Private);
				  Message.P(p, "Your messages will now appear to your team only.");
			  }
			  else{
				  Message.P(p, "=== MedievalWars Chat Preferences ===");
				  Message.P(p, "/chat <p/t>      - Toggle");
				  Message.P(p, "p = Your message will appear for everyone.");
				  Message.P(p, "t = Your message will appear for your team only.");
			  }
		  }
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("notify")){
		  if (args.length == 1){
		  if (args[0].equalsIgnoreCase("on")){
			if (Games.DoNotNotify.contains(p.getName())){
			Message.P(p, ChatColor.GREEN + "You have enabled notifications");
			Games.DoNotNotify.remove(p.getName());
			}
			else
			Message.P(p, ChatColor.GREEN + "Notifications were already enabled.");
			
		  }
		  else if (args[0].equalsIgnoreCase("off")){
				if (!Games.DoNotNotify.contains(p.getName())){
					Message.P(p, ChatColor.RED + "You have disabled notifications");
					Games.DoNotNotify.add(p.getName());
					}
					else
					Message.P(p, ChatColor.RED + "Notifications were already disabled.");
		  }
		  }
		  else {
			p.sendMessage(ChatColor.GOLD + "/Notify " + ChatColor.GRAY + "<" + 
			ChatColor.GREEN + "on" + ChatColor.GRAY + "/" + ChatColor.RED + "off" + ChatColor.GRAY + ">" + ChatColor.GOLD + "- Toggle notifications");
			}
		  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("classhelp")){
	  if (args.length == 0){
		  p.sendMessage(ChatColor.GOLD + "======== " + ChatColor.GREEN + "Medieval War" + ChatColor.GOLD + " ========");
		  //TODO: Explain what classes are.
		  p.sendMessage(ChatColor.GOLD + "/Class - View/Choose accessible classes");
		  p.sendMessage(ChatColor.GOLD + "======== " + ChatColor.GREEN + "Medieval War" + ChatColor.GOLD + " ========");

	  }
	  else if (args.length == 1){
		  try {
			Kits.Choose(p, args[0]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("class")){
			try {
				OpenClassMenu(p);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("activate")){
	  try {
		ActivateMenuWithoutClass(p);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  return true;
}
  else if (cmd.getName().equalsIgnoreCase("upgrade")){
	  try {
		UpgradeMenuWithoutClass(p);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  return true;
}
  else if (cmd.getName().equalsIgnoreCase("team")){
	  try {
		OpenTeamMenu(p);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("tp")){
	  if (p.hasPermission("mw.tp")){
	  if (args.length == 0){
		  p.sendMessage(ChatColor.GREEN + "/Tp [Player] <Target>");
	  }
	  else if (args.length == 1){
		  String player = args[0].toLowerCase();
		  for (Player pl : Bukkit.getOnlinePlayers()){
			  if (pl.getName().toLowerCase().contains(player)){
				  p.teleport(pl);
				  break;
			  }
		  }
	  }
	  else if (args.length == 2){
		  String player = args[0].toLowerCase();
		  String target = args[1].toLowerCase();
		  for (Player pl : Bukkit.getOnlinePlayers()){
			  if (pl.getName().toLowerCase().contains(player)){
				  for (Player targetp : Bukkit.getOnlinePlayers()){
					  if (targetp.getName().toLowerCase().contains(target)){
						  pl.teleport(targetp);
						  break;
					  }
				  }
				  break;
			  }
		  }
	  }
	  else
		  p.sendMessage(ChatColor.GREEN + "/tp [Player] <Target>");
	  }
	  else
		  p.sendMessage(ChatColor.RED + "No permission to use this command.");
	  
	  
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("tpall")){
	  if (p.hasPermission("mw.tpall")){
		  for (Player pl : Bukkit.getOnlinePlayers()){
			  pl.teleport(p);
			  pl.sendMessage(ChatColor.GOLD + "You have been teleported to " + p.getName());
		  }
		  p.sendMessage(ChatColor.GOLD + "All players have been teleported to you.");
	  }
	  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("gm")){
	  if (p.hasPermission("mw.gamemode")){
	  // /gm <Type> <Player>
	  if (args.length == 0){
		  p.sendMessage(ChatColor.RED + "/Gamemode <0/1/2> [Player]");
	  }
	  else if (args.length == 1){
		  int gm = Integer.parseInt(args[0]);
		  if (gm == 0){
			  p.setGameMode(GameMode.SURVIVAL);
			  p.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to survival mode.");
		  }
		  else if (gm == 1){
			  p.setGameMode(GameMode.CREATIVE);
			  p.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to creative mode.");
		  }
		  else if (gm == 2){
			  p.setGameMode(GameMode.ADVENTURE);
			  p.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to adventure mode.");
		  }
		  else{
			  p.sendMessage(ChatColor.RED + "Invalid gamemode chosen.");
		  }
	  }
	  else if (args.length == 2){
		  int gm = Integer.parseInt(args[0]);
		  String player = args[1].toLowerCase();
		  Player pl = Bukkit.getPlayer(player);
		  if (pl != null){
		  if (gm == 0){
			  pl.setGameMode(GameMode.SURVIVAL);
			  pl.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to survival mode.");
		  }
		  else if (gm == 1){
			  pl.setGameMode(GameMode.CREATIVE);
			  pl.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to creative mode.");
		  }
		  else if (gm == 2){
			  pl.setGameMode(GameMode.ADVENTURE);
			  pl.sendMessage(ChatColor.GREEN + "Your gamemode has been changed to adventure mode.");
		  }
		  else{
			  pl.sendMessage(ChatColor.RED + "Invalid gamemode chosen.");
		  }
		  }
		  else{
			  p.sendMessage(ChatColor.RED + "Invalid user.");
		  }
	  }
	  else{
		  p.sendMessage(ChatColor.RED + "/Gamemode <0/1/2> [Player]");

	  }
	  
	  }
	  else{
		  p.sendMessage(ChatColor.RED + "You don't have permission");
	  }
	  return true;
  }
  return false;

}


public static void OpenTeamMenu(Player p) throws SQLException {
	if (Main.Gamemode != Gamemode.TeamElimination){
		// if(MENUS.containsKey(p)) {
		//	 MENUS.get(p).destroy();
		//	 MENUS.remove(p);
		// }
		int userid = Mysql.GetUserID(p.getName());
    IconMenu menu = new IconMenu("Team Selection | Choose A Team", p.getName(), 9, new IconMenu.OptionClickEventHandler() {
        @Override
        public void onOptionClick(final IconMenu.OptionClickEvent event) {
        	final Player p = event.getPlayer();
        	if (event.getPosition() == 2)
        	JoinListener.AssignPlayer(p, Team.Red);
        	else if (event.getPosition() == 4)
            JoinListener.AssignPlayer(p, Team.Random);
            else if (event.getPosition() == 6)
            JoinListener.AssignPlayer(p, Team.Blue);
        	
        	
			if (!Kits.KitChoice.containsKey(p)){
    		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    			public void run() {
    				try {
						OpenClassMenu(event.getPlayer());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
    		}, 3L);
			}
            event.setWillClose(true);
            event.setWillDestroy(false);
            

        }
    }, plugin)
    .setOption(0, new ItemStack(Material.WOOL, 1), ChatColor.GRAY + "Spectate", ChatColor.WHITE + "Click me, to spectate!")
    .setOption(2, new ItemStack(Material.WOOL, 1,(short)14), ChatColor.RED + "Red Team", ChatColor.RED + "Click me, to join the red team!")
    .setOption(4, new ItemStack(Material.WOOL, 1,(short)4), ChatColor.YELLOW + "Random Team", ChatColor.YELLOW + "Click me, to join any team!")
    .setOption(6, new ItemStack(Material.WOOL, 1,(short)11), ChatColor.AQUA + "Blue Team", ChatColor.AQUA + "Click me, to join the blue team!")
    .setOption(8, new ItemStack(Material.WOOL, 1), ChatColor.GRAY + "Spectate", ChatColor.WHITE + "Click me, to spectate!");
    if (MENUS.containsKey(p.getName())){
        MENUS.get(p.getName()).destroy();
        MENUS.remove(p.getName());
    }
        menu.open(p);
        MENUS.put(p.getName(), menu);
	}
	else {
		p.sendMessage(ChatColor.RED + "You cannot join any teams, meanwhile Team Elimination is active.");
	}
}


@SuppressWarnings("deprecation")
public static void OpenPlayersMenu(final Player p) throws SQLException {
		 Integer invsize = 54;

		 IconMenu menu = new IconMenu(ChatColor.GOLD + "Current Players Alive", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	p.teleport(Bukkit.getPlayerExact(ChatColor.stripColor(event.getName())));
	            	Message.P(p, "You have been teleported to " + ChatColor.stripColor(event.getName()));
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	    		    if (MENUS.containsKey(p.getName()))
	    		        MENUS.get(p.getName()).destroy();
	            }
	        }, plugin);
		 int pos = 0;
		 int maxpos = 27;
		 for (Player player : JoinListener.red){
			 if (pos == maxpos)
				 break;
					ArrayList<String> container = new ArrayList<String>();
					String KitName = "";
					if (!Kits.KitRound.containsKey(player))
					KitName = "knight";
					else
					KitName = Kits.KitRound.get(player);
						
					ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitName);
					
					String Item = kit.getString("ITEMMENU");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					int TotalBandages = 0;
		        	for (ItemStack is : player.getInventory().getContents()){
		        		if (is != null && is.getTypeId() == 399){
		        			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() == true && is.getItemMeta().getDisplayName().toLowerCase().contains("player bandaid"))
		        				TotalBandages = is.getAmount() + TotalBandages;
		        		}
		        	}
		        	int Kills = 0;
		        	if (Games.Kills.containsKey(player.getName()))
		        		Kills = Games.Kills.get(player.getName());
		        	
					container.add(ChatColor.GRAY + "Bandages: " + TotalBandages);
					container.add(ChatColor.GRAY + "Health: " + Math.round(player.getHealth() / 2));
					container.add(ChatColor.RED + "All-time Kills: " + Kills);
					container.add(ChatColor.GOLD + "Kill Rank: " + Global.ChatRank(player.getName(), false));
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					
					boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
					
					
					if (fakeenchant == true){
					i = MinecraftReflection.getBukkitItemStack(i);
					i.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
					}
					menu.setOption(pos, i, ChatColor.RED + player.getName(), info);
					pos++;
					container.clear();
		 }
		 pos = 27;
		 maxpos = 54;
		 for (Player player : JoinListener.blue){
			 if (pos == maxpos)
				 break;
				ArrayList<String> container = new ArrayList<String>();
				String KitName = "";
				if (!Kits.KitRound.containsKey(player))
				KitName = "knight";
				else
				KitName = Kits.KitRound.get(player);
					
				ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitName);
				
				String Item = kit.getString("ITEMMENU");
				int itemid = 0;
				ItemStack i = null;
				short durability = 0;
				
				if (Item.contains(":")){
				String[] Icon = Item.split(":");
				itemid = Integer.parseInt(Icon[0]);
				durability = (short) Integer.parseInt(Icon[1]);
				i = new ItemStack(itemid, 1,
						durability);
				}
				else{
					i = new ItemStack(Integer.parseInt(Item), 1);
				}
				int TotalBandages = 0;
	        	for (ItemStack is : player.getInventory().getContents()){
	        		if (is != null && is.getTypeId() == 399){
	        			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() == true && is.getItemMeta().getDisplayName().toLowerCase().contains("player bandaid"))
	        				TotalBandages = is.getAmount() + TotalBandages;
	        		}
	        	}
	        	int Kills = 0;
	        	if (Games.Kills.containsKey(player.getName()))
	        		Kills = Games.Kills.get(player.getName());
	        	
				container.add(ChatColor.GRAY + "Bandages: " + TotalBandages);
				container.add(ChatColor.GRAY + "Health: " + Math.round(player.getHealth() / 2));
				container.add(ChatColor.RED + "All-time Kills: " + Kills);
				container.add(ChatColor.GOLD + "Kill Rank: " + Global.ChatRank(player.getName(), false));
				if (Item.contains(",")){
					String[] itemench = Item.split(",");
					i.addUnsafeEnchantment(
						Enchantment.getById(Integer.parseInt(itemench[0])),
						Integer.parseInt(itemench[1]));
				}
				String[] info = new String[container.size()];
				info = container.toArray(info);
				
				
				boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
				
				
				if (fakeenchant == true){
				i = MinecraftReflection.getBukkitItemStack(i);
				i.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
				}
				
				menu.setOption(pos, i, ChatColor.AQUA + player.getName(), info);
				pos++;
				container.clear();
	 }
					
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		        
		        
	  }


@SuppressWarnings("deprecation")
public static void OpenActivateMenu(final Player p, final String Class1) throws SQLException {
	  Set<String> classs = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(Class1.toLowerCase()).getKeys(false);

		 Integer invsize = 27;

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }

		 IconMenu menu = new IconMenu(ChatColor.RED + "Activate Menu  Pts: " + Mysql.getPlayerPts(p.getName()), p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	try {
						Mysql.ActivateUpgrade(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()),Class1.toLowerCase());
						} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								OpenActivateMenu(p, Class1);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	    
	            	
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);

		 for (String upgrade : classs){
			 try {
				  ConfigurationSection upgrades = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(Class1.toLowerCase() + "." + upgrade);
					ArrayList<String> container = new ArrayList<String>();
					String desc = upgrades.getString("Description");
					if (desc.contains("<endline>")){
						String[] splitinfo = desc.split("<endline>");
						for (String descline : splitinfo){
							container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + descline);
						}
						
						
					}
					else
					container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + desc);
					
					
					
					
					if (Mysql.GetUserActiveUps(p.getName()).contains(" " + Class1.toLowerCase() + ":" + upgrades.getName().toLowerCase())){
						container.add(ChatColor.GREEN + "Activated");
						container.add(ChatColor.GRAY + "Click to deactivate");
					}
					else{
						if (!Mysql.GetUserUps(p.getName()).contains(" " + Class1.toLowerCase() + ":" + upgrades.getName().toLowerCase()))
							container.add(ChatColor.RED + "Upgrade not purchased");
						else{
						container.add(ChatColor.GRAY + "Inactive");
						container.add(ChatColor.GRAY + "Click to activate");
						}
						}
					
					String Item = upgrades.getString("IconItem");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					
					
					int pos = upgrades.getInt("Position");

					Material.getMaterial(itemid);
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					
					boolean fakeenchant = upgrades.getBoolean("EnchantFakeIcon");
					if (fakeenchant == true){
					i = MinecraftReflection.getBukkitItemStack(i);
					i.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
					}
					
					
					
					menu.setOption(pos, i, ChatColor.GREEN + upgrades.getName(), info);
					container.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
	  }


public static void OpenClassMenu(Player p) throws SQLException {
	  Set<String> kits = Config.getConfig(ConfigFile.Kits).getKeys(false);
		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

		// if(MENUS.containsKey(p)) {
		//	 MENUS.get(p).destroy();
		//	 MENUS.remove(p);
		// }
		 int userid = Mysql.GetUserID(p.getName());
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class selection menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	try {
						Kits.Choose(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);
		 OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}


public static void UpgradeMenuWithoutClass(final Player p) throws SQLException{
	  Set<String> kits = Config.getConfig(ConfigFile.Kits).getKeys(false);

		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }

		 int userid = Mysql.GetUserID(p.getName());
		 
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class upgrade menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								UpgradeMenuWithClass(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	                
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);

		 OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}



public static void ActivateMenuWithoutClass(final Player p) throws SQLException{
	  Set<String> kits = Config.getConfig(ConfigFile.Kits).getKeys(false);

		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }
		 int userid = Mysql.GetUserID(p.getName());
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class activate menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								OpenActivateMenu(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	                
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	    

	            }
	        }, plugin);
OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
if (MENUS.containsKey(p.getName())){
    MENUS.get(p.getName()).destroy();
    MENUS.remove(p.getName());
}
    menu.open(p);
    MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}



@SuppressWarnings("deprecation")
private static void OpenMenu(IconMenu menu, int userid, Integer invsize, Set<String> kits) {

	 Integer mypos = 0;
	 Integer othpos = 1;
	 for(String kitname : kits) {	
		 try {

				ArrayList<String> container = new ArrayList<String>();
				ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(kitname.toLowerCase());
				List<String> kititems = kit.getStringList("ITEMS");
				for (String item : kititems) {
					String[] oneitem = item.split(",");

					String itemstring = null;
					Integer id = null;
					Integer amount = null;
					String enchantment = null;
					String enchantment1 = null;
					String enchantment2 = null;
					String ench_numb = null;
					String ench_numb1 = null;
					String ench_numb2 = null;
					
					if (oneitem[0].contains(":")) {
						String[] ITEM_ID = oneitem[0].split(":");
						id = Integer.valueOf(Integer.parseInt(ITEM_ID[0]));
						amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					} else {
						id = Integer.valueOf(Integer.parseInt(oneitem[0]));
						amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					}

					String[] I = null;
					if (item.contains(">")){
						I = item.split(">");
					}
						
					String name = Material.getMaterial(id).toString();
					if (I.length >= 3 && I[2].toLowerCase().contains("true")){
						name = I[1];
					}
					if (amount > 1){
					itemstring = " - "
							+ amount
							+ "x "
							+ name
									.replace("_", " ").toLowerCase();
					}
					else{
						itemstring = " - "
								+ name
										.replace("_", " ").toLowerCase();
						}
					
					
					
					
					if (oneitem.length >= 8) {
						enchantment = Enchantment
								.getById(Integer.parseInt(oneitem[2])).getName()
								.toLowerCase();
						ench_numb = oneitem[3];
						
						enchantment1 = Enchantment
								.getById(Integer.parseInt(oneitem[4])).getName()
								.toLowerCase();
						ench_numb1 = oneitem[5];
						
						enchantment2 = Enchantment
								.getById(Integer.parseInt(oneitem[6])).getName()
								.toLowerCase();
						ench_numb2 = oneitem[7];
						
						
						itemstring = itemstring + " with " + enchantment + " "
								+ ench_numb + ",\n " + enchantment1 + " " + ench_numb1 + " and " + enchantment2 + " " + ench_numb2;
					}
					else if (oneitem.length >= 6) {
						enchantment = Enchantment
								.getById(Integer.parseInt(oneitem[2])).getName()
								.toLowerCase();
						ench_numb = oneitem[3];
						
						enchantment1 = Enchantment
								.getById(Integer.parseInt(oneitem[4])).getName()
								.toLowerCase();
						ench_numb1 = oneitem[5];
						
						
						itemstring = itemstring + " with " + enchantment + " "
								+ ench_numb + " and " + enchantment1 + " " + ench_numb1;
					}
					
					else if (oneitem.length >= 4) {
						enchantment = Enchantment
								.getById(Integer.parseInt(oneitem[2])).getName()
								.toLowerCase();
						ench_numb = oneitem[3];
						
						itemstring = itemstring + " with " + enchantment + " "
								+ ench_numb;
					}
					itemstring = itemstring.replace("_", " ");
					
					if (itemstring.length() >= 36){
					String itemstring1 = itemstring.substring(0, 36);
					String itemstring2 = itemstring.substring(36, itemstring.length());

					container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + itemstring1);
					container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + itemstring2);
					}
					else
					container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + itemstring);
				}

				boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
				
				Integer itemid = kit.getInt("ITEMMENU");
				Material kitem = Material.getMaterial(itemid);
				
				ItemStack is = new ItemStack(kitem, 1);
				
				container.add(" ");
				container.add(ChatColor.GOLD + "Player bandages: " + kit.getString("PlayerBandage"));
				container.add(ChatColor.GOLD + "Horse bandages: " + kit.getString("HorseBandage"));
				container.add(" ");
				container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + "= = Abilities = =");
				List<String> pots = kit.getStringList("POTION");
				for(String pot : pots) {	
					if (pot != null & pot != "") {
						if (!pot.equals(0)) {
							String[] potion = pot.split(",");
							if (Integer.parseInt(potion[0]) != 0) {
								PotionEffectType pt = PotionEffectType.getById(Integer.parseInt(potion[0]));
								String name = pt.getName().toLowerCase().replace("_", " ");
								name = name.substring(0, 1).toUpperCase() + name.substring(1);
								if (Integer.parseInt(potion[1]) == 0) {
									name += " (Duration: Permenant)";
								} else {
									name += " (Duration: "+potion[1]+" sec)";
								}
								container.add(ChatColor.GRAY + " * " + name);
							}
						}
					}
				}
				if (kit.contains("Abilities")){
				List<String> abilities = kit.getStringList("Abilities");
				for (String ability : abilities)
				container.add(ChatColor.GRAY + " * " + ability);
				container.add(" ");
				}
				if (kit.contains("Strengths")){
				container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + "= = Strengths = =");
				List<String> strengths = kit.getStringList("Strengths");
				for (String strength : strengths)
					container.add(ChatColor.GRAY + " * " +  strength);
				}
				container.add(" ");
				String paid = "";
				if (kit.getBoolean("Paid"))
					paid = "Paid";
				else
					paid = "Free";
				container.add("Type: " + paid);

				
				
				
				
				
				
				if (fakeenchant == true){
				is = MinecraftReflection.getBukkitItemStack(is);
				is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
				}
				if (kit.getBoolean("Paid")){
					//Paid Kit
					String[] info = new String[container.size()];
				    info = container.toArray(info);

					menu.setOption(invsize - othpos, is, ChatColor.GREEN + kitname, info);
					mypos++;
					othpos++;
				}
				else{
					//Free Kit
					String[] info = new String[container.size()];
				    info = container.toArray(info);

					menu.setOption(mypos, is, ChatColor.GREEN + kitname, info);
					mypos++;
				}

				/*} else {
					 
					String[] info = new String[container.size()];
				    info = container.toArray(info);

					menu.setOption(invsize - othpos, new ItemStack(kitem, 1), ChatColor.RED + kitname, info);
					othpos++;
				}*/
			container.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	 }
}


@SuppressWarnings("deprecation")
private static void UpgradeMenuWithClass(final Player p, final String CClass) throws SQLException {
	  Set<String> classs = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(CClass.toLowerCase()).getKeys(false);

		 Integer invsize = 27;

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }

			  IconMenu menu = new IconMenu(ChatColor.GOLD + "Class upgrade | Pts: " + Mysql.getPlayerPts(p.getName()), p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
			        public void onOptionClick(IconMenu.OptionClickEvent event) {
			        	try {
							Mysql.BuyUpgrade(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()), Config.getConfig(ConfigFile.Upgrades).getInt(CClass.toLowerCase() + "." + ChatColor.stripColor(event.getName()) + ".Price"), CClass.toLowerCase());
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		                event.setWillClose(true);
		                event.setWillDestroy(false);
			        }
			    }, plugin);

		 for (String upgrade : classs){
			 try {
				  ConfigurationSection upgrades = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(CClass.toLowerCase() + "." + upgrade);
					ArrayList<String> container = new ArrayList<String>();
					String desc = upgrades.getString("Description");
					if (desc.contains("<endline>")){
						String[] splitinfo = desc.split("<endline>");
						for (String descline : splitinfo){
							container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + descline);
						}
						
						
					}
					else
					container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + desc);
					
					
					
					
					if (Mysql.GetUserUps(p.getName()).contains(" " + CClass.toLowerCase() + ":" + upgrades.getName().toLowerCase())){
						container.add(ChatColor.GREEN + "Already owned");
					}
					else{
						container.add(ChatColor.GOLD + "Price: " + upgrades.getInt("Price") + " Points");
					}
					
					String Item = upgrades.getString("IconItem");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					
					
					int pos = upgrades.getInt("Position");

					Material.getMaterial(itemid);
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					boolean fakeenchant = upgrades.getBoolean("EnchantFakeIcon");
					if (fakeenchant == true){
					i = MinecraftReflection.getBukkitItemStack(i);
					i.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
					}
					menu.setOption(pos, i, ChatColor.GREEN + upgrades.getName(), info);
					container.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}





public static void MainMenu(Player p) {
	  p.sendMessage(ChatColor.GOLD + "==== " + ChatColor.GRAY + "Medieval War" + ChatColor.GOLD + " ====");
	  p.sendMessage(ChatColor.GOLD + "/Team " + ChatColor.GRAY + "<" + ChatColor.RED + "red" + ChatColor.GRAY + "/" + ChatColor.AQUA + "blue" + ChatColor.GRAY + "/" + ChatColor.GOLD + "random" + ChatColor.GRAY + ">");
	  p.sendMessage(ChatColor.GOLD + "/Points         - View Personal Points");
	  p.sendMessage(ChatColor.GOLD + "/Class         - Class selection menu");
	  p.sendMessage(ChatColor.GOLD + "/Upgrade " + ChatColor.GRAY + "<" + ChatColor.GREEN + "ClassName" + ChatColor.GRAY + "> " + ChatColor.GOLD + "         - To upgrade class");
	  p.sendMessage(ChatColor.GOLD + "/Activate" + ChatColor.GRAY + "<" + ChatColor.GREEN + "ClassName" + ChatColor.GRAY + "> " + ChatColor.GOLD + "         - Activate Upgrade menu");
	  p.sendMessage(ChatColor.GOLD + "/Notify " + ChatColor.GRAY + "<" + 
	  ChatColor.GREEN + "on" + ChatColor.GRAY + "/" + ChatColor.RED + "off" + ChatColor.GRAY + ">" + ChatColor.GOLD + "         - Toggle notifications");
	  p.sendMessage(ChatColor.GOLD + "/Leave         - Leave current game & team");
	  p.sendMessage(ChatColor.GOLD + "==== " + ChatColor.GRAY + "Medieval War" + ChatColor.GOLD + " ====");
}

}
