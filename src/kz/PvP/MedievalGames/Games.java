package kz.PvP.MedievalGames;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import kz.PvP.MedievalGames.Commands.PlayerCommands;
import kz.PvP.MedievalGames.Enums.ChatType;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.DeathListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.Timers.EndGameTimer;
import kz.PvP.MedievalGames.Timers.PreGameTimer;
import kz.PvP.MedievalGames.Timers.PreparingTimer;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Message;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;
import org.kitteh.tag.TagAPI;



public class Games {
	public static Main plugin;
	
	
	public Games(Main mainclass) {
		plugin = mainclass;
		}
	public static HashMap<String, Integer> TeamKills = new HashMap<String, Integer>();// FullTeam Kills	
	public static HashMap<String, ChatType> Chatting = new HashMap<String, ChatType>();// FullTeam Kills	
	public static TreeMap<String, Integer> Kills = new TreeMap<String, Integer>(String.CASE_INSENSITIVE_ORDER);// FullTeam Kills	
	public static int Gamesrun = 0;
	// Organized Variables
	
	// TeamDeathmatch only Variables
	//public static HashMap<String, Integer> BlueKills = new HashMap<String, Integer>();// Player kills on Blue team
	//public static HashMap<String, Integer> RedKills = new HashMap<String, Integer>();// Player kills on Red team
	
	// Capture the flag only Variables
	public static Location BlueFlagLoc;
	public static Location RedFlagLoc;
	
	public static Player BlueFlagHolder;
	public static Player RedFlagHolder;
	
	public static int RedFlagCaps = 0;
	public static int BlueFlagCaps = 0;
	// Team Elimination only variables
	
	
	// Global Variables
	public static HashMap<UUID, String> BlueHorse = new HashMap<UUID, String>();// Player kills on Red team
	public static HashMap<UUID, String> RedHorse = new HashMap<UUID, String>();// Player kills on Red team
	public static Location redspawn = Bukkit.getWorlds().get(0).getSpawnLocation();// To make code shorter and cleaner we move the variable up here, so we don't have to declare it in both if statements
	public static Location bluespawn = Bukkit.getWorlds().get(0).getSpawnLocation();// To make code shorter and cleaner we move the variable up here, so we don't have to declare it in both if statements
	
	// None round related variables (For example AFK checks and sort of lists)
	public static ArrayList<String> NotAttacked = new ArrayList<String>();
	public static ArrayList<String> DoNotNotify = new ArrayList<String>();
	public static ArrayList<String> Invincibility = new ArrayList<String>();
	public static HashMap<String, Location> IdleUsers = new HashMap<String, Location>();// FullTeam Kills
	public static HashMap<Gamemode, Integer> TotalVotes = new HashMap<Gamemode, Integer>();// FullTeam Kills

	// Number settings
	static int MinRed = 0;// Minumum on red team to begin
	static int MinBlue = 0;// Minimum on blue team to begin
	public static int ScoreMixed = 200;// Score needed to end round in Mixed.
	public static final int FlagCap = 10;// Flag Points for cap in Mixed

	public static final long IdleTime = 20;

	private static final int PointsPerKill = 1;
	public static final int TeamBal = 1;
	public static int EndGameTime = 0;
	public static Integer TimerDespawn = 45;// Flag despawn timer | Mixed & CTF
	public static int FlagsToCap = 3;// Flag captures needed to win (TDM Gamemode)
	public static int KillsTDM = 100;// Kills needed to win Gamemode in TDM
	// Strings Must be dynamic
	public static String gamemode = "";
	public static String objectives = "";
	
	
	
	
	
	
	public static int WaitingTimer = 60;
	
	public static HashMap<String, Gamemode> VoteChoices = new HashMap<String, Gamemode>();// FullTeam Kills

	
	
	
	
	
	
	
	public static void BeginGame() throws SQLException {
		Gamesrun++;
		RefreshTags();
    	plugin.getServer().getWorld("war").getChunkAt(Global.GetLocOfString("redspawn.Loc", "war")).load();
    	plugin.getServer().getWorld("war").getChunkAt(Global.GetLocOfString("bluespawn.Loc", "war")).load();
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				for (Player pl : plugin.getServer().getOnlinePlayers())
				Global.updateTab(pl);
			}
		}, 6);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				SpawnAllHorses();
			}
		}, 20 * 10);
		
		
		if ((JoinListener.red.size() >= MinRed) && (JoinListener.blue.size() >= MinBlue) && (Main.Game == Game.PREGAME)){
		Main.Game = Game.GAME;
		
		Main.Gamemode = Gamemode.CTF;
		int old = 0;
		for (Entry<String, Gamemode> gm : Games.VoteChoices.entrySet()){
			if (!Games.TotalVotes.containsKey(gm.getKey()))
			Games.TotalVotes.put(gm.getValue(), 0);
			Games.TotalVotes.put(gm.getValue(), Games.TotalVotes.get(gm.getValue()) + 1);
		}
		for (Entry<Gamemode, Integer> entry : Games.TotalVotes.entrySet()){
		int in = entry.getValue();
		Gamemode gm = entry.getKey();
		if(in > old){
			Main.Gamemode = gm;
			}
		}
		TotalVotes.clear();
		
		if (Main.Gamemode == Gamemode.TeamElimination){
			for (Player pl : Bukkit.getOnlinePlayers()){
				if (!JoinListener.red.contains(pl) && (!JoinListener.blue.contains(pl)) || JoinListener.spectators.contains(pl)){
					JoinListener.spectators.remove(pl);
					JoinListener.AssignPlayer(pl, Team.Random);
					PlayerCommands.OpenClassMenu(pl);
				}
			}
			
			
		}
		
		
		if (Main.Gamemode == Gamemode.CTF){
			gamemode = "Capture the flag (CTF)";
			objectives = Message.Replacer(Message.CTFDesc, "" + FlagsToCap, "<flags>");
			EndGameTime = 60 * 20;
		}
		else if (Main.Gamemode == Gamemode.Mixed){
			gamemode = "Mixed (CTF + TDM)";
			objectives = Message.Replacer(Message.MixedDesc, "" + FlagCap, "<points>");
			EndGameTime = 60 * 35;
		}
		else if (Main.Gamemode == Gamemode.TDM){
			gamemode = "Team Deathmatch (TDM)";
			objectives = Message.Replacer(Message.TDMDesc, "" + PointsPerKill, "<perkill>");
			EndGameTime = 60 * 25;
		}
		else if (Main.Gamemode == Gamemode.TeamElimination){
			gamemode = "Team Elimination (Last team standing)";
			objectives = Message.Replacer(Message.TeamEliminationDesc, "" + PointsPerKill, "<perkill>");
			EndGameTime = 60 * 10;
			}
		
		EndGameTimer.BeginTimer();
		
		TeamKills.put("red", 0);
		TeamKills.put("blue", 0);
		
		
		plugin.getServer().getWorld("war").setTime(0);
		for (final Player p : Bukkit.getOnlinePlayers()){
		     	if ((JoinListener.blue.contains(p)) || (JoinListener.red.contains(p))){
						Start(p, true);
		     		}
		     	else{
		     			SpecJoin(p);
		     		}
		}
		
		if(Main.Gamemode == Gamemode.Mixed || Main.Gamemode == Gamemode.CTF){
		/* Flag set-up  */
		CTF.SpawnFlag(Global.GetLocOfString("flag.red", "war"), "red");
		CTF.SpawnFlag(Global.GetLocOfString("flag.blue", "war"), "blue");
		/*  END OF Flag Set-up  */
		}
		
		TeamKills.put("red", 0);
		TeamKills.put("blue", 0);
		Message.G(Message.PrepareTimer);
		}
	else if (Main.Game == Game.PREGAME){
		Message.G(Message.NotEnoughOn);
		PreGameTimer.BeginTimer();
		}
		
		
		// We will change variables depending on the Gamemode chosen
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	private static void SpawnAllHorses() {
		if (Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.blue") != null){
		Set<String> listblue = Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.blue").getKeys(false);
		for (String a : listblue){
			Global.SpawnBlueHorse(a);
		}
		}
		if (Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.red") != null){
		Set<String> listred = Config.getConfig(ConfigFile.Teleports).getConfigurationSection("horse.red").getKeys(false);
		for (String a : listred){
			Global.SpawnRedHorse(a);
		}
		}
	}
	
	
	
	public static void GameBegun() throws SQLException {
	}
	public static void RefreshTags() {
		for (Player p : Bukkit.getOnlinePlayers()){
			TagAPI.refreshPlayer(p);
		}
	}
	static org.bukkit.scoreboard.Scoreboard board;
	static ScoreboardManager manager;
	public static Objective objective;
	
	public static void SpecJoin(Player p) {
		
		Message.P(p, Message.GameBegunSpec);
		if (!JoinListener.spectators.contains(p))
		JoinListener.spectators.add(p);
		
		for (Player pl : Bukkit.getOnlinePlayers()){
			pl.hidePlayer(p);
		}
		
		
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		
		p.setDisplayName(ChatColor.GRAY + p.getName());
		
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		
		if (JoinListener.Objectives.containsKey(p.getName())){
			JoinListener.Objectives.remove(p.getName());
			JoinListener.Objectives.put(p.getName(), objective);
		}
		if (JoinListener.Boards.containsKey(p.getName())){
			JoinListener.Boards.remove(p.getName());
			JoinListener.Boards.put(p.getName(), board);
		}
		
		
		Location spawn = Bukkit.getWorld("war").getSpawnLocation();// To make code shorter and cleaner we move the variable up here, so we don't have to declare it in both if statements
		if (Main.Game != Game.PREGAME){
		final Location spectator = Global.GetLocOfString("spectator.Loc", "war");
		p.teleport(spectator);
		}
		
		p.getInventory().clear();
		p.setHealth(20.0);
		p.setGameMode(GameMode.SURVIVAL);
		p.setFoodLevel(20);
		p.setExp(0);
		p.setAllowFlight(true);
		Global.GiveTools(p, true);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				for (Player pl : plugin.getServer().getOnlinePlayers())
				Global.updateTab(pl);
			}
		}, 6);
	}
	@SuppressWarnings("deprecation")
	public static void Start(final Player p, boolean TpToSpawn) throws SQLException {

	JoinListener.spectators.remove(p);
	
	for (Player pl : JoinListener.spectators){
		if (!pl.canSee(p))
		pl.showPlayer(p);
		if (p.canSee(pl))
		p.hidePlayer(pl);
	}
	for (Player pl : JoinListener.red){
		if (!pl.canSee(p))
		pl.showPlayer(p);
		if (!p.canSee(pl))
		p.showPlayer(pl);
	}
	for (Player pl : JoinListener.blue){
		if (!pl.canSee(p))
		pl.showPlayer(p);
		if (!p.canSee(pl))
		p.showPlayer(pl);
	}
	
	for (PotionEffect pe : p.getActivePotionEffects())
		p.removePotionEffect(pe.getType());
	if (!Kits.KitChoice.containsKey(p) && !DoNotNotify.contains(p.getName())){
	Message.P(p, Message.DoNotNotify);
	}
	if (DeathListener.PrevDeaths.containsKey(p.getName())){
	if (DeathListener.PrevDeaths.get(p.getName()) == 3){
		Message.P(p, Message.GivenBuffForDeath3);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 30, 0));
	}
	else if (DeathListener.PrevDeaths.get(p.getName()) == 5){
		Message.P(p, Message.GivenBuffForDeath5);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 75, 0));

	}
	else if (DeathListener.PrevDeaths.get(p.getName()) == 10){
		Message.P(p, Message.GivenBuffForDeath10);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 180, 0));
	}
	}
	
	
	
	Invincibility.remove(p.getName());
	NotAttacked.add(p.getName());
	Invincibility.add(p.getName());
	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
			Invincibility.remove(p.getName());
		}
	}, 20L * 10);
	
	
	
	
	if (JoinListener.red.contains(p)){
	p.setDisplayName(ChatColor.RED + p.getName() + ChatColor.GRAY);
	
	}
	if (JoinListener.blue.contains(p)){
	p.setDisplayName(ChatColor.AQUA + p.getName() + ChatColor.GRAY);
	}
	if (JoinListener.red.contains(p)){
	redspawn = Global.GetLocOfString("redspawn.Loc", "war");
	if (TpToSpawn)
	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
			p.teleport(redspawn);
			if (p.getWorld().getBlockAt(p.getLocation()) != null)
				redspawn.add(0, 2, 0);
			p.teleport(redspawn);
		}
	}, 1L);
	}
	else if (JoinListener.blue.contains(p)){
	bluespawn = Global.GetLocOfString("bluespawn.Loc", "war");
	if (TpToSpawn)
	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
			p.teleport(bluespawn);
			if (p.getWorld().getBlockAt(p.getLocation()) != null)
				bluespawn.add(0, 2, 0);
			p.teleport(bluespawn);
		}
	}, 1L);
	
	}
	
	
	
	p.setFlying(false);
	p.setAllowFlight(false);
	p.setHealth(20.0);
	p.setFoodLevel(20);
	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	public void run() {
		p.setHealth(20.0);
		p.setFoodLevel(20);
		}
	}, 5L);

	Kits.GiveKit(p);
	p.updateInventory();
	for (Player pl : plugin.getServer().getOnlinePlayers())
	Global.updateTab(pl);
	}
	
	
}
