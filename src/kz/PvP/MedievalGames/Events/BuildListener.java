package kz.PvP.MedievalGames.Events;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.RewardType;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Global;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class BuildListener extends JavaPlugin implements Listener{
	public static Main plugin;
	public static ArrayList<String> Blocks = new ArrayList<String>();

	
	public BuildListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	/*
	private WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	    return (WorldGuardPlugin) plugin;
	}
	
	
	WorldGuardPlugin worldGuard = getWorldGuard();
	*/
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlacingFlag (BlockPlaceEvent e) throws SQLException{
		
		
		
		
		if (!e.isCancelled()){
			if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){
			
				if (e.getBlock().getTypeId() == 159){
					Location blockloc = e.getBlock().getLocation();
					blockloc = new Location(e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ());
					Location redflag = Global.GetLocOfString("flagstop.red", "war");// BLUE BASE
					Location redflag2 = Global.GetLocOfString("flagstop2.red", "war");// BLUE BASE
					Location blueflag = Global.GetLocOfString("flagstop.blue", "war");// RED BASE
					Location blueflag2 = Global.GetLocOfString("flagstop2.blue", "war");// RED BASE
					Location redflagnew = Global.GetLocOfString("flag.red", "war");
					Location blueflagnew = Global.GetLocOfString("flag.blue", "war");
					Location loc = e.getBlock().getLocation();
					
						if (Games.BlueFlagHolder == e.getPlayer() && 
								
								(
								(blueflag.getBlockX() == blockloc.getBlockX()) &&
								(blueflag.getBlockY() == blockloc.getBlockY()) &&
								(blueflag.getBlockZ() == blockloc.getBlockZ())) ||
								(
										(blueflag2.getBlockX() == blockloc.getBlockX()) &&
										(blueflag2.getBlockY() == blockloc.getBlockY()) &&
										(blueflag2.getBlockZ() == blockloc.getBlockZ()))
								){//Red player
							plugin.getServer().broadcastMessage(ChatColor.RED + "Red team has gained " + Games.FlagCap + " points for capturing the flag!");
							Games.TeamKills.put("red", Games.TeamKills.get("red") + Games.FlagCap);		
							Mysql.modifyUserPts(e.getPlayer().getName(), Games.FlagCap, RewardType.FlagCap);
							e.getPlayer().sendMessage(ChatColor.GOLD + "You have earned " + Games.FlagCap + " points for capturing the flag!");
							e.getBlock().setTypeId(0);
							Block blueflagblock = plugin.getServer().getWorld("war").getBlockAt(blueflagnew);
							blueflagblock.setTypeId(159);
							blueflagblock.setData((byte)11);// We spawn a blue colored stained clay
							Games.BlueFlagLoc = blueflagnew;
							Games.BlueFlagHolder = null;
						}
						else if (Games.RedFlagHolder == e.getPlayer() && 
								
								(
								(redflag.getBlockX() == blockloc.getBlockX()) &&
								(redflag.getBlockY() == blockloc.getBlockY()) &&
								(redflag.getBlockZ() == blockloc.getBlockZ())) ||
								(
										(redflag2.getBlockX() == blockloc.getBlockX()) &&
										(redflag2.getBlockY() == blockloc.getBlockY()) &&
										(redflag2.getBlockZ() == blockloc.getBlockZ()))
								){//Blue player
							plugin.getServer().broadcastMessage(ChatColor.AQUA + "Blue team has gained " + Games.FlagCap + " points for capturing the flag!");
							Games.TeamKills.put("blue", Games.TeamKills.get("blue") + Games.FlagCap);
							Mysql.modifyUserPts(e.getPlayer().getName(), Games.FlagCap, RewardType.FlagCap);
							e.getPlayer().sendMessage(ChatColor.GOLD + "You have earned " + Games.FlagCap + " points for capturing the flag!");
							e.getBlock().setTypeId(0);
							Block redflagblock = plugin.getServer().getWorld("war").getBlockAt(redflagnew);
							redflagblock.setTypeId(159);
							redflagblock.setData((byte)14);// We spawn a red colored stained clay
							Games.RedFlagLoc = redflagnew;
							Games.RedFlagHolder = null;
						}
						else{
				        	if (Games.BlueFlagHolder == e.getPlayer()){
				        		CTF.FlagDespawnTimer(loc, "blue");
				        	}
				        	else if (Games.RedFlagHolder == e.getPlayer()){
				        		CTF.FlagDespawnTimer(loc, "red");
				        	}
				        	e.getBlock().setData((byte) e.getBlock().getData());
				        	}
						if (Games.TeamKills.get("blue") >= Games.ScoreMixed){
							DeathListener.WinningTeam("Red", ChatColor.RED);
						}
						else if (Games.TeamKills.get("red") >= Games.ScoreMixed){
							DeathListener.WinningTeam("Blue", ChatColor.AQUA);
						}
						e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 1), true);
		        }
			}
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void DestroyingFlag (BlockBreakEvent e){
		int id = e.getBlock().getTypeId();
		if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){
		if (id == 159 && (e.getBlock().getData() == 11 || e.getBlock().getData() == 14)){
			e.setCancelled(true);
        }
		}
		
		
		
		if (id == 57 || id == 152)
		e.setCancelled(true);
		}
}
