package kz.PvP.MedievalGames.Events;

import java.util.ArrayList;
import java.util.HashMap;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Game;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class DamageListener extends JavaPlugin implements Listener{
	public static Main plugin;
	public static HashMap<String, Integer> ShieldBlocks = new HashMap<String, Integer>();
	public static ArrayList<String> IncreaseBlock1 = new ArrayList<String>();
	public static ArrayList<String> IncreaseBlock2 = new ArrayList<String>();
	public static ArrayList<String> IncreaseBlock3 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock1 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock2 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock3 = new ArrayList<String>();
	public static int HealingHP = 10;
	public DamageListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	@EventHandler
	public void OnArrowLand (ProjectileHitEvent e){
		if (e.getEntity() instanceof Arrow)
		e.getEntity().remove();
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnPlayerAttack (EntityDamageByEntityEvent e){
		if (Main.Game == Game.GAME){
		Entity damager = e.getDamager();
		Entity defender = e.getEntity();
		if (damager instanceof Player && defender instanceof Player){
			Player dam = (Player) damager;
			Player def = (Player) defender;
			
			
			
			
			
			
			if (JoinListener.red.contains(dam) && JoinListener.red.contains(def))
				e.setCancelled(true);
			else if (JoinListener.blue.contains(dam) && JoinListener.blue.contains(def))
				e.setCancelled(true);
			else if (DeathListener.x.containsKey(def) || DeathListener.x.containsKey(dam))
				e.setCancelled(true);
			if (JoinListener.spectators.contains(dam) || JoinListener.spectators.contains(def))
				e.setCancelled(true);
			else if ((JoinListener.blue.contains(dam) && JoinListener.red.contains(def)) || (JoinListener.blue.contains(def) && JoinListener.red.contains(dam))){
				if (JoinListener.blue.contains(def)){
					if (def.getLocation().distance(Games.bluespawn) <= 3){
						e.setDamage(e.getDamage()/2);
					}
				}
				else if (JoinListener.red.contains(def)){
					if (def.getLocation().distance(Games.redspawn) <= 3){
						e.setDamage(e.getDamage()/2);
					}
				}
				
				
				
				
				
			if (def.getItemInHand().hasItemMeta() && def.getItemInHand().getItemMeta().hasDisplayName()){
				if (!ShieldBlocks.containsKey(def.getName()))
				ShieldBlocks.put(def.getName(), 64);
			if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) > 0){
				double newshield = 0.0;
				
				if (IncreaseBlock1.contains(def.getName()))
				newshield = (e.getDamage()/5) * 2;
				else if (IncreaseBlock2.contains(def.getName()))
				newshield = (e.getDamage()/10) * 3;
				else if (IncreaseBlock3.contains(def.getName()))
				newshield = (e.getDamage()/5) * 1;
				else
				newshield = (e.getDamage()/2) * 1;
				
				
				ShieldBlocks.put(def.getName(), ShieldBlocks.get(def.getName()) - 1);
				ItemMeta im = def.getItemInHand().getItemMeta();
				String[] realnamesplit = def.getItemInHand().getItemMeta().getDisplayName().split("   ");
				String realname = realnamesplit[0];
				im.setDisplayName(realname + "   " +  ChatColor.GOLD +  ShieldBlocks.get(def.getName()) + "/64");
				def.getItemInHand().setItemMeta(im);
				e.setDamage(newshield);
			}
			else if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) <= 0){
				def.sendMessage(ChatColor.GRAY + "Your shield has broken.");
				def.setItemInHand(new ItemStack(Material.AIR));
				def.updateInventory();
			}
			}
			if (dam.getItemInHand().hasItemMeta()){
			if (dam.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield")){
			e.setCancelled(true);
			dam.sendMessage(ChatColor.GRAY + "You are carrying a shield, which cannot be used to attack.");
			}
			}
			}
		}
		else if (e.getDamager() instanceof Projectile){
			Projectile projectile = (Projectile) e.getDamager();
			if (projectile.getShooter() instanceof Player){
				Player dam = (Player) projectile.getShooter();
				if (defender instanceof Player){
					Player def = (Player) defender;
					
					if (InteractListener.Healshot.contains(projectile.getUniqueId())){
						if (def.getHealth() >= (20 - HealingHP)){
							def.setHealth(20);
						}
						else {
							def.setHealth(def.getHealth() + HealingHP);
						}
						def.sendMessage(ChatColor.WHITE + "++  " + ChatColor.AQUA + "You have been healed by " + dam.getName() + ChatColor.WHITE + "  ++");
					}
					
					if (JoinListener.red.contains(dam) && JoinListener.red.contains(def))
						e.setCancelled(true);
					else if (JoinListener.blue.contains(dam) && JoinListener.blue.contains(def))
						e.setCancelled(true);
					else if ((JoinListener.blue.contains(dam) && JoinListener.red.contains(def)) || (JoinListener.blue.contains(def) && JoinListener.red.contains(dam))){
						if (InteractListener.Crossbow.contains(projectile.getUniqueId())){
							e.setDamage(4.0);
							InteractListener.Crossbow.remove(projectile.getUniqueId());
						}
						if (InteractListener.Longbow.contains(projectile.getUniqueId())){
							e.setDamage(e.getDamage() + 3);
							InteractListener.Longbow.remove(projectile.getUniqueId());
						}
						if (InteractListener.PoisonousArrow.contains(projectile.getUniqueId())){
							if (InteractListener.PoisonousShooters1.contains(dam.getName()))
							ChancePoison(5, def, 5);
							else if (InteractListener.PoisonousShooters2.contains(dam.getName()))
							ChancePoison(10, def, 5);
							else if (InteractListener.PoisonousShooters3.contains(dam.getName()))
							ChancePoison(15, def, 5);
						}
						
						if (def.getItemInHand().hasItemMeta()){
							if (!ShieldBlocks.containsKey(def.getName()))
							ShieldBlocks.put(def.getName(), 64);
						if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) > 0){
							double newshield = 0.0;
							
							if (ArcherBlock1.contains(def.getName()))
							newshield = (e.getDamage()/5) * 3;
							else if (ArcherBlock2.contains(def.getName()))
							newshield = (e.getDamage()/2) * 1;
							else if (ArcherBlock3.contains(def.getName()))
							newshield = (e.getDamage()/5) * 2;
							else
							newshield = (e.getDamage()/4) * 3;
							
							
							ShieldBlocks.put(def.getName(), ShieldBlocks.get(def.getName()) - 1);
							ItemMeta im = def.getItemInHand().getItemMeta();
							String[] realnamesplit = def.getItemInHand().getItemMeta().getDisplayName().split("   ");
							String realname = realnamesplit[0];
							im.setDisplayName(realname + "   " +  ChatColor.GOLD +  ShieldBlocks.get(def.getName()) + "/64");
							def.getItemInHand().setItemMeta(im);
							e.setDamage(newshield);
						}
						}
					}
			}
		}
		}
		}
		else{
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void DamagePlayer (EntityDamageEvent e){
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			if (Games.NotAttacked.contains(def.getName())){
				Games.NotAttacked.remove(def.getName());
			}
			if (Games.Invincibility.contains(def.getName())){
				e.setCancelled(true);
			}
		}
	}
	
	private void ChancePoison(int i, Player def, int length) {
		double chance = (i * 0.01);
		double ran = Math.random();
		if (ran <= chance){
			def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * length, 2));
			def.sendMessage(ChatColor.DARK_GREEN + "You have been arrow poisoned.");
		}
	}
}
