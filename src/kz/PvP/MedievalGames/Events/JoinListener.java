package kz.PvP.MedievalGames.Events;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;
import org.kitteh.tag.PlayerReceiveNameTagEvent;
import org.kitteh.tag.TagAPI;
import org.mcsg.double0negative.tabapi.TabAPI;




public class JoinListener extends JavaPlugin implements Listener{
	private static final int DiffTeam = 0;
	private static final int DiffRed = DiffTeam;
	private static final int DiffBlue = DiffTeam;
	public static Main plugin;
	public static ArrayList<Player> red = new ArrayList<Player>();
	public static ArrayList<Player> blue = new ArrayList<Player>();
	public static ArrayList<Player> spectators = new ArrayList<Player>();
	public static HashMap<String, Integer> TimedGaming = new HashMap<String, Integer>();
	public static HashMap<String, Objective> Objectives = new HashMap<String, Objective>();
	public static HashMap<String, org.bukkit.scoreboard.Scoreboard> Boards = new HashMap<String, org.bukkit.scoreboard.Scoreboard>();
	
	
	static org.bukkit.scoreboard.Scoreboard board;
	static ScoreboardManager manager;
	public static Objective objective;
	
	
	public JoinListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	@EventHandler
	public void OnServerJoin (PlayerJoinEvent e) throws SQLException{
		final Player p = e.getPlayer();
		
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		if (!Objectives.containsKey(p.getName()))
		Objectives.put(p.getName(), objective);

		if (!Boards.containsKey(p.getName()))
		Boards.put(p.getName(), board);
		
		TabAPI.setPriority(plugin, p, 2);
		
		p.getInventory().clear();
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.getInventory().setHelmet(null);
		p.setExp(0);
		p.setLevel(0);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				for (Player pl : plugin.getServer().getOnlinePlayers())
				Global.updateTab(pl);
			}
		}, 20);
		
		
		if (Mysql.GetUserID(p.getName()) == 0){
		PreparedStatement ps = Mysql.PS("INSERT INTO Users (Username) VALUES (?)");
		ps.setString(1, p.getName());
		ps.executeUpdate();
		
		PreparedStatement ps2 = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
		ps2.setInt(1, Mysql.GetUserID(p.getName()));
		ps.setLong(2, System.currentTimeMillis()/1000);
		ps2.setString(3, p.getAddress().getAddress().getHostAddress());
		ps2.executeUpdate();
		
		
		
		Message.P(p, ChatColor.GOLD + "Welcome first timer, " + p.getName() + "!");
		Message.P(p, Message.TypeHelpforHelp);
		}
		
		
		
		
		ResultSet rss = Mysql.dbGet("SELECT * FROM UserInfo WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
				
		if (!rss.next()){
			PreparedStatement ps = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
			ps.setInt(1, Mysql.GetUserID(p.getName()));
			ps.setLong(2, System.currentTimeMillis()/1000);
			ps.setString(3, p.getAddress().getAddress().getHostAddress());
			ps.executeUpdate();
		}
		
    	plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
    	    @Override  
    	    public void run() {
    	    	if (TimedGaming.containsKey(p.getName()))
    	    		TimedGaming.put(p.getName(), 2);
    	    	else
    	    		TimedGaming.put(p.getName(), TimedGaming.get(p.getName()) + 2);
    	    }
    	}, 20L * 2, 20L * 2);
		
		
		
		ResultSet rs = Mysql.dbGet("SELECT COUNT(*) as TOTALKILLS FROM DEATHS WHERE Killer = '" + Mysql.GetUserID(p.getName()) + "'");
		if (!Games.Kills.containsKey(p.getName()) && rs.next())
			Games.Kills.put(p.getName(), rs.getInt("TOTALKILLS"));
		
		
		//ResultSet rs = Mysql.dbGet("SELECT COUNT(*) AS Kills FROM DEATHS WHERE Killer = '" + Mysql.GetUserID(p.getName()) + "'");
		//Games.Kills.put(p.getName(), rs.getInt("Kills"));
		if (p.hasPermission("mw.announcejoin"))
		e.setJoinMessage(ChatColor.GOLD + p.getName() + " has joined.");
		else
		e.setJoinMessage(null);
		
		
		Games.SpecJoin(e.getPlayer());
		
		
		Message.P(p, ChatColor.GOLD + "" + ChatColor.ITALIC + "To join game, type /Team");
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) book.getItemMeta();
		meta.setTitle(ChatColor.GRAY + "" + ChatColor.BOLD + "MW Tutorial");
		meta.setAuthor(ChatColor.DARK_PURPLE + "MedievalWars");
		List<String> pages = new ArrayList<String>();
		pages.add(ChatColor.DARK_GREEN + "Welcome to Medievalwars!\n\n"  + 
		ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Table of Contents\n" +
			ChatColor.RESET + ChatColor.DARK_BLUE +
			
			
				"2 - How to play?\n" +
				"3 - Notable Commands\n" +
				"4 - Classes\n" + 
				"5 - Upgrading\n" +
				"6 - Rewards\n" +
				"7 - Tools\n");
		
		pages.add(ChatColor.RED + "How to play?" + ChatColor.RESET + ChatColor.BLACK +
				"\nMW is a mixture of multiple minigames, but with a twist!\n\n" +
				" Your goal can be determined on the gamemode for that round. You may check round information by typing /help. Each gamemode, has a different goal for the team.");
		pages.add(ChatColor.RED + "= Commands = \n\n" + ChatColor.DARK_GRAY +
				"/Class" + ChatColor.GRAY + " - Choose class\n" + ChatColor.DARK_GRAY +
				"/Upgrade" + ChatColor.GRAY + " - Upgrade class\n" + ChatColor.DARK_GRAY +
				"/Activate" + ChatColor.GRAY + " - Activate upgrade\n" + ChatColor.DARK_GRAY +
				"/Vote" + ChatColor.GRAY + " - Vote for Gamemode\n" + ChatColor.DARK_GRAY +
				"/Leave" + ChatColor.GRAY + " - Spectate\n" + ChatColor.DARK_GRAY +
				"/Help" + ChatColor.GRAY + " - Round Info\n" + ChatColor.DARK_GRAY +
				"/MW" + ChatColor.GRAY + " - Commands Index\n");

		
		pages.add(ChatColor.RED + "= Classes = \n\n" + ChatColor.BLACK + "Type /class to choose a class.\n\n" + "Each class has its own advantages, and disadvantages. Some classes are melee or ranged. They can also be aggressive or defensive. \n\n" + ChatColor.RED + "So pick wisely!");
		
		pages.add(ChatColor.RED + "Upgrading\n\n" + ChatColor.BLACK + "Upgrading is a huge aspect in MedievlWars." +
				"Each class has its own set of upgrades which enhance your gameplayer. Points are used to purchase upgrades. To open upgrade menu, type /upgrade");
		pages.add(ChatColor.RED + "" + ChatColor.BOLD + "Rewards" + ChatColor.RESET + ChatColor.BLACK + "\n\n Player Kill:\n" + ChatColor.DARK_BLUE + DeathListener.RewardKill + " point(s)" + ChatColor.BLACK + ".\n\n Flag capture\n " + ChatColor.DARK_GREEN + Games.FlagCap + " point(s).");
		
		meta.setPages(pages);
		book.setItemMeta(meta);
		p.getInventory().addItem(book);
		
		
	}
	@EventHandler
	public void onNameTag(PlayerReceiveNameTagEvent e) {
		Player p = e.getNamedPlayer();
		if (JoinListener.red.contains(p))
			e.setTag(ChatColor.RED + p.getName());
		else if (JoinListener.blue.contains(p))
			e.setTag(ChatColor.BLUE + p.getName());
		else
			e.setTag(ChatColor.GRAY + p.getName());
	}
	
	public static void AssignPlayer(Player p, Team team) {
		
		if ((!red.contains(p)) && (!blue.contains(p))){
			p.getInventory().clear();
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.getInventory().setLeggings(null);
			p.getInventory().setBoots(null);
			p.setExp(0);
		if (team == Team.Red){//Player wants to join RED team
		if (red.size() > (blue.size() + DiffBlue)){
			Message.P(p, ChatColor.RED + "This team has more players than the other. Type " + ChatColor.GOLD + "/team " + ChatColor.AQUA + "to join another team.");
			}
		else{
		red.add(p);
		Message.P(p, ChatColor.RED + "You have joined red team");
		JoinGame(p);
		}
		}
		else if (team == Team.Blue){//Player wants to join BLUE team
		if (blue.size() > (red.size() + DiffRed)){
		Message.P(p, ChatColor.AQUA + "This team has more players than the other. Type " + ChatColor.GOLD + "/team " + ChatColor.AQUA + "to join another team.");
		}
		else{
		blue.add(p);
		Message.P(p, ChatColor.AQUA + "You have joined blue team");
		JoinGame(p);
		}
		}
		else if (team == Team.Random){// Player wants a random team
		
		if ((blue.size() + DiffBlue) < red.size()){// Blue is lower than red team
		blue.add(p);
		Message.P(p, ChatColor.AQUA + "You have joined blue team");
		JoinGame(p);
		}
		else if ((red.size() + DiffRed) < blue.size()){// Red is lower than blue team
		red.add(p);
		Message.P(p, ChatColor.RED + "You have joined red team");
		JoinGame(p);
		}
		else{
			double r = Math.random();
			if (r <= 0.5){
				red.add(p);
				Message.P(p, ChatColor.RED + "You have joined red team");
				JoinGame(p);
			}
			else{
				blue.add(p);
				Message.P(p, ChatColor.AQUA + "You have joined blue team");
				JoinGame(p);
			}
		}
		}
		
		}
		else{
		Message.P(p,"You are already assigned. To spectate type " + ChatColor.ITALIC + "/Leave" + "\n To change teams type /Leave and then /Team");
		
		}
	}
	static public void JoinGame(Player p){
		TagAPI.refreshPlayer(p);
		spectators.remove(p);
		if (Main.Game != Game.PREGAME){
		Message.P(p, "To spectate type /Leave");
		try {
			Games.Start(p, true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Player pl : JoinListener.red){
			pl.showPlayer(p);
		}
		for (Player pl : JoinListener.blue){
			pl.showPlayer(p);
		}
		}
	}
	static public void ScoreBoard(Player player){
		if ((!Boards.containsKey(player.getName())) || (!Objectives.containsKey(player.getName()))){
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		if (!Objectives.containsKey(player.getName()))
		Objectives.put(player.getName(), objective);
		
		
		if (!Boards.containsKey(player.getName()))
		Boards.put(player.getName(), board);
		}
		
		objective = Objectives.get(player.getName());
		board = Boards.get(player.getName());
		String Team = "None";
		if (red.contains(player))
		Team = ChatColor.RED + "Red";
		else if (blue.contains(player))
		Team = ChatColor.AQUA + "Blue";
		else if (spectators.contains(player))
		Team = ChatColor.GRAY + "Spec";
		
		String Status = "None";
		if (Main.Gamemode == Gamemode.CTF)
		Status = "CTF";
		else if (Main.Gamemode == Gamemode.Mixed)
		Status = "Mixed";
		else if (Main.Gamemode == Gamemode.TDM)
		Status = "TDM";
		else if (Main.Gamemode == Gamemode.TeamElimination)
		Status = "Team Elimination";
		else
		Status = "Voting";
		
		objective.setDisplayName(ChatColor.GOLD + Status + " | " + Team);
		GiveStat(player, objective, red.size(), ChatColor.GREEN + "Red Team", ChatColor.RED);
		GiveStat(player, objective, blue.size(), ChatColor.GREEN + "Blue Team", ChatColor.AQUA);
		
		/*
		if (red.contains(player) && (Games.RedKills.containsKey(player.getName())))
		GiveStat(player, objective, Games.RedKills.get(player.getName()), "Kills", ChatColor.RED);
		else if (blue.contains(player) && (Games.BlueKills.containsKey(player.getName())))
		GiveStat(player, objective, Games.BlueKills.get(player.getName()), "Kills", ChatColor.AQUA);
		*/
		if (Main.Game == Game.GAME){
			
		if (Main.Gamemode == Gamemode.TDM){
			GiveStat(player, objective, Games.KillsTDM, "Goal", ChatColor.GOLD);
			GiveStat(player, objective, Games.TeamKills.get("blue"), "Blue Kills", ChatColor.AQUA);
			GiveStat(player, objective, Games.TeamKills.get("red"), "Red Kills", ChatColor.RED);
		}	
		else if (Main.Gamemode == Gamemode.CTF){
			GiveStat(player, objective, Games.FlagsToCap, "Captures", ChatColor.GOLD);
			GiveStat(player, objective, Games.BlueFlagCaps, "Blue Caps", ChatColor.AQUA);
			GiveStat(player, objective, Games.RedFlagCaps, "Red Caps", ChatColor.RED);
		}
		else if (Main.Gamemode == Gamemode.Mixed){
			GiveStat(player, objective, Games.ScoreMixed, "Goal", ChatColor.GOLD);
			GiveStat(player, objective, Games.TeamKills.get("blue"), "Blue Score", ChatColor.AQUA);
			GiveStat(player, objective, Games.TeamKills.get("red"), "Red Score", ChatColor.RED);
		}
		}
		player.setScoreboard(board);
	}
	public static void GiveStat(Player player, Objective objective, Integer integer, String string, ChatColor COLOR) {
		  Score score = objective.getScore(Bukkit.getOfflinePlayer(COLOR + string)); //Get a fake offline player
		  score.setScore(integer); //Integer only!
	}
	
	@EventHandler
	public void OnKick (PlayerKickEvent e) throws SQLException{
		e.setLeaveMessage(null);
		
		Player p = e.getPlayer();
		
		if (TimedGaming.containsKey(p.getName())){
			PreparedStatement ps = Mysql.PS("UPDATE UserInfo SET MWTime = '" + Mysql.getMwTime(p.getName()) + TimedGaming.get(p.getName()) + "' WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
			ps.executeUpdate();
			TimedGaming.remove(p.getName());
		}
		if (Main.Gamemode == Gamemode.TeamElimination && (JoinListener.red.size() <= 1 || JoinListener.blue.size() <= 1) && (JoinListener.blue.contains(p) || JoinListener.red.contains(p))){
			if (JoinListener.blue.contains(p))
			DeathListener.WinningTeam("Red", ChatColor.RED);
			else if (JoinListener.red.contains(p))
			DeathListener.WinningTeam("Blue", ChatColor.BLUE);
		}
	    for (PotionEffect effect : p.getActivePotionEffects())
	        p.removePotionEffect(effect.getType());

		red.remove(p);
		blue.remove(p);
		spectators.remove(p);
		Objectives.remove(p);
		Boards.remove(p);
		InteractListener.IncreaseHorseHP1.remove(p.getName());
		InteractListener.IncreaseHorseHP2.remove(p.getName());
		InteractListener.IncreaseHorseHP3.remove(p.getName());
		InteractListener.Compass.remove(p.getName());
		InteractListener.Crossbow.remove(p.getName());
		InteractListener.Longbow.remove(p.getName());
		InteractListener.PoisonousShooters1.remove(p.getName());
		InteractListener.PoisonousShooters2.remove(p.getName());
		InteractListener.PoisonousShooters3.remove(p.getName());
		DamageListener.ArcherBlock1.remove(p.getName());
		DamageListener.ArcherBlock2.remove(p.getName());
		DamageListener.ArcherBlock3.remove(p.getName());
		DamageListener.IncreaseBlock1.remove(p.getName());
		DamageListener.IncreaseBlock2.remove(p.getName());
		DamageListener.IncreaseBlock3.remove(p.getName());
		DamageListener.ShieldBlocks.remove(p.getName());
		Games.IdleUsers.remove(p.getName());
		if (Main.Game == Game.GAME && JoinListener.red.contains(p) || JoinListener.blue.contains(p)){
			Message.P(p, "");
		}
		
		TabAPI.setPriority(plugin, p, -2);
		Location loc = p.getLocation();
		if (Games.BlueFlagHolder == p){
			CTF.SpawnFlag(loc, "blue");
			CTF.FlagDespawnTimer(loc, "blue");
		}
		else if (Games.RedFlagHolder == p){
			CTF.SpawnFlag(loc, "red");
			CTF.FlagDespawnTimer(loc, "red");
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				for (Player pl : plugin.getServer().getOnlinePlayers())
				Global.updateTab(pl);
			}
		}, 6);
		
		if (Games.Kills.containsKey(p.getName()))
			Games.Kills.remove(p.getName());
		
		
		
	}
	
	@EventHandler
	public void OnServerQuit (PlayerQuitEvent e) throws SQLException{
		e.setQuitMessage(null);
		
		
		Player p = e.getPlayer();
		
		if (TimedGaming.containsKey(p.getName())){
			PreparedStatement ps = Mysql.PS("UPDATE UserInfo SET MWTime = '" + Mysql.getMwTime(p.getName()) + TimedGaming.get(p.getName()) + "' WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
			ps.executeUpdate();
			TimedGaming.remove(p.getName());			
		}
		if (Main.Gamemode == Gamemode.TeamElimination && (JoinListener.red.size() <= 1 || JoinListener.blue.size() <= 1) && (JoinListener.blue.contains(p) || JoinListener.red.contains(p))){
			if (JoinListener.blue.contains(p))
			DeathListener.WinningTeam("Red", ChatColor.RED);
			else if (JoinListener.red.contains(p))
			DeathListener.WinningTeam("Blue", ChatColor.BLUE);
		}
	    for (PotionEffect effect : p.getActivePotionEffects())
	        p.removePotionEffect(effect.getType());

		red.remove(p);
		blue.remove(p);
		spectators.remove(p);
		Objectives.remove(p);
		Boards.remove(p);
		InteractListener.IncreaseHorseHP1.remove(p.getName());
		InteractListener.IncreaseHorseHP2.remove(p.getName());
		InteractListener.IncreaseHorseHP3.remove(p.getName());
		InteractListener.Compass.remove(p.getName());
		InteractListener.Crossbow.remove(p.getName());
		InteractListener.Longbow.remove(p.getName());
		InteractListener.PoisonousShooters1.remove(p.getName());
		InteractListener.PoisonousShooters2.remove(p.getName());
		InteractListener.PoisonousShooters3.remove(p.getName());
		DamageListener.ArcherBlock1.remove(p.getName());
		DamageListener.ArcherBlock2.remove(p.getName());
		DamageListener.ArcherBlock3.remove(p.getName());
		DamageListener.IncreaseBlock1.remove(p.getName());
		DamageListener.IncreaseBlock2.remove(p.getName());
		DamageListener.IncreaseBlock3.remove(p.getName());
		DamageListener.ShieldBlocks.remove(p.getName());
		Games.IdleUsers.remove(p.getName());
		if (Main.Game == Game.GAME && JoinListener.red.contains(p) || JoinListener.blue.contains(p)){
			Message.P(p, "");
		}
		
		TabAPI.setPriority(plugin, p, -2);
		Location loc = p.getLocation();
		if (Games.BlueFlagHolder == p){
			CTF.SpawnFlag(loc, "blue");
			CTF.FlagDespawnTimer(loc, "blue");
		}
		else if (Games.RedFlagHolder == p){
			CTF.SpawnFlag(loc, "red");
			CTF.FlagDespawnTimer(loc, "red");
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				for (Player pl : plugin.getServer().getOnlinePlayers())
				Global.updateTab(pl);
			}
		}, 6);
		
		if (Games.Kills.containsKey(p.getName()))
			Games.Kills.remove(p.getName());
		
		
		
	}
	
	
}
