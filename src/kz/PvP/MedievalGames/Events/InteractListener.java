package kz.PvP.MedievalGames.Events;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Enums.ChatType;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.RewardType;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import me.confuserr.banmanager.BmAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;


public class InteractListener extends JavaPlugin implements Listener{
	public static Main plugin;
	public static ArrayList<UUID> Longbow = new ArrayList<UUID>();
	public static ArrayList<UUID> Crossbow = new ArrayList<UUID>();
	public static ArrayList<UUID> Healshot = new ArrayList<UUID>();
	public static ArrayList<UUID> PoisonousArrow = new ArrayList<UUID>();
	public static ArrayList<String> DelayShooter = new ArrayList<String>();
	public static ArrayList<String> PoisonousShooters1 = new ArrayList<String>();
	public static ArrayList<String> PoisonousShooters2 = new ArrayList<String>();
	public static ArrayList<String> PoisonousShooters3 = new ArrayList<String>();
	
	public static ArrayList<String> IncreaseHorseHP1 = new ArrayList<String>();
	public static ArrayList<String> IncreaseHorseHP2 = new ArrayList<String>();
	public static ArrayList<String> IncreaseHorseHP3 = new ArrayList<String>();
	int MaxCharges = 3;
	
	public static HashMap<String, Integer> Charges = new HashMap<String, Integer>();
	public static ArrayList<String> NotifiedPlayerSlot = new ArrayList<String>();
	public static HashMap<String, String> Compass = new HashMap<String, String>();
	

	public InteractListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
	@EventHandler
	public void Chat (AsyncPlayerChatEvent e){
		if (!e.isCancelled() || !BmAPI.isMuted(e.getPlayer().getName())){
		PermissionUser user = PermissionsEx.getUser(e.getPlayer());
		String prefx = user.getPrefix();
		
		if (prefx.toLowerCase().contains("owner"))
		prefx = "&6*&bO&6*";
		else if (prefx.toLowerCase().contains("co-owner"))
		prefx = "&6*&aC-O&6*";
		else if (prefx.toLowerCase().contains("admin"))
		prefx = "&7*&bA&7*";
		else if (prefx.toLowerCase().contains("mod"))
		prefx = "&7*&aM&7*";
		else if (prefx.toLowerCase().contains("vip"))
		prefx = "&7*&5V&7*";
		else
		prefx = "";
		
		String msg = "";
		
		if (e.getPlayer().hasPermission("chat.color"))
		msg = ChatColor.translateAlternateColorCodes('&', e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1));
		else
		msg = e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1);
		
		
		String prefix = ChatColor.translateAlternateColorCodes('&', prefx + " ");
		
		
		
		if (Main.Game != Game.PREGAME){
			if (JoinListener.spectators.contains(e.getPlayer())){
			for (Player p : JoinListener.spectators){
				p.sendMessage(ChatColor.GRAY + "*Spec* " + prefix + e.getPlayer().getDisplayName() + ": " + msg);
			}
			Bukkit.getConsoleSender().sendMessage("*Spec* " + e.getPlayer().getDisplayName() + ":" + msg);
			e.setCancelled(true);
			}
	if (Games.Chatting.get(e.getPlayer().getName()) == ChatType.Public){
		e.setFormat(prefix + ChatColor.WHITE + Global.ChatRank(e.getPlayer().getName(), true) + e.getPlayer().getDisplayName() + ChatColor.GRAY + ": " + msg);		
	}
	else{
		if (JoinListener.red.contains(e.getPlayer())){
			for (Player pl : JoinListener.red)
			pl.sendMessage("[T] " + prefix + ChatColor.WHITE + Global.ChatRank(e.getPlayer().getName(), true) + ChatColor.RED + e.getPlayer().getDisplayName() + ": " + msg);
			for (Player pl : JoinListener.spectators)
			pl.sendMessage("[T] " + prefix + ChatColor.WHITE + Global.ChatRank(e.getPlayer().getName(), true) + ChatColor.RED + e.getPlayer().getDisplayName() + ": " + msg);
			System.out.println(prefix + e.getPlayer().getName() + ": " + e.getMessage());

			e.setCancelled(true);
		}
		if (JoinListener.blue.contains(e.getPlayer())){
			for (Player pl : JoinListener.blue)
			pl.sendMessage("[T] " + prefix + ChatColor.WHITE + Global.ChatRank(e.getPlayer().getName(), true) + ChatColor.AQUA + e.getPlayer().getDisplayName() + ": " + msg);
			for (Player pl : JoinListener.spectators)
			pl.sendMessage("[T] " + prefix + ChatColor.WHITE + Global.ChatRank(e.getPlayer().getName(), true) + ChatColor.AQUA + e.getPlayer().getDisplayName() + ": " + msg);
			
			System.out.println(prefix +  " " + e.getPlayer().getName() + ": " + e.getMessage());
			e.setCancelled(true);
		}
	}
	}
	else{
		e.setFormat(prefix + ChatColor.GRAY + e.getPlayer().getDisplayName() + ChatColor.GRAY + ": " + msg);		

		
	}
		
		
		
		}
	
	}
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void UseHorse (PlayerInteractEntityEvent e){
				if(e.getPlayer() instanceof Player && e.getRightClicked() instanceof Horse){
					Player p = e.getPlayer();
					Horse h = (Horse) e.getRightClicked();
					String Class = Kits.KitRound.get(p);
					if (Class != null && !Class.toLowerCase().contains("mounted")){
						e.setCancelled(true);
						p.sendMessage(ChatColor.RED + "Your class is not capable to ride horses.");
					}
					else if (Games.RedFlagHolder == p || Games.BlueFlagHolder == p){
						e.setCancelled(true);
						p.sendMessage(ChatColor.GRAY + "The horse is not able to carry you, and the flag. You must travel by foot!");
					}
					else{
						
						if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasDisplayName()){
							if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("horse bandaid")){
									if (h.getHealth() == h.getMaxHealth()){
										p.sendMessage(ChatColor.GRAY + "The horse is at full health.");
									}
									else if (h.getHealth() < h.getMaxHealth()){
										h.setHealth(h.getMaxHealth());
										if (p.getItemInHand().getAmount() > 1)
										p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
										else
										p.setItemInHand(new ItemStack(Material.AIR));
										p.updateInventory();
										p.sendMessage(ChatColor.GREEN + "You have healed the horse to full health.");
									}
									e.setCancelled(true);
							}
						}
						else{
						boolean fillup = false;
						if (h.getMaxHealth() == h.getHealth())
						fillup = true;
						
						if (IncreaseHorseHP1.contains(p.getName()))
						h.setMaxHealth(24.0 + 6.0);
						else if (IncreaseHorseHP2.contains(p.getName()))
						h.setMaxHealth(24.0 + 12.0);
						else if (IncreaseHorseHP3.contains(p.getName())){
						h.setMaxHealth(24.0 + 20.0);
						}
						
						
						
						
						if (fillup == true)
						h.setHealth(h.getMaxHealth());
						}
					}
					}
		
		
	}
	
	@EventHandler
	public void OnBlockChoose (VehicleExitEvent e){
		if (e.getVehicle() instanceof Horse && e.getExited() instanceof Player){
			Horse h = (Horse)e.getVehicle();
			
			if (h.getHealth() == h.getMaxHealth())
				h.setHealth(12.0);
			else if (h.getHealth() > 12.0)
				h.setHealth(11.0);
			
			h.setMaxHealth(12.0);
		}
	}
@SuppressWarnings("deprecation")
@EventHandler
public void OnBlockChoose (PlayerInteractEvent e){
	final Player p = e.getPlayer();
	
	if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
			&& p.getItemInHand().hasItemMeta() == true && p.getItemInHand().getItemMeta().hasDisplayName() == true){
		
		
		if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("horse bandaid")){
			if (p.getVehicle() instanceof Horse){
				Horse h = (Horse)p.getVehicle();
				if (h.getHealth() == h.getMaxHealth()){
					p.sendMessage(ChatColor.GRAY + "The horse is at full health.");
				}
				else if (h.getHealth() < h.getMaxHealth()){
					h.setHealth(h.getMaxHealth());
					if (p.getItemInHand().getAmount() > 1)
					p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
					else
					p.setItemInHand(new ItemStack(Material.AIR));
					
					
					p.updateInventory();
					p.sendMessage(ChatColor.GREEN + "You have healed the horse to full health.");
				}
			}
		}
		else if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("player bandaid")){
			if (p.getHealth() == p.getMaxHealth())
			p.sendMessage(ChatColor.GRAY + "You are currently at full health.");
			else if (p.getHealth() < p.getMaxHealth()){
				p.setHealth(p.getMaxHealth());
				if (p.getItemInHand().getAmount() > 1)
				p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
				else
				p.setItemInHand(new ItemStack(Material.AIR));
				p.updateInventory();
				p.sendMessage(ChatColor.GREEN + "You have healed yourself to full health & removed all potion effects.");
    			
				p.setFireTicks(0);
			    for (PotionEffect effect : p.getActivePotionEffects()){
			        
			    	if (effect.getDuration() <= 20 * 60 * 5)
			    	p.removePotionEffect(effect.getType());
			    }
			}
			
		}
		else if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("healing wand") && Kits.KitRound.containsKey(p) && Config.getConfig(ConfigFile.Kits).getBoolean(Kits.KitRound.get(p) + ".AllowHeal")){
			if (!Charges.containsKey(p.getName()))
			Charges.put(p.getName(), MaxCharges);
			
			
			if (!DelayShooter.contains(p.getName())){
			if (Charges.get(p.getName()) >= 1){
			Fireball fb = p.launchProjectile(Fireball.class);
			fb.setVelocity(p.getLocation().getDirection().multiply(2));
			fb.setShooter(p);
			fb.setIsIncendiary(false);
			
			Healshot.add(fb.getUniqueId());
			DelayShooter.add(p.getName());
			Charges.put(p.getName(), Charges.get(p.getName()) - 1);
			
			int ChargesLeft = Charges.get(p.getName());
			String[] wepname = p.getItemInHand().getItemMeta().getDisplayName().split("    ");
			String wep = wepname[0];
			ItemMeta im = p.getItemInHand().getItemMeta();
			im.setDisplayName(wep + "    " + ChatColor.YELLOW + ChargesLeft + "/" + MaxCharges);
			p.getItemInHand().setItemMeta(im);
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					DelayShooter.remove(p.getName());
				}
				}, 20L * 4);
			}
			else{
				p.sendMessage(ChatColor.GRAY + "Your healing wand, has lost all its charges.");
			}
		  }
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	//int id = p.getItemInHand().getTypeId();
	//if ((id == 268 || id == 272 || id == 267 || id == 276 || id == 283 || id == 261) && e.getAction() == Action.RIGHT_CLICK_BLOCK)
	
	
	if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){
	if (e.getClickedBlock() != null){
	if (e.getClickedBlock().getTypeId() == 159 && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)){
		if (e.getClickedBlock().getData() == 11 || e.getClickedBlock().getData() == 14){
			if (e.getClickedBlock().getData() == 11 && JoinListener.red.contains(e.getPlayer()) && Games.BlueFlagLoc.getBlockY() == e.getClickedBlock().getLocation().getBlockY() && Games.BlueFlagLoc.getBlockZ() == e.getClickedBlock().getLocation().getBlockZ() && Games.BlueFlagLoc.getBlockX() == e.getClickedBlock().getLocation().getBlockX()){
				plugin.getServer().getWorld("war").getBlockAt(e.getClickedBlock().getLocation()).setTypeId(0);
				ItemStack IS = new ItemStack(Material.STAINED_CLAY);
				IS.setDurability((short) 11);
				ItemMeta im = IS.getItemMeta();
				im.setDisplayName(ChatColor.BLUE + "Blue Flag");
				IS.setItemMeta(im);
				//e.getPlayer().getInventory().addItem(IS);
				ItemStack old = e.getPlayer().getInventory().getItemInHand();
				p.getInventory().setItemInHand(IS);
				p.getInventory().addItem(old);
				Games.BlueFlagHolder = e.getPlayer();
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 200, 0));
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The blue flag is now captured by " + ChatColor.RED + p.getName());
			}
			else if (e.getClickedBlock().getData() == 14 && JoinListener.blue.contains(e.getPlayer())  && Games.RedFlagLoc.getBlockY() == e.getClickedBlock().getLocation().getBlockY() && Games.RedFlagLoc.getBlockZ() == e.getClickedBlock().getLocation().getBlockZ() && Games.RedFlagLoc.getBlockX() == e.getClickedBlock().getLocation().getBlockX()){
				plugin.getServer().getWorld("war").getBlockAt(e.getClickedBlock().getLocation()).setTypeId(0);
				ItemStack IS = new ItemStack(Material.STAINED_CLAY);
				IS.setDurability((short) 14);
				ItemMeta im = IS.getItemMeta();
				im.setDisplayName(ChatColor.RED + "Red Flag");
				IS.setItemMeta(im);
				//e.getPlayer().getInventory().addItem(IS);
				ItemStack old = e.getPlayer().getInventory().getItemInHand();
				p.getInventory().setItemInHand(IS);
				p.getInventory().addItem(old);
				Games.RedFlagHolder = e.getPlayer();
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 200, 0));
				plugin.getServer().broadcastMessage(ChatColor.GRAY + "The red flag is now captured by " + ChatColor.AQUA + p.getName());
			}
			
			if (p.getVehicle() != null)
			p.getVehicle().eject();
			e.getPlayer().updateInventory();
		}
	}
	}
	}
	if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){

	if (p.getItemInHand().getType() == Material.COMPASS){
		if (Games.RedFlagLoc != null && Games.BlueFlagLoc != null){
		if (p.getItemInHand().hasItemMeta() == true && p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("detector")){
			
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){//UpdateDistance & Point
				if (Compass.get(p.getName()) == null){
					if (JoinListener.blue.contains(p))
						Compass.put(p.getName(), "RedTeam");
					else if (JoinListener.blue.contains(p))
						Compass.put(p.getName(), "BlueTeam");
				}
				else if (Compass.get(p.getName()) == "RedTeam"){
				p.setCompassTarget(Games.RedFlagLoc);
				int blocks = (int) p.getLocation().distance(Games.RedFlagLoc);
				String holder = "";
				if (Games.RedFlagHolder != null)
				holder = " " + Games.RedFlagHolder.getName() + " is holding the flag.";
				p.sendMessage(ChatColor.GRAY + "Tracing the red flag, which is " + blocks + " blocks away." + holder);
				}
				else if (Compass.get(p.getName()) == "BlueTeam"){
				p.setCompassTarget(Games.BlueFlagLoc);
				int blocks = (int) p.getLocation().distance(Games.BlueFlagLoc);
				String holder = "";
				if (Games.BlueFlagHolder != null)
				holder = " " + Games.BlueFlagHolder.getName() + " is holding the flag.";
				p.sendMessage(ChatColor.GRAY + "Tracing the blue flag, which is " + blocks + " blocks away." + holder);
				}
				
				
			}
			if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){//Change Traced team
				if (Compass.get(p.getName()) == null){
					if (JoinListener.blue.contains(p))
						Compass.put(p.getName(), "RedTeam");
					else if (JoinListener.blue.contains(p))
						Compass.put(p.getName(), "BlueTeam");
					}
				else if (Compass.get(p.getName()) == "RedTeam"){
					Compass.put(p.getName(), "BlueTeam");
					p.sendMessage(ChatColor.AQUA + "Now tracing the blue team flag.");
					}
				else if (Compass.get(p.getName()) == "BlueTeam"){
					Compass.put(p.getName(), "RedTeam");
					p.sendMessage(ChatColor.RED + "Now tracing the red team flag.");
					}
				}
			}
		}
	}
	}
}

@SuppressWarnings("deprecation")
@EventHandler
public void EntityExplode (EntityExplodeEvent e){
	if (Healshot.contains(e.getEntity().getUniqueId())) {
		Healshot.remove(e.getEntity().getUniqueId());
			e.setCancelled(true);
	}
	if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){
	for (Block b : e.blockList()){
		if ((b.getData() == 11 || b.getData() == 14) && b.getTypeId() == 159)
		e.setCancelled(true);
	}
	}
	
}
public void onExplosionPrime(ExplosionPrimeEvent e) {
	e.setFire(false); //Only really needed for fireballs
	if (Healshot.contains(e.getEntity().getUniqueId())) {
		Healshot.remove(e.getEntity().getUniqueId());
			e.setCancelled(true);
			e.setRadius(0);
	}
}
@SuppressWarnings("deprecation")
@EventHandler
public void OnSlotChangeWhileFlag(final PlayerItemHeldEvent e){
	if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){
	if (e.getPlayer().getInventory().getItem(e.getPreviousSlot()) != null){
	if (e.getPlayer().getInventory().getItem(e.getPreviousSlot()).getTypeId() == 159 && (Games.BlueFlagHolder == e.getPlayer() || Games.RedFlagHolder == e.getPlayer())){
	e.setCancelled(true);
	if (!NotifiedPlayerSlot.contains(e.getPlayer().getName())){
	e.getPlayer().sendMessage(ChatColor.YELLOW + "You must drop the flag, to use other items. To drop flag press 'Q' (default).");
	NotifiedPlayerSlot.add(e.getPlayer().getName());
	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	public void run() {
		NotifiedPlayerSlot.remove(e.getPlayer().getName());
	}
	}, 20L * 3);
	}
	}
	}
	
	}
}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void InventoryUse (InventoryClickEvent e){
		if (e.getSlot() == 39){
			e.setCancelled(true);
			}
		else if (e.getCurrentItem() != null && e.getCurrentItem().getTypeId() == 159 && (e.getCurrentItem().getDurability() == 14 || e.getCurrentItem().getDurability() == 11)){
			e.setCancelled(true);
		}
	
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnPlayerDropItem (PlayerDropItemEvent e){
				//Player p = (Player) e.getPlayer();
				/*
				if (InteractListener.Crossbow.contains(e.getItem().getUniqueId())){
				p.getInventory().addItem(GetIMOf("Iron Bolts"));
				e.setCancelled(true);
				e.getItem().remove();
				}
				else if (InteractListener.Longbow.contains(e.getItem().getUniqueId())){
				p.getInventory().addItem(GetIMOf("Maple Arrows"));
				e.setCancelled(true);
				e.getItem().remove();
				}*/        
        Item drop = e.getItemDrop();
        int item = drop.getItemStack().getTypeId();
        if (item != 159 && (item == 345 || (item >= 298 && item <= 301)))
        e.setCancelled(true);
        	
        	
        else if (drop.getItemStack().getTypeId() == 159){
        	Location loc = new Location(plugin.getServer().getWorld("war"), e.getPlayer().getLocation().getX(), e.getPlayer().getLocation().getY(), e.getPlayer().getLocation().getZ());
        	if (Games.BlueFlagHolder == e.getPlayer()){
        		CTF.SpawnFlag(loc, "blue");
        		CTF.FlagDespawnTimer(loc, "blue");
        	}
        	else if (Games.RedFlagHolder == e.getPlayer()){
				CTF.SpawnFlag(loc, "red");
        		CTF.FlagDespawnTimer(loc, "red");
        	}
        	e.getItemDrop().remove();
        }
	}
	
	@EventHandler
	public void OnPlayerShoot (ProjectileLaunchEvent e) throws SQLException{
		if (e.getEntity().getShooter() instanceof Player){
			Player p = (Player) e.getEntity().getShooter();
			if (p.getItemInHand().hasItemMeta() == true){
			if (p.getItemInHand().getItemMeta().hasDisplayName() && p.getItemInHand().getItemMeta().getDisplayName().contains("Maple Longbow")){
	               Vector vel = e.getEntity().getVelocity().multiply(0.80);
	               e.getEntity().setVelocity(vel);
	               Longbow.add(e.getEntity().getUniqueId());
			}
			if (PoisonousShooters1.contains(p.getName()) || PoisonousShooters2.contains(p.getName()) || PoisonousShooters3.contains(p.getName())){
				PoisonousArrow.add(e.getEntity().getUniqueId());
			
			}
				
				
			}
			}
			/*
			if (Main.Game != Game.GAME){
					e.setCancelled(true);
					if (InteractListener.Crossbow.contains(e.getEntity().getUniqueId()))
					p.getInventory().addItem(GetIMOf("Iron Bolts"));
					else if (InteractListener.Longbow.contains(e.getEntity().getUniqueId()))
					p.getInventory().addItem(GetIMOf("Maple Arrows"));
					else
					p.getInventory().addItem(new ItemStack(Material.ARROW,1));
			}
			*/
		}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void CrossBowEvent (PlayerInteractEvent e){
		final Player p = e.getPlayer();
		if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasDisplayName()){
		if (p.getItemInHand().getItemMeta().getDisplayName().contains("Crossbow")){
			   Arrow arrow = p.launchProjectile(Arrow.class);
        	   Vector vel = p.getLocation().getDirection().normalize().multiply(4);
   			   arrow.setVelocity(vel);
   			   arrow.setShooter(p);
        	   Crossbow.add(arrow.getUniqueId());
        	    final ItemStack im = p.getItemInHand();
        	    p.setItemInHand(new ItemStack(Material.AIR));
				p.updateInventory();
        		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
        			public void run() {
        				p.setItemInHand(im);
        				p.updateInventory();
        			}
        			}, 20L * 2);
			}
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnMoveEvent (PlayerMoveEvent e) throws SQLException{
		Player p = e.getPlayer();
		if (Main.Gamemode == Gamemode.CTF || Main.Gamemode == Gamemode.Mixed){ 
		if (e.getPlayer().getWorld() == plugin.getServer().getWorld("war")){
		 int ID = p.getItemInHand().getTypeId();
		 short dura = p.getItemInHand().getDurability();
		
		
		if (Games.RedFlagLoc != null && Games.BlueFlagLoc != null){
		
			
		int newlocx = e.getTo().getBlockX();
		int newlocy = e.getTo().getBlockY();
		int newlocz = e.getTo().getBlockZ();
		
		int oldlocx = e.getFrom().getBlockX();
		int oldlocy = e.getFrom().getBlockY();
		int oldlocz = e.getFrom().getBlockZ();
		if (newlocx != oldlocx || newlocz != oldlocz || newlocy != oldlocy){
			double px = p.getLocation().getX();
			double py = p.getLocation().getY();
			double pz = p.getLocation().getZ();
			double BlueBlock1X = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.blue.X");
			 double BlueBlock1Y = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.blue.Y");
			 double BlueBlock1Z = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.blue.Z");
			
			 double BlueBlock2X = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.blue.X");
			 double BlueBlock2Y = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.blue.Y");
			 double BlueBlock2Z = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.blue.Z");
			 double RedBlock1X = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.red.X");
			 double RedBlock1Y = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.red.Y");
			 double RedBlock1Z = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop.red.Z");
			
			 double RedBlock2X = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.red.X");
			 double RedBlock2Y = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.red.Y");
			 double RedBlock2Z = (double) Config.getConfig(ConfigFile.Teleports).get("flagstop2.red.Z");
			
			if (ID == 159 && (dura == 14 || dura == 11) && (Games.BlueFlagHolder == p || Games.RedFlagHolder == p)){
				if (((px >= RedBlock1X && px <= RedBlock2X) || (px <= RedBlock1X && px >= RedBlock2X))
						&& ((py >= RedBlock1Y && py <= RedBlock2Y) || (py <= RedBlock1Y && py >= RedBlock2Y))
						&& ((pz >= RedBlock1Z && pz <= RedBlock2Z) || (pz <= RedBlock1Z && pz >= RedBlock2Z))
						&& dura == 11 && Games.BlueFlagHolder == p
						){
					p.setItemInHand(new ItemStack(Material.AIR));
					p.updateInventory();
					Location blueflagnew = Global.GetLocOfString("flag.blue", "war");
					if (Main.Gamemode == Gamemode.Mixed){
					Games.TeamKills.put("red", Games.TeamKills.get("red") + Games.FlagCap);		
					plugin.getServer().broadcastMessage(ChatColor.RED + "Red team has gained " + Games.FlagCap + " points for capturing the flag!");
					}
					else if (Main.Gamemode == Gamemode.CTF){
					Games.RedFlagCaps = Games.RedFlagCaps + 1;
					plugin.getServer().broadcastMessage(ChatColor.RED + "Red team has captured the flag!");
					}
					
					ResultSet rs = Mysql.dbGet("SELECT * FROM UserInfo WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
					
					if (rs.next()){
						PreparedStatement ps2 = Mysql.PS("UPDATE UserInfo SET FlagCaptures = ? WHERE User = ?");
						ps2.setInt(1, rs.getInt("FlagCaptures") + 1);
						ps2.setInt(2, Mysql.GetUserID(p.getName()));
						ps2.executeUpdate();
					}
					else{
					
					
						PreparedStatement ps = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
						ps.setInt(1, Mysql.GetUserID(p.getName()));
						ps.setFloat(2, System.currentTimeMillis()/1000);
						ps.setString(3, p.getAddress().getAddress().getHostAddress());
						ps.executeUpdate();
					
						PreparedStatement ps2 = Mysql.PS("UPDATE UserInfo SET FlagCaptures = ? WHERE User = ?");
						ps2.setInt(1, rs.getInt("FlagCaptures") + 1);
						ps2.setInt(2, Mysql.GetUserID(p.getName()));
						ps2.executeUpdate();
					
					}
					
					
					Mysql.modifyUserPts(p.getName(), Games.FlagCap, RewardType.FlagCap);
					CTF.SpawnFlag(blueflagnew, "blue");
					p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 0), true);
				}
				else if (((px >= BlueBlock1X && px <= BlueBlock2X) || (px <= BlueBlock1X && px >= BlueBlock2X))
						&& ((py >= BlueBlock1Y && py <= BlueBlock2Y) || (py <= BlueBlock1Y && py >= BlueBlock2Y))
						&& ((pz >= BlueBlock1Z && pz <= BlueBlock2Z) || (pz <= BlueBlock1Z && pz >= BlueBlock2Z))
						&& dura == 14 && Games.RedFlagHolder == p
						){
					if (p.getItemInHand().getTypeId() == 159 && p.getItemInHand().getDurability() == 14){
						p.setItemInHand(new ItemStack(Material.AIR));
						p.updateInventory();
						Location redflagnew = Global.GetLocOfString("flag.red", "war");						
						if (Main.Gamemode == Gamemode.Mixed){
						Games.TeamKills.put("blue", Games.TeamKills.get("blue") + Games.FlagCap);		
						plugin.getServer().broadcastMessage(ChatColor.AQUA + "Blue team has gained " + Games.FlagCap + " points for capturing the flag!");
						}
						else if (Main.Gamemode == Gamemode.CTF){
						Games.BlueFlagCaps = Games.BlueFlagCaps + 1;
						plugin.getServer().broadcastMessage(ChatColor.AQUA + "Blue team has captured the flag!");
						}
						
						
						ResultSet rs = Mysql.dbGet("SELECT * FROM UserInfo WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
						
						if (rs.next()){
							PreparedStatement ps2 = Mysql.PS("UPDATE UserInfo SET FlagCaptures = ? WHERE User = ?");
							ps2.setInt(1, rs.getInt("FlagCaptures") + 1);
							ps2.setInt(2, Mysql.GetUserID(p.getName()));
							ps2.executeUpdate();
						}
						else{
						
						
							PreparedStatement ps = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
							ps.setInt(1, Mysql.GetUserID(p.getName()));
							ps.setLong(2, System.currentTimeMillis()/1000);
							ps.setString(3, p.getAddress().getAddress().getHostAddress());
							ps.executeUpdate();
						
							PreparedStatement ps2 = Mysql.PS("UPDATE UserInfo SET FlagCaptures = ? WHERE User = ?");
							ps2.setInt(1, rs.getInt("FlagCaptures") + 1);
							ps2.setInt(2, Mysql.GetUserID(p.getName()));
							ps2.executeUpdate();
						
						}
						
						
						Mysql.modifyUserPts(p.getName(), Games.FlagCap, RewardType.FlagCap);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 0), true);
						CTF.SpawnFlag(redflagnew, "red");
					}
				}
				
				if (Main.Gamemode == Gamemode.Mixed){
				if (Games.TeamKills.get("red") >= Games.ScoreMixed){
					DeathListener.WinningTeam("Red", ChatColor.RED);
				}
				else if (Games.TeamKills.get("blue") >= Games.ScoreMixed){
					DeathListener.WinningTeam("Blue", ChatColor.AQUA);
				}
				}
				else if (Main.Gamemode == Gamemode.CTF){
					if (Games.BlueFlagCaps >= Games.FlagsToCap)
					DeathListener.WinningTeam("Blue", ChatColor.AQUA);
					else if (Games.RedFlagCaps >= Games.FlagsToCap)
					DeathListener.WinningTeam("Red", ChatColor.RED);
				}
				
				
				}
			
		if (Games.BlueFlagHolder == p || Games.RedFlagHolder == p){
			if (p == Games.BlueFlagHolder)
			Games.BlueFlagLoc = p.getLocation();
			else if (p == Games.RedFlagHolder)
			Games.RedFlagLoc = p.getLocation();
		}
		
		if (Compass.get(p.getName()) == null){
			if (JoinListener.blue.contains(p))
				Compass.put(p.getName(), "RedTeam");
			else if (JoinListener.red.contains(p))
				Compass.put(p.getName(), "BlueTeam");
			else 
				Compass.put(p.getName(), "BlueTeam");
		}
		else if (Compass.get(p.getName()) == "RedTeam"){
		p.setCompassTarget(Games.RedFlagLoc);
		}
		else if (Compass.get(p.getName()) == "BlueTeam"){
		p.setCompassTarget(Games.BlueFlagLoc);
		}
		if (!JoinListener.blue.contains(p) && !JoinListener.red.contains(p) && !JoinListener.spectators.contains(p))
		Games.SpecJoin(p);
		
		}
	}
	}
	}
	}
	}
