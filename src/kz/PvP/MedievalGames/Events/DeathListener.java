package kz.PvP.MedievalGames.Events;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Vote;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.KitType;
import kz.PvP.MedievalGames.Enums.RewardType;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.Timers.EndGameTimer;
import kz.PvP.MedievalGames.Timers.PreGameTimer;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Message;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;


public class DeathListener extends JavaPlugin implements Listener{
	public static Main plugin;
	//WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
	
	public DeathListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	public static int RewardKill = 1;
	public static int RespawnTimer = 5;
	public static HashMap<String, Integer> x = new HashMap<String, Integer>();
	public static HashMap<String, Integer> PrevKills = new HashMap<String, Integer>();
	public static HashMap<String, Integer> PrevDeaths = new HashMap<String, Integer>();

	public static HashMap<String, BukkitTask> BT = new HashMap<String, BukkitTask>();
	public static HashMap<String, Boolean> Taken = new HashMap<String, Boolean>();
    int task = 0;
	
	int respawntimer;
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onKill (PlayerDeathEvent e) throws SQLException{
		if (e.getEntity() instanceof Player){
			final Player p = e.getEntity();
			Location loc = new Location(p.getWorld(), p.getLocation().getX(), p.getLocation().getY(),
			p.getLocation().getZ());
			
			
			Location bloodloc = p.getLocation().add(0, 1, 0);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			
			if (Games.BlueFlagHolder == p){
				CTF.SpawnFlag(loc, "blue");
				CTF.FlagDespawnTimer(loc, "blue");
			}
			else if (Games.RedFlagHolder == p){
				CTF.SpawnFlag(loc, "red");
				CTF.FlagDespawnTimer(loc, "red");
			}
			
			for (ItemStack im : e.getDrops()){
				int item = im.getTypeId();
		        if (item == 261 || item == 262 || item == 283 || item == 268 || item == 267 || item == 272 || item == 276 || item == 329 ||item == 345 || item ==  388 || item == 398 || item == 288 || item == 159 || (item >= 298 && item <= 317) || (item == 35 && (im.getDurability() == (short)14 || im.getDurability() == (short)11)))
		        	im.setTypeId(0);
		        else if ((item == 399 || item == 370) && im.getAmount() >= 5){
		        	im.setAmount(5);
		        }
			}
			e.getDrops().clear();
		    for (PotionEffect effect : p.getActivePotionEffects())
		        p.removePotionEffect(effect.getType());
			
			if (p.getKiller() instanceof Player){
				
			//Mysql.dbSubmit("INSERT INTO DEATHS (Victim, Killer) VALUES ('" + Mysql.GetUserID(p.getName()) + "','" + Mysql.GetUserID(p.getKiller().getName()) + "')");
			PreparedStatement ps = Mysql.PS("INSERT INTO DEATHS (Victim, Killer) VALUES (?,?)");
			ps.setInt(1, Mysql.GetUserID(p.getName()));
			ps.setInt(2, Mysql.GetUserID(p.getKiller().getName()));
			ps.executeUpdate();
			
			
			
			Mysql.modifyUserPts(p.getKiller().getName(), RewardKill, RewardType.Kill);
		    
		    
			if (JoinListener.red.contains(p) || (JoinListener.blue.contains(p))){
				ChatColor KILL = ChatColor.GOLD;
				ChatColor DEAD = ChatColor.GOLD;
				
				if (Games.Kills.containsKey(p.getKiller().getName()))
				Games.Kills.put(p.getKiller().getName(), Games.Kills.get(p.getKiller().getName().toLowerCase()) + 1);
				else{
					ResultSet rs = Mysql.dbGet("SELECT COUNT(*) as TOTALKILLS FROM DEATHS WHERE Killer = '" + Mysql.GetUserID(p.getKiller().getName()) + "'");
					if (!Games.Kills.containsKey(p.getKiller().getName()) && rs.next())
						Games.Kills.put(p.getKiller().getName(), rs.getInt("TOTALKILLS"));
				}
				
				
				
				if ((Games.TeamKills.get("blue") == 0) && Games.TeamKills.get("red") == 0)
				Bukkit.broadcastMessage(ChatColor.GOLD + p.getKiller().getName() + " has gotten FIRST BLOOD!");
				
				
				if (JoinListener.red.contains(p)){// Blue Kill
				/*
				if (!Games.BlueKills.containsKey(p.getName()))
				Games.BlueKills.put(p.getKiller().getName(), 1);
				*/
				DEAD = ChatColor.RED;
				KILL = ChatColor.AQUA;
				
				Games.TeamKills.put("blue", Games.TeamKills.get("blue") + 1);
				}
				else if (JoinListener.blue.contains(p)){//Red Kill
				//if (!Games.RedKills.containsKey(p.getName()))
				//Games.RedKills.put(p.getKiller().getName(), 1);
				DEAD = ChatColor.AQUA;
				KILL = ChatColor.RED;
				Games.TeamKills.put("red", Games.TeamKills.get("red") + 1);
				
				
				}
				
				if (PrevKills.containsKey(p.getName()))
					PrevKills.remove(p.getName());
				
				
				if (PrevDeaths.containsKey(p.getKiller().getName()))
					PrevDeaths.remove(p.getKiller().getName());
				
				if (!PrevDeaths.containsKey(p.getName()))
				PrevDeaths.put(p.getName(), 0);
				
				PrevDeaths.put(p.getName(), PrevDeaths.get(p.getName()) + 1);

				
				if (!PrevKills.containsKey(p.getKiller().getName()))
				PrevKills.put(p.getKiller().getName(), 0);
				
				PrevKills.put(p.getKiller().getName(), PrevKills.get(p.getKiller().getName()) + 1);
				
				
				if (PrevKills.get(p.getKiller().getName()) % 4 == 0){
					Message.G(p.getKiller().getDisplayName() + ChatColor.GOLD + " is on a " + PrevKills.get(p.getKiller().getName()) + " killstreak!");
					Message.P(p.getKiller(), "You have been rewarded with strength for " + PrevKills.get(p.getKiller().getName()) * 5 + " seconds!");
					//Put potion effect
    				p.getKiller().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * PrevKills.get(p.getKiller().getName()), 0));
    				p.getKiller().setHealth(20.0);
				}
				
				
				
				
				
				String weapon = p.getKiller().getItemInHand().getType().name().replace("_", " ").toLowerCase();
				if ((((Games.TeamKills.get("blue") >= Games.ScoreMixed) || (Games.TeamKills.get("red") >= Games.ScoreMixed)) && Main.Gamemode == Gamemode.Mixed) || (((Games.TeamKills.get("blue") >= Games.KillsTDM) || (Games.TeamKills.get("red") >= Games.KillsTDM)) && Main.Gamemode == Gamemode.TDM))
				e.setDeathMessage(p.getKiller().getName() + ChatColor.GOLD + " has gotten the final kill, on " + p.getName());
				else{
				if (plugin.getServer().getOnlinePlayers().length <= 25)
					e.setDeathMessage(RandomlyGenerateFunnyQuote(KILL + p.getKiller().getName() + ChatColor.GRAY, DEAD + p.getName() + ChatColor.GRAY, weapon));
				}
				double lobbyx = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "X");
				double lobbyy = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Y");
				double lobbyz = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Z");
				final Location LobbyLoc = new Location(Bukkit.getWorld("lobby"),lobbyx, lobbyy,lobbyz);
				
				
				if (Main.Gamemode == Gamemode.Mixed){
				if (Games.TeamKills.get("red") >= Games.ScoreMixed){
					WinningTeam("Red", ChatColor.RED);
					p.teleport(LobbyLoc);
					p.getKiller().teleport(LobbyLoc);
				}
				else if (Games.TeamKills.get("blue") >= Games.ScoreMixed){
					WinningTeam("Blue", ChatColor.AQUA);
					p.teleport(LobbyLoc);
					p.getKiller().teleport(LobbyLoc);
				}
				}
				else if (Main.Gamemode == Gamemode.TDM){
					if (Games.TeamKills.get("red") >= Games.KillsTDM){
						WinningTeam("Red", ChatColor.RED);
						p.teleport(LobbyLoc);
						p.getKiller().teleport(LobbyLoc);
					}
					else if (Games.TeamKills.get("blue") >= Games.KillsTDM){
						WinningTeam("Blue", ChatColor.AQUA);
						p.teleport(LobbyLoc);
						p.getKiller().teleport(LobbyLoc);
					}
				}
				else if (Main.Gamemode == Gamemode.TeamElimination){
					JoinListener.red.remove(p);
					JoinListener.blue.remove(p);
					Games.SpecJoin(p);
					
					if (JoinListener.blue.size() >= 1 && JoinListener.red.size() == 0){
					WinningTeam("Blue", ChatColor.AQUA);
					p.teleport(LobbyLoc);
					}
					else if (JoinListener.red.size() >= 1 && JoinListener.blue.size() == 0){
					WinningTeam("Red", ChatColor.RED);
					p.teleport(LobbyLoc);
					}
				}
				
				
			}
			}
			else if (p.getLastDamageCause() != null){
				DamageCause dc = p.getLastDamageCause().getCause();
				String dead = p.getName();
				if (JoinListener.blue.contains(p))
				dead = ChatColor.AQUA + dead + ChatColor.GRAY;
				if (JoinListener.red.contains(p))
				dead = ChatColor.RED + dead + ChatColor.GRAY;
				
				if (dc == DamageCause.BLOCK_EXPLOSION){
					e.setDeathMessage(dead + " just learned what explosions are.");
				}
				else if (dc == DamageCause.FALL){
					e.setDeathMessage(dead + " has just learned what gravity is.");
				}
				else if (dc == DamageCause.FALLING_BLOCK){
					e.setDeathMessage(ChatColor.GRAY + "No more falling blocks for you, " + dead);
				}
				else if (dc == DamageCause.FIRE){
					e.setDeathMessage(dead + " was burned to death.");
				}
				else if (dc == DamageCause.LAVA){
					e.setDeathMessage(dead + " took a bath in a lake of fire.");
				}
				else if (dc == DamageCause.LIGHTNING){
					e.setDeathMessage(dead + " did not listen to the weatherman.");
				}
				else if (dc == DamageCause.POISON){
					e.setDeathMessage(dead + " did not know poison is bad for health.");
				}
				else if (dc == DamageCause.MAGIC){
					e.setDeathMessage(ChatColor.GRAY + "Herobrine killed " + dead);
				}
				else if (dc == DamageCause.STARVATION){
					e.setDeathMessage(dead + " Weird death... Starvation is disabled...");
				}
				else if (dc == DamageCause.SUFFOCATION){
					e.setDeathMessage(dead + " thought walls are not solid...");
				}
				else if (dc == DamageCause.DROWNING){
					e.setDeathMessage(dead + " forgot to take a breath of fresh air.");
				}
				else if (dc == DamageCause.SUICIDE){
					e.setDeathMessage(dead  + " ");
				}
				else if (dc == DamageCause.VOID){
					e.setDeathMessage(dead + " fell through the world...");
				}
				else{
					e.setDeathMessage(null);
				}
				PreparedStatement ps = Mysql.PS("INSERT INTO DEATHS (Victim, Killer) VALUES (?,0)");
				ps.setInt(1, Mysql.GetUserID(p.getName()));
				ps.executeUpdate();
			
			}
			if (p.isDead() == true){
				if (JoinListener.red.contains(p) || JoinListener.blue.contains(p)){
					p.setHealth(20.0);
					if (p.getVehicle() != null)
					p.getVehicle().eject();
			        for (ItemStack i : p.getInventory().getContents())
			        {
			        	if (i != null){
			        	int item = i.getTypeId();	
			        	
			        	if (Kits.HasItemAbility(p, KitType.ITEM, i, null))
			        	i.setAmount(0);
			        	
				        if ((item == 159 && (i.getDurability() == (short)14 || i.getDurability() == (short)11))){i.setAmount(0);}
				        else{
				        if (i == null || i.getType() == Material.AIR || i.getTypeId() == 0){
				        	
				        }
				        else{
			            if (item == 399 || item == 370){
			            	double r = Math.random();
			            	if (r <= 0.05)
			            	i.setAmount(i.getAmount());
			            	else if (r <= 0.10)
				            i.setAmount((int) ((int)i.getAmount() * 0.75));
			            	else if (r <= 0.25)
			            	i.setAmount((int) ((int)i.getAmount() * 0.5));
			            	else if (r <= 0.5)
				            i.setAmount((int) ((int)i.getAmount() * 0.40));
			            	else if (r <= 0.75)
				            i.setAmount((int) ((int)i.getAmount() * 0.30));
			            	else
				            i.setAmount((int) ((int)i.getAmount() * 0.2));
			            }
			            if (i.getAmount() >= 1)
				        p.getWorld().dropItemNaturally(p.getLocation(), i);
				        }
			            p.getInventory().remove(i);
				        }
				        }
			        }
			        p.getInventory().clear();
			        p.closeInventory();
			        if (Main.Gamemode == Gamemode.TeamElimination){
						JoinListener.red.remove(p);
						JoinListener.blue.remove(p);
						Games.SpecJoin(p);
						Global.GiveTools(p, true);
						Message.P(p, Message.YouJustgotEliminated);
						if (JoinListener.blue.size() >= 1 && JoinListener.red.size() == 0)
						WinningTeam("Blue", ChatColor.AQUA);
						else if (JoinListener.red.size() >= 1 && JoinListener.blue.size() == 0)
						WinningTeam("Red", ChatColor.RED);
					}
			        else{
			        if (BT.containsKey(p.getName())){
	    			BT.get(p.getName()).cancel();
    	    		BT.remove(p.getName());
			        }
			    	x.put(p.getName(), RespawnTimer);
					double deadx = (double) Config.getConfig(ConfigFile.Teleports).get("death" + ".Loc." +  "X");
					double deady = (double) Config.getConfig(ConfigFile.Teleports).get("death" + ".Loc." +  "Y");
					double deadz = (double) Config.getConfig(ConfigFile.Teleports).get("death" + ".Loc." +  "Z");
					
					Location deadspawn = new Location(Bukkit.getWorld("war"),deadx, deady,deadz);
					p.teleport(deadspawn);
					for (PotionEffect pe : p.getActivePotionEffects())
					p.removePotionEffect(pe.getType());
					p.sendMessage(ChatColor.GRAY + "Respawning in " + RespawnTimer + " seconds.");
					p.setVelocity(new Vector());
					BukkitTask task = new kz.PvP.MedievalGames.RepeatingTasks.RespawnTimer(p).runTaskTimerAsynchronously(plugin, 20L, 20L);
			    	BT.put(p.getName(), task);
			        }
					
					
				}
				else{
					Global.GiveTools(p, true);
				}
			}
		}
	}
	
	private String RandomlyGenerateFunnyQuote(String killer, String dead, String weapon) {
		String FunnyQuote = null;
		double r = Math.random();
		if (r <= 0.05)
		FunnyQuote = killer + " has brutally murdered " + dead + " while wielding a " + weapon;
		else if (r <= 0.1)
		FunnyQuote = killer + " killed " + dead + " wielding a " + weapon;
		else if (r <= 0.15)
		FunnyQuote = dead + " forgot how to PVP, and lost against " + killer;
		else if (r <= 0.2)
		FunnyQuote = dead + " thought a " + weapon + " was not enough to kill in the hands of " + killer;
		else if (r <= 0.25)
		FunnyQuote = dead + " forgot how to use a weapon unlike " + killer;
		else if (r <= 0.3)
		FunnyQuote = dead + " thought it was all fun and games when against " + killer;
		else if (r <= 0.35)
		FunnyQuote = dead  + " vs " + killer + " = the victory of " + killer;
		else if (r <= 0.4)
		FunnyQuote = killer + " + " + weapon  + " = the death of " + dead;
		else if (r <= 0.45)
		FunnyQuote = dead + " was being a royal pain.";
		else if (r <= 0.5)
		FunnyQuote = dead + " spoke an infinite deal of nothing.";
		else if (r <= 0.55)
		FunnyQuote = dead + " thought they were Chuck Norris";
		else if (r <= 0.6)
		FunnyQuote = killer + " has stolen " + dead + "'s soul.";
		else if (r <= 0.65)
		FunnyQuote = dead + " could not tell right-click from left-click.";
		else if (r <= 0.7)
		FunnyQuote = killer + " has destroyed " + dead + "'s soul.";
		else if (r <= 0.75)
		FunnyQuote = dead + " decided life is not worth living when faced against " + killer + ".";
		else if (r <= 0.8)
		FunnyQuote = dead + " just learned that the cake was a lie with the help of " + killer + ".";
		else if (r <= 0.85)
		FunnyQuote = killer + " has pwned " + dead;
		else if (r <= 0.9)
		FunnyQuote = killer + " has brutally murdered " + dead;
		else if (r <= 0.95)
		FunnyQuote = killer + " has brutally murdered " + dead;
		else
		FunnyQuote = killer + " has brutally murdered " + dead;
		return FunnyQuote;
	}

	@EventHandler
	public void onEntityDeath(final EntityDeathEvent e) {

	    
	    if (e.getEntity() instanceof Horse){
			final UUID ID = e.getEntity().getUniqueId();
			if (Games.BlueHorse.containsKey(ID)){
				// Blue Horse
				final String a = Games.BlueHorse.get(ID);
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                    	Games.BlueHorse.remove(ID);
        				Global.SpawnBlueHorse(a);
                        }
                        }, 20L * 10);
			}
			else if (Games.RedHorse.containsKey(ID)){
				// Red Horse
				final String a = Games.RedHorse.get(ID);
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                    	Games.RedHorse.remove(ID);
                    	Global.SpawnRedHorse(a);
                        }
                        }, 20L * 10);
			}
			e.getDrops().clear();
			if (e.getEntity() instanceof Horse){
				Horse h = (Horse) e.getEntity();
				h.getInventory().setArmor(null);
				h.getInventory().setSaddle(null);
				h.getInventory().clear();
			}
		}
	}
	
	
	@EventHandler
	public void Test (final PlayerToggleSneakEvent e){
		if (!Taken.containsKey(e.getPlayer().getName()))
			Taken.put(e.getPlayer().getName(), false);
		final Player p = e.getPlayer();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
		        if (e.isSneaking() != p.isSneaking()){
		        	for (ItemStack is : p.getInventory().getContents()){
		        		if (is != null && is.getTypeId() == 399){
		        			if (Taken.get(p.getName()) == false){
		        			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() == true && is.getItemMeta().getDisplayName().toLowerCase().contains("player bandaid"))
		        			
		        			if (p.hasPermission("mw.instantheal")){
			        			if (p.getHealth() == p.getMaxHealth())
			        				p.sendMessage(ChatColor.GRAY + "You are currently at full health.");
			        				else if (p.getHealth() < p.getMaxHealth()){
			        					p.setHealth(p.getMaxHealth());
			        					p.sendMessage(ChatColor.GREEN + "You have healed yourself to full health & removed all potion effects.");
			        			
			        					p.setFireTicks(0);
			        				    for (PotionEffect effect : p.getActivePotionEffects()){
			        				        
			        				    	if (effect.getDuration() <= 20 * 60 * 5)
			        				    	p.removePotionEffect(effect.getType());
			        				    }
			        					
			        			if (is.getAmount() > 1){
			        			is.setAmount(is.getAmount() - 1);	
			        			}
			        			else{
			        			p.getInventory().remove(Material.NETHER_STAR);
			        			is.setTypeId(0);
			        			p.getInventory().remove(is);
			        			}
			        			}
	        					p.updateInventory();
			        			
			        			
			        			Taken.put(e.getPlayer().getName(), true);
			        			
			        			
		        			}
		        			}
		        			
		        			
		        			
		        		}
		        		
		        	}
        			Taken.put(e.getPlayer().getName(), false);
		        	
		        	
		        	
		        }
				}
			}, 3L);
		
	}
	
	public static void WinningTeam(String string, ChatColor chat) {
		Bukkit.getScheduler().cancelTask(EndGameTimer.pgtimer);
		for (Entry<String, BukkitTask>  task: BT.entrySet()){
			task.getValue().cancel();
		}
		Main.Gamemode = kz.PvP.MedievalGames.Enums.Gamemode.Voting;
		if (string != "none"){
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.BOLD + "" + chat+ "=================================");
		Bukkit.broadcastMessage(chat + string +  " team has won this battle!");
		Bukkit.broadcastMessage(chat + "Next round begins in " + PreGameTimer.OR + " seconds.");
		Bukkit.broadcastMessage(chat + "Teams are the same as last round. To change type" + ChatColor.GOLD + " /Team");
		Bukkit.broadcastMessage(ChatColor.BOLD + "" + chat + "=================================");
		PreGameTimer.BeginTimer();
		double lobbyx = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "X");
		double lobbyy = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Y");
		double lobbyz = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Z");
		final Location LobbyLoc = new Location(Bukkit.getWorld("lobby"),lobbyx, lobbyy,lobbyz);

		for (Player p : Bukkit.getOnlinePlayers()){
			p.setDisplayName(ChatColor.GRAY + p.getName());
			if (p.getVehicle() != null)
			p.getVehicle().eject();
			p.teleport(LobbyLoc);
			p.getInventory().clear();
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.setExp(0);
			p.setGameMode(GameMode.SURVIVAL);
			p.setAllowFlight(true);
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.getInventory().setLeggings(null);
			p.getInventory().setBoots(null);
		}
		
		for ( Entity e : Bukkit.getWorld("war").getEntities()){
			if (e.getType() != EntityType.PLAYER)
			e.remove();
		}
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				
				for (Player pl : Bukkit.getServer().getOnlinePlayers()){
			        if (Main.Game == Game.PREGAME && (!Games.VoteChoices.containsKey(pl.getName()))){
					Vote.ShowVoteMenu(pl);
					}
				}
				
				for (Entry<String, BukkitTask>  task: BT.entrySet()){
					task.getValue().cancel();
				}
				}
			}, 20L * 1);
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
					        LoadMapSave("war");
						}
					}, 20L * 7);
		}
		
		
		
		//plugin.getServer().getWorld("war").getBlockAt(Games.BlueFlagLoc).setTypeId(0);
		//plugin.getServer().getWorld("war").getBlockAt(Games.RedFlagLoc).setTypeId(0);
		Main.GamemodeVoting = true;
		Main.Gamemode = Gamemode.Voting;
		Main.Game = Game.PREGAME;
		Games.VoteChoices.clear();
		//////////////////////////////////////////////////////////////////////////////
		JoinListener.Boards.clear();
		JoinListener.Objectives.clear();
		Games.BlueFlagCaps = 0;
		Games.RedFlagCaps = 0;
		//Games.BlueKills.clear();
		//Games.RedKills.clear();
		Games.TeamKills.clear();
        
        
        
		for (Player p : JoinListener.spectators){
			for (Player pl : JoinListener.red){
				pl.showPlayer(p);
			}
			for (Player pl : JoinListener.blue){
				pl.showPlayer(p);
			}
		}
			JoinListener.spectators.clear();
	}
    public static void unloadMap(String mapname){
    	
        if(plugin.getServer().unloadWorld(mapname, false)){
            plugin.getLogger().info("Successfully unloaded " + mapname);
        }else{
            plugin.getLogger().severe("COULD NOT UNLOAD " + mapname);
        }
    }
    //Loading maps (MUST BE CALLED AFTER UNLOAD MAPS TO FINISH THE ROLLBACK PROCESS)
    public static void loadMap(final String mapname){
	     Runnable map = new Runnable() {
	         public void run() {
    	World w = Bukkit.getServer().createWorld(new WorldCreator(mapname));
        w.setAutoSave(false);
        w.setDifficulty(Difficulty.EASY);
        w.setTime(0);
        w.setPVP(true);
	         }
	     };
	     ExecutorService executor = Executors.newCachedThreadPool();
	     executor.submit(map);

    }

	@EventHandler
	public void onRespawn (PlayerRespawnEvent e){
			Player p = e.getPlayer();
			if (JoinListener.red.contains(p) || (JoinListener.blue.contains(p))){
				if (Main.Game != Game.PREGAME){
					try {
						Games.Start(p, true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			else{
				Global.GiveTools(p, true);
			}
		}
	
	
	
	
	
	public static void LoadMapSave(final String mapname){
		System.out.println("Deleting old world.");
		double lobbyx = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "X");
		double lobbyy = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Y");
		double lobbyz = (double) Config.getConfig(ConfigFile.Teleports).get("mainlobby" + ".Loc." +  "Z");
		final Location LobbyLoc = new Location(Bukkit.getWorld("lobby"),lobbyx, lobbyy,lobbyz);

    	
    	World world = plugin.getServer().getWorld(mapname);
    	
    	
    	if (world != null){
        	for (Player p: world.getPlayers()){
        		p.teleport(LobbyLoc);
        	}
        if(plugin.getServer().unloadWorld(mapname, false)){
            plugin.getLogger().info("Successfully unloaded " + mapname);
            deleteDirectory(world.getWorldFolder());
			
            plugin.getLogger().info("Copying saved world. (War)");
			try {
				copyDirectory(new File(plugin.getDataFolder(), "war"),
						new File("war"));
			} catch (IOException e) {
				plugin.getLogger().warning("Error: " + e.toString());
			}
			loadMap(mapname);
			}else{
        	plugin.getLogger().severe("Map cannot be unloaded " + mapname);
        }
    	}
    	else {
            plugin.getLogger().info("Copying saved world. (War)");
			try {
				copyDirectory(new File(plugin.getDataFolder(), "war"),
						new File("war"));
			} catch (IOException e) {
				plugin.getLogger().warning("Error: " + e.toString());
			}
			loadMap(mapname);
    		
    	}
    	
    	
    	
	}
	
	private static void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean deleteDirectory(File path) {
        if( path.exists() ) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
            deleteDirectory(files[i]);
            }
                else {
                    files[i].delete();
                } //end else
            }
            }
        return( path.delete() );
        }
	
	
	
	
}
