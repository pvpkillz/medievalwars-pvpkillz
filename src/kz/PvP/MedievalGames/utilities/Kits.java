package kz.PvP.MedievalGames.utilities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Mysql;
import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.KitType;
import kz.PvP.MedievalGames.Events.DamageListener;
import kz.PvP.MedievalGames.Events.InteractListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Config.ConfigFile;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;



public class Kits {
	public static HashMap<Player, String> KitChoice = new HashMap<Player, String>();
	public static HashMap<Player, String> KitRound = new HashMap<Player, String>();
	public static ArrayList<String> Kits = new ArrayList<String>();
	public Main plugin;
	
	public Kits(Main mainclass) {
	 	this.plugin = mainclass;

		Set<String> kitList = Config.getConfig(ConfigFile.Kits).getKeys(false);
		for(String kit : kitList) {
			Kits.add(kit.toLowerCase());
		}
		}
	@SuppressWarnings("deprecation")
	public static void GiveKit (Player p) throws SQLException{
		p.getInventory().clear();
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.setExp(0);
		p.setLevel(0);
		p.eject();
	    for (PotionEffect effect : p.getActivePotionEffects())
	        p.removePotionEffect(effect.getType());
	    
	    InteractListener.Charges.remove(p.getName());
		InteractListener.IncreaseHorseHP1.remove(p.getName());
		InteractListener.IncreaseHorseHP2.remove(p.getName());
		InteractListener.IncreaseHorseHP3.remove(p.getName());
		InteractListener.Compass.remove(p.getName());
		InteractListener.Crossbow.remove(p.getName());
		InteractListener.Longbow.remove(p.getName());
		InteractListener.PoisonousShooters1.remove(p.getName());
		InteractListener.PoisonousShooters2.remove(p.getName());
		InteractListener.PoisonousShooters3.remove(p.getName());
		DamageListener.ArcherBlock1.remove(p.getName());
		DamageListener.ArcherBlock2.remove(p.getName());
		DamageListener.ArcherBlock3.remove(p.getName());
		DamageListener.IncreaseBlock1.remove(p.getName());
		DamageListener.IncreaseBlock2.remove(p.getName());
		DamageListener.IncreaseBlock3.remove(p.getName());
		DamageListener.ShieldBlocks.remove(p.getName());
		
		
		
		
		
		if (JoinListener.blue.contains(p) || JoinListener.red.contains(p)){
		if (!KitChoice.containsKey(p)){
		// If player did not choose a kit
		}
		else {
			if (KitRound.containsKey(p))
			KitRound.remove(p);
			KitRound.put(p, KitChoice.get(p));
			ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitChoice.get(p));
			List<String> kititems = kit.getStringList("ITEMS");
			for (String item : kititems) {
				String[] oneitem = item.split(",");
				ItemStack i = null;
				Integer id = null;
				Integer amount = null;
				Short durability = null;
				
				int blue = 0;
				int green = 0;
				int red = 0;
				if (JoinListener.blue.contains(p))
				{
				blue = 255;
				green = 0;
				red = 0;
				}
				else if (JoinListener.red.contains(p))
				{
				blue = 0;
				green = 0;
				red = 255;
				}			
				
				if (oneitem[0].contains(":")) {
					String[] ITEM_ID = oneitem[0].split(":");
					id = Integer.valueOf(Integer.parseInt(ITEM_ID[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					durability = Short.valueOf(Short.parseShort(ITEM_ID[1]));
					i = new ItemStack(id.intValue(), amount.intValue(),
							durability.shortValue());
				} else {
					id = Integer.valueOf(Integer.parseInt(oneitem[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					i = new ItemStack(id.intValue(), amount.intValue());
				}
				if (oneitem.length >= 8) {
					i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(oneitem[6])),
							Integer.parseInt(oneitem[7]));
			}
				if (oneitem.length >= 6) {
						i.addUnsafeEnchantment(
								Enchantment.getById(Integer.parseInt(oneitem[4])),
								Integer.parseInt(oneitem[5]));
				}
				if (oneitem.length >= 4) {
					i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(oneitem[2])),
							Integer.parseInt(oneitem[3]));
				}
				
				if (item.contains(">")){
				String[] I = item.split(">");
				ItemMeta im = i.getItemMeta();
				im.setDisplayName(I[1]);
				i.setItemMeta(im);
				}
				
				String upgrades = Mysql.GetUserActiveUps(p.getName());
				Set<String> upgradelist = Config.getConfig(ConfigFile.Upgrades).getConfigurationSection(KitChoice.get(p)).getKeys(false);
				for(String upgrade : upgradelist){
				if (upgrades != null && upgrades.contains(" " + KitChoice.get(p).toLowerCase() + ":" + upgrade.toLowerCase())){
				int upgradeitem = Config.getConfig(ConfigFile.Upgrades).getInt(KitChoice.get(p) + "." + upgrade + ".UpgradeItem");
				if (upgradeitem == id){
				String enchantment = Config.getConfig(ConfigFile.Upgrades).getString(KitChoice.get(p) + "." + upgrade + ".Upgrade");
				String[] enchlist = enchantment.split(",");
				int enchlvl = Integer.parseInt(enchlist[1]);
				int enchid = Integer.parseInt(enchlist[0]);
				i.addUnsafeEnchantment(Enchantment.getById(enchid), enchlvl);
				}
				if ((upgradeitem == 3000) || (upgradeitem == 4000) || (upgradeitem == 5000) || (upgradeitem == 6000)){
					
					int enchantmentid = 0;
					if (upgradeitem == 3000)// 3000 = Fire Protection
					enchantmentid = 1;
					else if (upgradeitem == 4000)// 4000 = Blast protection
					enchantmentid = 3;
					else if (upgradeitem == 5000)// 5000 = Projectile protection
					enchantmentid = 4;
					else if (upgradeitem == 6000)// 6000 = Protection
					enchantmentid = 0;
					
					String enchantment = Config.getConfig(ConfigFile.Upgrades).getString(KitChoice.get(p) + "." + upgrade + ".Upgrade");
					String[] enchlist = enchantment.split(",");
					int enchlvl = Integer.parseInt(enchlist[1]);
					
					
					if ((id.intValue() == 299) || (id.intValue() == 303)
							|| (id.intValue() == 307) || (id.intValue() == 311)
							|| (id.intValue() == 315)) {
						// Chestplate
						i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
					} else if ((id.intValue() == 300) || (id.intValue() == 304)
							|| (id.intValue() == 308) || (id.intValue() == 312)
							|| (id.intValue() == 316)) {
						// Leggings
						i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
					} else if ((id.intValue() == 301) || (id.intValue() == 305)
							|| (id.intValue() == 309) || (id.intValue() == 313)
							|| (id.intValue() == 317)) {
							// Boots
							i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
					}
				}
				else if (upgradeitem >= 10000){//Custom Upgrades which require coding
					ItemMeta im2 = i.getItemMeta();
					if (upgradeitem == 10000){
					ChangeItemname(ChatColor.GOLD + "Crossbow",i);
					}
					else if (upgradeitem == 10001){
					ChangeItemname(ChatColor.GOLD + "Maple Longbow",i);
					}
					if (upgradeitem == 10002)
					InteractListener.PoisonousShooters1.add(p.getName());
					else if (upgradeitem == 10003)
					InteractListener.PoisonousShooters2.add(p.getName());
					else if (upgradeitem == 10004)
					InteractListener.PoisonousShooters3.add(p.getName());
					if (upgradeitem == 10010)
					InteractListener.IncreaseHorseHP1.add(p.getName());
					else if (upgradeitem == 10011)
					InteractListener.IncreaseHorseHP2.add(p.getName());
					else if (upgradeitem == 10012)
					InteractListener.IncreaseHorseHP3.add(p.getName());
					if (upgradeitem == 10020)
					DamageListener.IncreaseBlock1.add(p.getName());
					else if (upgradeitem == 10021)
					DamageListener.IncreaseBlock2.add(p.getName());
					else if (upgradeitem == 10022)
					DamageListener.IncreaseBlock3.add(p.getName());
					if (upgradeitem == 10025)
					DamageListener.ArcherBlock1.add(p.getName());
					else if (upgradeitem == 10026)
					DamageListener.ArcherBlock2.add(p.getName());
					else if (upgradeitem == 10027)
					DamageListener.ArcherBlock3.add(p.getName());
					i.setItemMeta(im2);
				}
				}
				
				
				}
				
				
				
				
				
				
				
				if ((id.intValue() < 298) || (317 < id.intValue())) {
					p.getInventory().addItem(new ItemStack[] { i });
				} else if ((id.intValue() == 298) || (id.intValue() == 302)
						|| (id.intValue() == 306) || (id.intValue() == 310)
						|| (id.intValue() == 314)) {
					if (id.intValue() == 298)
					{
					    LeatherArmorMeta c = (LeatherArmorMeta)i.getItemMeta();
					    c.setColor(Color.fromBGR(blue, green, red));
					    i.setItemMeta(c);
					}
					i.setAmount(1);
					p.getInventory().setHelmet(i);
				} else if ((id.intValue() == 299) || (id.intValue() == 303)
						|| (id.intValue() == 307) || (id.intValue() == 311)
						|| (id.intValue() == 315)) {
					if (id.intValue() == 299)
					{
					    LeatherArmorMeta c = (LeatherArmorMeta)i.getItemMeta();
					    c.setColor(Color.fromBGR(blue, green, red));
					    i.setItemMeta(c);
					}
					i.setAmount(1);
					p.getInventory().setChestplate(i);
				} else if ((id.intValue() == 300) || (id.intValue() == 304)
						|| (id.intValue() == 308) || (id.intValue() == 312)
						|| (id.intValue() == 316)) {
					
					if (id.intValue() == 300)
					{
					    LeatherArmorMeta l = (LeatherArmorMeta)i.getItemMeta();
					    l.setColor(Color.fromBGR(blue, green, red));
					    i.setItemMeta(l);
					}
					
					i.setAmount(1);
					p.getInventory().setLeggings(i);
				} else if ((id.intValue() == 301) || (id.intValue() == 305)
						|| (id.intValue() == 309) || (id.intValue() == 313)
						|| (id.intValue() == 317)) {
					if (id.intValue() == 301)
					{
					    LeatherArmorMeta b = (LeatherArmorMeta)i.getItemMeta();
					    b.setColor(Color.fromBGR(blue, green, red));
					    i.setItemMeta(b);
					}
					i.setAmount(1);
					p.getInventory().setBoots(i);
				}
			}
			List<String> pots = kit.getStringList("POTION");
			for(String pot : pots) {
				if (pot != null & pot != "") {
					if (!pot.equals(0)) {
						String[] potion = pot.split(",");
						if (Integer.parseInt(potion[0]) != 0) {
							if (Integer.parseInt(potion[1]) == 0) {
								p.addPotionEffect(new PotionEffect(PotionEffectType
										.getById(Integer.parseInt(potion[0])),
										 120000, Integer
										.parseInt(potion[2])));
							} else {
								p.addPotionEffect(new PotionEffect(PotionEffectType
										.getById(Integer.parseInt(potion[0])), Integer
										.parseInt(potion[1]) * 20, Integer
										.parseInt(potion[2])));
							}
						}
					}
				}
			}
			
			int bandaids = kit.getInt("PlayerBandage");
			int Hbandaids = kit.getInt("HorseBandage");
			if (bandaids >= 1){
			ItemStack is = new ItemStack(Material.NETHER_STAR, bandaids);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(ChatColor.RED + "+ " + ChatColor.WHITE + "Player Bandaid" + ChatColor.RED + " +" );
			List<String> Lore = new ArrayList<String>();
			Lore.add(ChatColor.WHITE + "Use this item, to");
			Lore.add(ChatColor.WHITE + "instantly heal yourself.");
			Lore.add(ChatColor.WHITE + " ");
			Lore.add(ChatColor.WHITE + " ");
			if (p.hasPermission("mw.instantheal"))
			Lore.add(ChatColor.WHITE + "- Tap shift to instantly heal");
			else
			Lore.add(ChatColor.RED + "[VIP+] - Tap shift to instantly heal");
			
			
			
			
			im.setLore(Lore);
			is.setItemMeta(im);
			p.getInventory().addItem(is);
			Lore.clear();
			}
			if (Hbandaids >= 1){
			ItemStack is = new ItemStack(Material.GHAST_TEAR, bandaids);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(ChatColor.RED + "+ " + ChatColor.WHITE + "Horse Bandaid" + ChatColor.RED + " +" );
			List<String> Lore = new ArrayList<String>();
			Lore.add(ChatColor.WHITE + "Use this item, to");
			Lore.add(ChatColor.WHITE + "instantly heal a horse.");
			im.setLore(Lore);
			is.setItemMeta(im);
			p.getInventory().addItem(is);
			Lore.clear();
			}
			
			
			
			
		}
	}
	Global.GiveTools(p,false);
	}
	
	public static boolean HasItemAbility (Player p, KitType type, ItemStack is, PotionEffect pe){
		if (KitRound.get(p) != null){
		if (type == KitType.ITEM){
			ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitRound.get(p));
			List<String> items = kit.getStringList("ITEMS");
			for (String item : items){
				String[] oneitem = item.split(",");
				ItemStack i = null;
				Integer id = null;
				Integer amount = null;
				Short durability = null;
				if (oneitem[0].contains(":")) {
					String[] ITEM_ID = oneitem[0].split(":");
					id = Integer.valueOf(Integer.parseInt(ITEM_ID[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					durability = Short.valueOf(Short.parseShort(ITEM_ID[1]));
					i = new ItemStack(id.intValue(), amount.intValue(),
							durability.shortValue());
				} else {
					id = Integer.valueOf(Integer.parseInt(oneitem[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					i = new ItemStack(id.intValue(), amount.intValue());
				}
				String name = "";
				if (item.contains(">")){
				String[] I = item.split(">");
				name = I[1];
				}
				
				if (i != null && is != null && i.getTypeId() == is.getTypeId() && ((is.hasItemMeta() && is.getItemMeta().hasDisplayName()) && (ChatColor.stripColor(is.getItemMeta().getDisplayName()).toLowerCase().contains(name.toLowerCase())))){
				return true;
				}
			}
		}
		else if (type == KitType.POTION){
			ConfigurationSection kit = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitRound.get(p));
			List<String> potions = kit.getStringList("POTION");
			for (String pot : potions){
					if (pot != null & pot != "") {
						if (!pot.equals(0)) {
							String[] potion = pot.split(",");
							if (Integer.parseInt(potion[0]) != 0) {
								if (Integer.parseInt(potion[1]) == 0) {
									PotionEffect potioneffect = new PotionEffect(PotionEffectType
											.getById(Integer.parseInt(potion[0])),
											 120000, Integer
											.parseInt(potion[2]));
									if (pe == potioneffect)
									return true;
									
									
								} else {
									PotionEffect potioneffect = new PotionEffect(PotionEffectType
											.getById(Integer.parseInt(potion[0])), Integer
											.parseInt(potion[1]) * 20, Integer
											.parseInt(potion[2]));
									if (pe == potioneffect)
										return true;
									
								}
							}
						}
					}
			}
			
		}
		}
		return false;
	}
	
	public static void Choose(Player p, String kitname) throws SQLException {
		// TODO Auto-generated method stub
		  if (Kits.contains(kitname)){
			  Message.P(p, ChatColor.GRAY + "You have chosen the " + kitname.toLowerCase() + " class.");
			  if ((Main.Game != Game.PREGAME) && (!JoinListener.spectators.contains(p)) && (!Games.NotAttacked.contains(p.getName())))
			  Message.P(p, "The class change will be applied on death.");
			  else if (JoinListener.spectators.contains(p)){
				  Message.P(p, ChatColor.GRAY + "Type " + ChatColor.GOLD + "/Join " + ChatColor.GRAY + "to join the game and play!");
			  }
			  KitChoice.remove(p);
			  KitChoice.put(p, kitname);
			  if (Games.NotAttacked.contains(p.getName()) || ((!JoinListener.spectators.contains(p)) && (Main.Game == Game.PREPARING))){
				  GiveKit(p);
				  Message.P(p, "Your class has been changed.");
			  }
		  }
		  else
			  Message.P(p, "Invalid kit chosen");
	}
	
	
	
	
	public static boolean CheckAbility(Player p, Integer id){
		ConfigurationSection Class = Config.getConfig(ConfigFile.Kits).getConfigurationSection(KitRound.get(p));
		if (Class.contains("ABILITIES")){
		List<Integer> abilities = Class.getIntegerList("ABILITIES");
			for (Integer ID : abilities){
				if (ID == id){
					return true;
				}
			}
		
		}
		return false;
	}
	
	
	public static void ChangeItemname(String newname, ItemStack is){
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(newname);
		is.setItemMeta(im);
	}
	
	
	
	
	
}
