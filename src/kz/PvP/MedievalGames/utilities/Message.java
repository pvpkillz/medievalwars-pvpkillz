package kz.PvP.MedievalGames.utilities;

import java.util.logging.Logger;

import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Timers.PreparingTimer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;



public class Message extends JavaPlugin{
	public static Main plugin;
	public static String PREFIX = ChatColor.RED + "[" + ChatColor.GOLD + "MW" + ChatColor.RED + "] " + ChatColor.GREEN;
	/*
	 *  Translation file (Will be extra, since this plugin will be used in english, but I need a challenge)
	 */
	
	public static final String ServerName = "PvPKillz";
	public static final String PrepareTimer =  "The war has begun! May the best team win!";
	public static final String NotEnoughOn = ChatColor.GREEN + "Not enough players on both teams to begin.";
	public static final String GameBegunSpec = ChatColor.LIGHT_PURPLE + "The games have begun, to play you must be on a team. Type " + ChatColor.GOLD + "/team" + ChatColor.LIGHT_PURPLE + " for more info.";
	public static final String DoNotNotify = ChatColor.GRAY + "You currently are using the default class. To choose a class type " + ChatColor.GREEN + "/class" + ChatColor.GRAY + ". Turn off this notification, with " + ChatColor.RED + "/mv notify off";
	public static final String Idle = ChatColor.GRAY + "You have been moved to spectators, due to idling for <time> minutes.";
	public static final String JoinGame = ChatColor.GREEN + "To join the game, type " + ChatColor.RED + "/join";
	public static final String YouAlreadyOwnUpgrade = ChatColor.RED + "You already own this upgrade.";
	public static final String SuccessPurchase = ChatColor.GREEN + "You have successfully purchased this upgrade!";
	public static final String CannotAfford = ChatColor.YELLOW + "You cannot afford this purchase. " + ChatColor.RED + "Cost: <cost> " + ChatColor.GRAY + "|" + ChatColor.GREEN + " Balance: <points>";
	public static final String DeactivatedUpgrade = ChatColor.RED + "You have deactivated this upgrade.";
	public static final String ActivatedUpgrade = ChatColor.GREEN + "Upgrade has been activated for the <class> class! To deactivate type " + ChatColor.GOLD + "/activate";
	public static final String UnactivatedUpgrade = ChatColor.RED + "Upgrade not activated. You must purchase the upgrade first. " + ChatColor.GOLD + "/Upgrade";
	public static final String CompassDesc = "Allows user to trace down \ntheir team's or the opposing \nteam's flag.";
	public static final String CTFDesc = "Your objective in this gamemode, is to capture the other team's flag a total of <flags> time(s). First team to capture their enemy's flag <flags> wins the round.";
	public static final String MixedDesc = "Your objective in this gamemode, is to capture the enemy's flag and kill enemy players as many times as you can. \n Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + 1 + " Point(s)\n" + ChatColor.GRAY + "Flag capture - " + ChatColor.GREEN + "<points> Point(s)";
	public static final String TDMDesc = "Your objective in this gamemode, is to kill enemy players as many times as you can. \n Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + " <perkill> Point(s)\n";
	public static final String TeamEliminationDesc = "Your objective in this gamemode, is to kill enemy players as many times as you can, as quickly as you can.\n " + "Players who die, will be kicked out of the round. First team, to eliminate the full opposing team, wins the round." + "\n" + "Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + "<perkill> Point(s)\n";;;
	public static String UpgradeDesc = "Use this item in hand,\n to open the upgrade menu.";
	public static String ClassDesc = "Use this item in hand, \nto open the class selection menu.";
	public static String ActivatorDesc = "Use this item in hand, \nto open the upgrade-activation menu.";
	public static final String GivenPoints = ChatColor.GRAY + "You have earned <earned> points, and have a total of <got> points.";
	public static final String NotEnoughMoney = ChatColor.RED + "You do not have enough coins. You need <need> and have only <got>";
	public static final String PurchaseSuccess = ChatColor.GREEN + "Successful transfer, you now have <got> after paying <paid>";
	public static final String EarnedPoints = ChatColor.GOLD + "You have earned $ points <reason>. Total: <curpoints>";
	public static final String YouJustgotEliminated = "You have just been eliminated from Team Eimination.";
	public static final String TimeLeft = "This round will end in <time>.";
	public static final String SwappedDueToBalance = "You have been swapped to the opposing team, due to auto-balance.";
	public static final String TypeHelpforHelp = "Type /help at anytime for more help.";
	public static final String SpectateCompassDesc = "Use this compass\nto teleport to users playing.";
	public static final String FeedbackPlease = "&aPlease help us &7make this gamemode better by leaving your feedback. Type &a/Feedback &7for more info.";
	public static final String TexturePackUsed = "Are you using our specially made resourcepack? Download it today from http://PvP.kz/MW.zip";
	public static final String AllPlayersRandomized = "All players have been assigned a random team. Beware this occurs every 5 rounds.";
	public static final String GivenBuffForDeath3 = "Deathstreak: You have been given a small strength buff.";
	public static final String GivenBuffForDeath5 = "Deathstreak: You have been given a medium strength buff.";
	public static final String GivenBuffForDeath10 = "Deathstreak: You have been given a strong strength buff.";

	public Message (Main mainclass){
		plugin = mainclass;
	}
	
	
	
	
	static Logger log = Bukkit.getLogger();
	
	public static void G (String Message){
		Message = ChatColor.translateAlternateColorCodes('&', Message);
		plugin.getServer().broadcastMessage(PREFIX + Message);
	}
	public static void P (Player p, String Message){
		Message = ChatColor.translateAlternateColorCodes('&', Message);
		p.sendMessage(PREFIX + Message);
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(Message) + " | sent to " + p.getName());
	}
	public static void NP (Player p, String Message){
		Message = ChatColor.translateAlternateColorCodes('&', Message);
		p.sendMessage(Message);
		log.info(ChatColor.stripColor(Message) + " | sent to " + p.getName());
	}
	public static void GPAll (Player p, String Message){
		Message = ChatColor.translateAlternateColorCodes('&', Message);

		for (Player pl : plugin.getServer().getOnlinePlayers()){
			if (pl != p)
			pl.sendMessage(PREFIX + Message);
		}
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(Message) + " | sent to all players.");
	}
	
	
	public static void G(String Message, Team team) {
		Message = ChatColor.translateAlternateColorCodes('&', Message);

		if (team == Team.Red){
			for (Player pl : JoinListener.red){
				pl.sendMessage(PREFIX + Message);
			}
		}
		if (team == Team.Blue){
			for (Player pl : JoinListener.blue){
				pl.sendMessage(PREFIX + Message);
			}
		}
		if (team == Team.Spectator){
			for (Player pl : JoinListener.spectators){
				pl.sendMessage(PREFIX + Message);
			}
		}
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(Message) + " | sent to all players of " + team + " team");
	}
	
	public static String Replacer(String fullstr, String replace, String find) {
		if (fullstr.toLowerCase().contains(find))
		replace = fullstr.replace(find, replace);
		else
		replace = fullstr;
		
	return replace;
	}
	
}
