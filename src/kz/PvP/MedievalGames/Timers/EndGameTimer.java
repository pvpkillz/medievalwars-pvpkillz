package kz.PvP.MedievalGames.Timers;

import java.sql.SQLException;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Events.DeathListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;



public class EndGameTimer extends JavaPlugin{
	public static int pgtimer;
	
	public static Main plugin;
	
	public EndGameTimer(Main main) {
	plugin = main;
	}
	static String TMinutes;
	static String TSeconds;
	public static int WaitingTime = 0;
	public static Gamemode gmed;
	public static void BeginTimer() {
	WaitingTime = Games.EndGameTime;
	pgtimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
	    @Override  
	    public void run() {
	    	if (WaitingTime > 0){
	    		if (WaitingTime >= 30){
	    			double Minutes = 0;
	    			Minutes = WaitingTime/60;
	    			if ((double)Minutes == 1.0)
	    			TMinutes = (int)Minutes  + " minute";
	    			else
		    		TMinutes = (int)Minutes  + " minutes";
	    			
	    			if (WaitingTime == (60 * 5.0) || WaitingTime == (60 * 1.0)){
	    				Message.G(Message.Replacer(Message.TimeLeft, TMinutes, "<time>"));
	    			}
	    			
	    			
	    		}
	    		else if (WaitingTime % 10 == 0 && WaitingTime <= 30){
    			if (WaitingTime == 1)
	    			TSeconds = WaitingTime + " second";
	    			else
	    			TSeconds = WaitingTime + " seconds";
				Message.G(Message.Replacer(Message.TimeLeft, TSeconds, "<time>"));
	    		}
	    		
	    		if (WaitingTime % 10 == 0 && (Main.Gamemode != Gamemode.Voting && Main.Gamemode != Gamemode.TeamElimination)){
	    			Global.AutoBalanceTeams();
	    		}
	    		WaitingTime--;
	    	}
	    	else if (WaitingTime == 0){
				if (Main.Gamemode == Gamemode.Mixed){
					if (Games.TeamKills.get("red") >= Games.TeamKills.get("blue")){
						DeathListener.WinningTeam("Red", ChatColor.RED);
					}
					else if (Games.TeamKills.get("blue") >= Games.TeamKills.get("red")){
						DeathListener.WinningTeam("Blue", ChatColor.AQUA);
					}
				}
				else if (Main.Gamemode == Gamemode.TDM){
					if (Games.TeamKills.get("red") > Games.TeamKills.get("blue")){
						DeathListener.WinningTeam("Red", ChatColor.RED);
					}
					else if (Games.TeamKills.get("blue") >= Games.TeamKills.get("red")){
						DeathListener.WinningTeam("Blue", ChatColor.AQUA);
					}
				}
				else if (Main.Gamemode == Gamemode.TeamElimination){
					if (JoinListener.blue.size() > JoinListener.red.size()){
						DeathListener.WinningTeam("Blue", ChatColor.AQUA);
					}
					else if (JoinListener.red.size() >= JoinListener.blue.size()){
						DeathListener.WinningTeam("Red", ChatColor.RED);
					}
				}
				else if (Main.Gamemode == Gamemode.CTF){
					
					if (Games.BlueFlagCaps > Games.RedFlagCaps){
						DeathListener.WinningTeam("Blue", ChatColor.AQUA);
					}
					else if (Games.RedFlagCaps > Games.BlueFlagCaps){
						DeathListener.WinningTeam("Red", ChatColor.RED);
					}
					else{
						DeathListener.WinningTeam("Tie", ChatColor.GRAY);
					}
				}
				else {
					DeathListener.WinningTeam("Server", ChatColor.BLACK);
				}
	    		Bukkit.getScheduler().cancelTask(pgtimer);
	    	}
	    }
	}, 20L, 20L);
	}
}
