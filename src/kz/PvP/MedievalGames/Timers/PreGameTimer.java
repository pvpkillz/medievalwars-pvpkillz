package kz.PvP.MedievalGames.Timers;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.PvP.MedievalGames.Games;
import kz.PvP.MedievalGames.Main;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;



public class PreGameTimer extends JavaPlugin{
	public static int pgtimer;
	
	
	public static ArrayList<Player> red = new ArrayList<Player>();
	public static ArrayList<Player> blue = new ArrayList<Player>();
	
	
	public static Main plugin;
	
	public PreGameTimer(Main main) {
	plugin = main;
	}
	static String TMinutes;
	static String time;
	public static int OR = Games.WaitingTimer;
	public static int WaitingTime = OR;
	public static Gamemode gmed;
	public static void BeginTimer() {
	WaitingTime = OR;
	pgtimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
	    @Override  
	    public void run() {
			for (Player p : plugin.getServer().getOnlinePlayers()){
				p.setLevel(WaitingTime);
			}
	    	if (WaitingTime > 0){
	    		if (WaitingTime % 10 == 0){
	    			double Minutes = 0;
					if (WaitingTime >= 60){
	    			Minutes = WaitingTime/60;
	    			if ((int)Minutes == 1)
	    			TMinutes = (int)Minutes  + " minute.";
	    			else
		    		TMinutes = (int)Minutes  + " minutes.";
	    			
	    			//Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "Game starting in " + TMinutes);
					}
					else{
		    			if (WaitingTime == 1)
			    			time = " second";
			    			else
			    			time = " seconds";
						//Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "Game starting in " + WaitingTime + time);
					}
	    		}
	    		else if (WaitingTime == 20 && Games.Gamesrun == 5){
	    			Message.G(Message.AllPlayersRandomized);
	    			blue.addAll(JoinListener.blue);
	    			red.addAll(JoinListener.red);
	    			JoinListener.red.clear();
	    			JoinListener.blue.clear();
	    			
	    			for (Player redp : red){
	    				JoinListener.AssignPlayer(redp, Team.Random);
	    			}
	    			for (Player bluep : blue){
	    				JoinListener.AssignPlayer(bluep, Team.Random);
	    			}
	    			Games.Gamesrun = 0;
	    		}
	    		else if (WaitingTime <= 9){
	    			if (WaitingTime == 1)
	    			time = " second";
	    			else
	    			time = " seconds";
	    			
	    			if (WaitingTime == 7){
	    				Location bluespawn = Global.GetLocOfString("bluespawn" + ".Loc", "war");
	    				Location redspawn = Global.GetLocOfString("redspawn" + ".Loc", "war");
	    				Location specspawn = Global.GetLocOfString("spectator" + ".Loc", "war");
	    				plugin.getServer().getWorld("war").loadChunk(plugin.getServer().getWorld("war").getChunkAt(bluespawn));
	    				plugin.getServer().getWorld("war").loadChunk(plugin.getServer().getWorld("war").getChunkAt(redspawn));
	    				plugin.getServer().getWorld("war").loadChunk(plugin.getServer().getWorld("war").getChunkAt(specspawn));
	    			}
	    			//Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "Game starting in " + WaitingTime + time);
	    			for (Player pl : Bukkit.getServer().getOnlinePlayers()){
						pl.playSound(pl.getLocation(), Sound.CLICK, 1.0F, (byte) 1);
	    			}
	    		}
	    		WaitingTime--;
	    	}
	    	else if (WaitingTime == 0){
	    		
	    		
	    		
	    	
				try {
					Games.BeginGame();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
	    	
	    	
	    	
	    	Bukkit.getScheduler().cancelTask(pgtimer);
	    	}
	    }
	}, 20L, 20L);
	}
}
