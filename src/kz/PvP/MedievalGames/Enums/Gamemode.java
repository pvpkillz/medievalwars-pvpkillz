package kz.PvP.MedievalGames.Enums;

public enum Gamemode {
	
	CTF, TDM, Mixed, TeamElimination, SoloElimination, Voting;
	
}