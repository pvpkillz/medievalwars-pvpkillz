package kz.PvP.MedievalGames;

import kz.PvP.MedievalGames.Enums.Game;
import kz.PvP.MedievalGames.Enums.Gamemode;
import kz.PvP.MedievalGames.utilities.IconMenu;
import kz.PvP.MedievalGames.utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


public class Vote {
	public static Main plugin;
	public Vote(Main mainclass) {
		plugin = mainclass;
		}
	
	
	public static void ShowVoteMenu(Player p){
		
        if (Main.Game == Game.PREGAME){
        	/*if(PlayerCommands.MENUS.containsKey(p)) {
			 PlayerCommands.MENUS.get(p).destroy();
			 PlayerCommands.MENUS.remove(p);
		 }*/
		 if (Games.VoteChoices.containsKey(p.getName()))
		 Games.VoteChoices.remove(p.getName());
		 
	    IconMenu menu = new IconMenu("Voting Period", p.getName(), 9, new IconMenu.OptionClickEventHandler() {
	        @Override
	        public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        	final Player p = event.getPlayer();
	        	if (event.getPosition() == 0 || event.getPosition() == 8){
	        	Games.VoteChoices.put(p.getName(), Gamemode.Mixed);
	        	Message.P(p, "You have voted for Mixed game.");
	        	}
	        	else if (event.getPosition() == 2){
	        	Games.VoteChoices.put(p.getName(), Gamemode.TDM);
	        	Message.P(p, "You have voted for Team Deathmatch.");
	        	}
	        	else if (event.getPosition() == 4){
		        Games.VoteChoices.put(p.getName(), Gamemode.CTF);
	        	Message.P(p, "You have voted for Capture the flag.");
	        	}
		        else if (event.getPosition() == 6){
		        Games.VoteChoices.put(p.getName(), Gamemode.TeamElimination);
	        	Message.P(p, "You have voted for Team Elimination.");
	            }
	        	event.setWillClose(true);
                event.setWillDestroy(false);
	        }
	    }, plugin)
	    .setOption(0, new ItemStack(Material.DIAMOND_PICKAXE, 1), ChatColor.GRAY + "Mixed Game", ChatColor.WHITE + "Vote for a Mixed gamemode!")
	    .setOption(2, new ItemStack(Material.DIAMOND_SWORD, 1), ChatColor.GOLD + "TDM", ChatColor.RED + "Vote for Team Deathmatch!")
	    .setOption(4, new ItemStack(Material.WOOL, 1), ChatColor.GREEN + "CTF", ChatColor.YELLOW + "Vote for capture the flag!")
	    .setOption(6, new ItemStack(Material.GOLD_SWORD, 1), ChatColor.RED + "Team Elimination", ChatColor.AQUA + "Vote for Team Elimination!")
	    .setOption(8, new ItemStack(Material.DIAMOND_PICKAXE, 1), ChatColor.GRAY + "Mixed Game", ChatColor.WHITE + "Vote for a Mixed gamemode!");
	    menu.open(p);
	    //PlayerCommands.MENUS.put(p, menu);
		
        }
        else{
        	p.sendMessage(ChatColor.RED + "You cannot vote at this time. Wait until round ends.");
        }
	}
	
	
	
	
}
