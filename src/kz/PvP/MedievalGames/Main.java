package kz.PvP.MedievalGames;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import kz.PvP.MedievalGames.Commands.GameCommands;
import kz.PvP.MedievalGames.Commands.PlayerCommands;
import kz.PvP.MedievalGames.Enums.Team;
import kz.PvP.MedievalGames.Events.BuildListener;
import kz.PvP.MedievalGames.Events.DamageListener;
import kz.PvP.MedievalGames.Events.DeathListener;
import kz.PvP.MedievalGames.Events.InteractListener;
import kz.PvP.MedievalGames.Events.JoinListener;
import kz.PvP.MedievalGames.Events.Spectator;
import kz.PvP.MedievalGames.Gamemodes.CTF;
import kz.PvP.MedievalGames.Gamemodes.Elimination;
import kz.PvP.MedievalGames.Gamemodes.Global;
import kz.PvP.MedievalGames.Gamemodes.TDM;
import kz.PvP.MedievalGames.Timers.EndGameTimer;
import kz.PvP.MedievalGames.Timers.PreGameTimer;
import kz.PvP.MedievalGames.Timers.PreparingTimer;
import kz.PvP.MedievalGames.utilities.Config;
import kz.PvP.MedievalGames.utilities.Kits;
import kz.PvP.MedievalGames.utilities.Message;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcsg.double0negative.tabapi.TabAPI;

import com.comphenix.protocol.Packets;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;



public class Main extends JavaPlugin{
		public static final int GameLength = 10;//Minute length of game
		static Logger log = Logger.getLogger("Minecraft");
		public static Main plugin;
		public static kz.PvP.MedievalGames.Enums.Game Game = kz.PvP.MedievalGames.Enums.Game.PREGAME;
		public static kz.PvP.MedievalGames.Enums.Gamemode Gamemode = kz.PvP.MedievalGames.Enums.Gamemode.Voting;
		public static boolean GamemodeVoting = true;
		public static Connection con;
		public static Plugin plug;
		//PluginManager pm = getServer().getPluginManager();
		public void onEnable()
	    {
		log.info("Enabling Medieval War");
		try
		{
		  String myDriver = "com.mysql.jdbc.Driver";
		  String myUrl = "jdbc:mysql://PvP-HG.com/MedievalWar";
		  Class.forName(myDriver);
		  // We connected, so we don't have to ever close it! :D
		  con = DriverManager.getConnection(myUrl, "MedievalWar", "test12");
		  System.out.println("Successfully connected to the Database for MedievalWars");
		}
		catch (Exception e)
		{
		  System.err.println("Failed to connect to Database!");
		  System.err.println(e.getMessage());
		}
		this.getServer().getWorlds().get(0).setAutoSave(false);
		//QueueManager.getInstance().setup();
		Config.loadConfigs();//We enable configs
	    new JoinListener(this);
	    new DeathListener(this);
	    new BuildListener(this);
	    //new QueueManager(this);
	    new DamageListener(this);
	    //new LoggingManager(this);
	    new InteractListener(this);
	    new Games(this);
	    new PlayerCommands(this);
	    new Spectator(this);
	    new Message(this);
	    new Mysql(this);
	    new GameCommands(this);
	    new PreGameTimer(this);
	    new Kits(this);
	    new PreparingTimer(this);
	    new EndGameTimer(this);
		DeathListener.WinningTeam("none", ChatColor.GRAY);
		new CTF(this);
	    new Global(this);
	    new Elimination(this);
	    new TDM(this);
	    new Vote(this);
        PreGameTimer.BeginTimer();
    	RegisterCommands();
		for ( Entity e : Bukkit.getWorlds().get(0).getEntities()){
			if (e.getType() != EntityType.PLAYER)
			e.remove();
		}
    	Scoreboard();
    	EnableProtocolLibFunctions();
    	
    	for (Player pl : this.getServer().getOnlinePlayers())
    	TabAPI.setPriority(this, pl, 2);
		DeathListener.LoadMapSave("war");
		Gamemode = kz.PvP.MedievalGames.Enums.Gamemode.Voting;
		}
	    private void EnableProtocolLibFunctions() {
			ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(
					this, ConnectionSide.SERVER_SIDE, ListenerPriority.HIGH, 
					Packets.Server.SET_SLOT, Packets.Server.WINDOW_ITEMS) {
				@Override
				public void onPacketSending(PacketEvent event) {
					if (event.getPacketID() == Packets.Server.SET_SLOT) {
						addGlow(new ItemStack[] { event.getPacket().getItemModifier().read(0) });
					} else {
						addGlow(event.getPacket().getItemArrayModifier().read(0));
					}
				}
			});
		}
		private void Scoreboard() {
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        for (Player pl : getServer().getOnlinePlayers()){
	    	        	JoinListener.ScoreBoard(pl);
	    	        	pl.setFoodLevel(20);
	    	        }
	    	    }
	    	}, 35L, 35L);
	    	
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        Message.G(Message.JoinGame, Team.Spectator);
	    			Location lobby = Global.GetLocOfString("mainlobby" + ".Loc", "lobby");
	    			if (!getServer().getWorld("lobby").isChunkLoaded(getServer().getWorld("lobby").getChunkAt(lobby)))
	    			getServer().getWorld("lobby").getChunkAt(lobby).load();
	    	    }
	    	}, 20 * 60 * 1, 20L * 60 * 1);
	    	
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        Message.G(Message.FeedbackPlease);
	    	    }
	    	}, 20 * 60 * 10, 20L * 60 * 10);
	    	
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        Message.G(Message.TexturePackUsed);
	    	    }
	    	}, 20 * 60 * 8, 20L * 60 * 8);
	    	
	    	
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        for (Player pl : getServer().getOnlinePlayers()){
	    	        	if (!Games.IdleUsers.containsKey(pl.getName()))
	    	        	Games.IdleUsers.put(pl.getName(), pl.getLocation());
	    	        	else{
	    	        		if (Games.IdleUsers.get(pl.getName()) == pl.getLocation()){
	    	        			JoinListener.red.remove(pl);
	    	        			JoinListener.blue.remove(pl);
	    	        			Games.SpecJoin(pl);
								Message.P(pl, Message.Replacer(Message.Idle, "" + Games.IdleTime, "<time>"));
	    	        		}
	    	        	}
	    	        }
	    	    }
	    	}, 20L * Games.IdleTime, 20L  * Games.IdleTime);
	    	
	    	
		}
		private void RegisterCommands() {
	    	getCommand("mw").setExecutor(new PlayerCommands(this));
	    	getCommand("mwa").setExecutor(new GameCommands(this));
	    	getCommand("mwcontrol").setExecutor(new GameCommands(this));
	    	getCommand("class").setExecutor(new PlayerCommands(this));
	    	getCommand("upgrade").setExecutor(new PlayerCommands(this));
	    	getCommand("team").setExecutor(new PlayerCommands(this));
	    	getCommand("activate").setExecutor(new PlayerCommands(this));
	    	getCommand("leave").setExecutor(new PlayerCommands(this));
	    	getCommand("chat").setExecutor(new PlayerCommands(this));
	    	getCommand("notify").setExecutor(new PlayerCommands(this));
	    	getCommand("points").setExecutor(new PlayerCommands(this));
	    	getCommand("help").setExecutor(new PlayerCommands(this));
	    	getCommand("rtv").setExecutor(new PlayerCommands(this));
	    	getCommand("vote").setExecutor(new PlayerCommands(this));
	    	getCommand("tell").setExecutor(new PlayerCommands(this));
	    	getCommand("reply").setExecutor(new PlayerCommands(this));
	    	getCommand("tp").setExecutor(new PlayerCommands(this));
	    	getCommand("tpall").setExecutor(new PlayerCommands(this));
	    	getCommand("gm").setExecutor(new PlayerCommands(this));
	    	getCommand("feedback").setExecutor(new PlayerCommands(this));
	    	getCommand("stats").setExecutor(new PlayerCommands(this));
		}
		public void onDisable(){
	    log.info("Disabling MedievalWar");
	    try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bukkit.getServer().getScheduler().cancelAllTasks();
		Bukkit.getServer().unloadWorld(Bukkit.getServer().getWorld("war"), false);
		//DeathListener.deleteDir(new File("war"));
	    }
		

		
		public static void addGlow(ItemStack[] selector) {
			for (ItemStack stack : selector) {
				if (stack != null) {
					// Only update those stacks that have our flag enchantment
					if (stack.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 32) {
						NbtCompound compound = (NbtCompound) NbtFactory.fromItemTag(stack);
						compound.put(NbtFactory.ofList("ench"));
					}
				}
			}
		}
		
		
		
		
}
